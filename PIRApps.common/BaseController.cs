﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using PIRApps.model;


namespace PIRApps.common
{
    public class BaseController : Controller
    {
        public void checkSession()
        {
            if (Session["UserId"] == null)
            {
                RedirectToAction("Login", "Home");
            }
        }
        public int UserId
        {
            get { return int.Parse(Session["UserId"].ToString()); }
        }
        public PrivilegeModel GetPrivilege()
        {
            PrivilegeModel privilege = (PrivilegeModel)Session["privilege"];
            

            return privilege;

        }
        public string UserGroupId
        {
            get { return Session["group"].ToString(); }
        }

    }
}
