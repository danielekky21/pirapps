﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using PIRApps.common.Security;

namespace PIRApps.common.Cookie
{
    public class CookieManager
    {
        private const string COOKIE_PREFIX = "PlazaIndonesia_";
        public const string COOKIE_LOGIN = "login";
        public const string COOKIE_LOGIN_MEMBER = "loginMember";
        public const int CookieTimeoutInDays = 1;


        /// <summary>
        /// Adds the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="value">The value.</param>
        /// <param name="dateExpires">The date expires.</param>
        //public static void Add(string key, string value, DateTime dateExpires)
        //{
        //    value = DataEncription.Encrypt(value);
        //    var cookie = new HttpCookie(COOKIE_PREFIX + key) { Expires = dateExpires, Value = value };
        //    HttpContext.Current.Response.Cookies.Add(cookie);
        //}

        public static void Add(string key, string value, bool nonPersistent, bool encrypt)
        {
            if (encrypt)
                value = DataEncription.Encrypt(value);

            HttpCookie Cookie = new HttpCookie(COOKIE_PREFIX + key, value);

            if (!nonPersistent)
                Cookie.Expires = DateTime.Now.AddDays(CookieTimeoutInDays);

            HttpContext.Current.Response.Cookies.Add(Cookie);
        }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static string GetValue(string key)
        {
            return Contains(key) ? DataEncription.Decrypt(HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key].Value) : string.Empty;
        }

        /// <summary>
        /// Determines whether [contains] [the specified key].
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>
        /// 	<c>true</c> if [contains] [the specified key]; otherwise, <c>false</c>.
        /// </returns>
        public static bool Contains(string key)
        {
            return HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key] != null ? true : false;
        }

        /// <summary>
        /// Deletes the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        public static void Delete(string key)
        {
            if (HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key] != null)
            {
                var cookie = new HttpCookie(COOKIE_PREFIX + key) { Expires = DateTime.Now.AddDays(-1d) };
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool CookieExist(string key)
        {

            // *** Check to see if we have a cookie we can use

            HttpCookie loCookie = HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key];

            if (loCookie == null)
                return false;

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public static void Remove(string key)
        {

            HttpCookie Cookie = HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key];

            if (Cookie != null)
            {

                //Cookie.Expires = DateTime.Now.AddHours(-2);
                Cookie.Expires = DateTime.Now.AddDays(-1);
                //HttpContext.Current.Response.Cookies.Remove(Cookie.Name);
                HttpContext.Current.Response.Cookies.Add(Cookie);

            }

        }

        public static string GetCookieValue(string key, bool encrypted)
        {
            string cookieVal = String.Empty;
            if (HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key] != null)
            {
                cookieVal = HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key].Value;

                if (encrypted)
                    cookieVal = DataEncription.Decrypt(cookieVal);
            }
            return cookieVal;
        }

        public static void SetCookie(string key, string value)
        {
            string cookieVal = String.Empty;
            if (HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key] != null)
            {
                cookieVal = HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key].Value;

                HttpContext.Current.Request.Cookies[COOKIE_PREFIX + key].Value = value;
            }
        }

    }
}
