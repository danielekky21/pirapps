﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PIRApps.model;
using System.Net.Mail;
using System.IO;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace PIRApps.common
{
    public class GlobalFunction
    {
        public static int GetCookieAdminLogin()
        {
            int id = 0;

            if (!string.IsNullOrEmpty(HttpContext.Current.Request.Cookies["PlazaindonesiaManagementCookie"].Values["user"]))
            {
                id = int.Parse(HttpContext.Current.Request.Cookies["PlazaindonesiaManagementCookie"].Values["user"]);
            }

            return id;
        }
        public static int ConvertMonthNameIntoInt(string month)
        {
            return DateTime.ParseExact(month, "MMMM", CultureInfo.InvariantCulture).Month;
        }
        public enum statusEnum
        {
            disable = 0,
            enable = 1
        }
        public static List<PriorityModel> GetPriorityModel()
        {
            List<PriorityModel> priority = new List<PriorityModel>();
            PriorityModel modelLow = new PriorityModel();
            modelLow.PriorityValue = "L";
            modelLow.PriorityText = "LOW";
            priority.Add(modelLow);
            PriorityModel modelMedium = new PriorityModel();
            modelMedium.PriorityValue = "M";
            modelMedium.PriorityText = "MEDIUM";
            priority.Add(modelMedium);
            PriorityModel modelHigh = new PriorityModel();
            modelHigh.PriorityValue = "H";
            modelHigh.PriorityText = "HIGH";
            priority.Add(modelHigh);
            return priority;
        }
        public static async Task<bool> SendMailWorkFlow(string emailRecipient,string name,string message)
        {
            try
            {
                string smtpHost = System.Configuration.ConfigurationManager.AppSettings["SMTP"];
                string emailHost = System.Configuration.ConfigurationManager.AppSettings["MailName"];
                string emailName = System.Configuration.ConfigurationManager.AppSettings["MailUser"];
                string emailPass = System.Configuration.ConfigurationManager.AppSettings["Password"];
                string port = System.Configuration.ConfigurationManager.AppSettings["Port"];
                var url = System.Configuration.ConfigurationManager.AppSettings["SiteURL"];
                var workflowlink = "<br><a href=" + url + "/WorkFlow/List>Please click link below to go to workflow</a>";
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(emailHost, emailName);
                mail.To.Add(emailRecipient);
                SmtpClient client = new SmtpClient();
                client.Port = int.Parse(port);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(emailHost, emailPass);
                client.Host = smtpHost;
                client.EnableSsl = true;
                mail.IsBodyHtml = true;
                mail.Subject = "WorkFlow";

                mail.Body = "Dear " + name + " " + message + workflowlink ;
                client.SendCompleted += (s, e) =>
                {
                    client.Dispose();
                    mail.Dispose();
                };
                await client.SendMailAsync(mail);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
           

        }
        public static async Task<bool> SendAttachmentFile(string emailRecipient, string name, string fileName)
        {
            try
            {
                string smtpHost = System.Configuration.ConfigurationManager.AppSettings["SMTP"];
                string emailHost = System.Configuration.ConfigurationManager.AppSettings["MailName"];
                string emailName = System.Configuration.ConfigurationManager.AppSettings["MailUser"];
                string emailPass = System.Configuration.ConfigurationManager.AppSettings["Password"];
                string port = System.Configuration.ConfigurationManager.AppSettings["Port"];
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(emailHost, emailName);
                mail.To.Add(emailRecipient);
                SmtpClient client = new SmtpClient();
                client.Port = int.Parse(port);
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(emailName, emailPass);
                client.Host = smtpHost;
                client.EnableSsl = true;
                mail.IsBodyHtml = true;
                mail.Subject = "WorkFlow";
                mail.Body = "Dear " + name + " " + "FYI <br/> Here, the file you are requested";
                FileInfo f = new FileInfo(fileName);
                if (f.Exists)
                {
                    mail.Attachments.Add(new Attachment(f.FullName));
                }
                client.SendCompleted += (s, e) =>
                {
                    client.Dispose();
                    mail.Dispose();
                };
                await client.SendMailAsync(mail);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public static void ConvertToExcelHelper(HttpResponseBase Response,GridView gv,string excelName)
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename="+ excelName + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
        }
        public static List<TicketStatusModel> TicketStatusGenerate()
        {
            List<TicketStatusModel> model = new List<TicketStatusModel>();
            TicketStatusModel ticket = new TicketStatusModel();
            ticket.statusCode = "D";
            ticket.statusName = "OPEN";
            model.Add(ticket);
            ticket = new TicketStatusModel();
            ticket.statusCode = "T";
            ticket.statusName = "TAKEN";
            model.Add(ticket);
            ticket = new TicketStatusModel();
            ticket.statusCode = "S";
            ticket.statusName = "SUBMITTED";
            model.Add(ticket);
            ticket = new TicketStatusModel();
            ticket.statusCode = "C";
            ticket.statusName = "CLOSED";
            model.Add(ticket);
            return model;

        }
        public static List<ActiveModel> ActiveModelGenerate()
        {
            List<ActiveModel> model = new List<ActiveModel>();
            ActiveModel active = new ActiveModel();
            active.ActiveModelText = "Active";
            active.ActiveModelValue = "Active";
            model.Add(active);
            active = new ActiveModel();
            active.ActiveModelText = "Not Active";
            active.ActiveModelValue = "NotActive";
            model.Add(active);
            return model;
            
        }
        
    }
}
