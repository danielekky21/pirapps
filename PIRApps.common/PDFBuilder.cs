﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp;
using System.IO;
using RazorEngine;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;

namespace PIRApps.common
{
    public class PDFBuilder
    {

        private  string _file;
        private string _Url;
        private PIRApps.model.QuotationModel quot;
        public void PdfBuilder(string file,string imgUrl,model.QuotationModel Model)
        {
            _file = file;
            _Url = imgUrl;
            quot = Model;
        }
        public FileContentResult GetPdf()
        {
            var html = GetHtml();
            Byte[] bytes;
            using (var ms = new MemoryStream())
            {
                using (var doc = new Document())
                {
                    using (var writer = PdfWriter.GetInstance(doc, ms))
                    {
                        doc.Open();
                        
                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(_Url);
                        jpg.ScaleToFit(140f, 120f);
                        //jpg.SpacingBefore = 10f;
                        //jpg.SpacingAfter = 1f;
                        jpg.Alignment = Element.ALIGN_LEFT;
                        doc.Add(jpg);
                        try
                        {
                            
                            using (var msHtml = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(html)))
                            {
                           
                                    XMLWorkerHelper.GetInstance()
                                    .ParseXHtml(writer, doc, msHtml, System.Text.Encoding.UTF8);
                            }
                        }
                        finally
                        {
                           
                            doc.Close();
                        }
                    }
                }
                bytes = ms.ToArray();
            }
            return new FileContentResult(bytes, "application/pdf");
        }
        private string GetHtml()
        {
            var html = File.ReadAllText(_file);
            return Razor.Parse(html,quot);
        }
    }
}
