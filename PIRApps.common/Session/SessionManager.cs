﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace PIRApps.common.Session
{
    public class SessionManager
    {
        private const string SESSION_PREFIX = "";
        private const string CUSTOMER_ID = "CurrentCustomerId";
        public const string CUSTOMER_LOG = "LogKey";

        public static int CustomerId
        {
            get { return Get<int>(CUSTOMER_ID); }
            set { Add(CUSTOMER_ID, value); }
        }
        public static bool CustomerLoggedIn
        {
            get { return Contains(CUSTOMER_ID); }
        }

        /// <summary>
        /// Check if the session contains a specified key
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool Contains(string key)
        {
            foreach (string k in HttpContext.Current.Session.Keys)
            {
                if (k == key)
                    return true;
            }
            return false;
        }

        public static bool IsLogin(string key)
        {
            if (!string.IsNullOrEmpty(HttpContext.Current.Session[key].ToString()))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Adds the value to the session
        /// </summary>
        /// <param name="key">Key to use to be added</param>
        /// <param name="value">Value to be added</param>
        public static void Add(string key, object value)
        {
            if (HttpContext.Current.Session != null)
                HttpContext.Current.Session[SESSION_PREFIX + key] = value;
        }

        /// <summary>
        /// Gets the session value
        /// </summary>
        /// <typeparam name="T">Type of the session to be retrieved</typeparam>
        /// <param name="key">Key for the session object</param>
        public static T Get<T>(string key)
        {
            return (T)HttpContext.Current.Session[SESSION_PREFIX + key];
        }

        /// <summary>
        /// Removes the item from the session
        /// </summary>
        /// <param name="key">Key to be removed from the cache</param>
        public static void Remove(string key)
        {
            HttpContext.Current.Session.Remove(SESSION_PREFIX + key);
        }
    }
}
