﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
namespace PIRApps.common
{
    public class SiteSettings
    {
        public static string Domain
        {
            get { return ConfigurationManager.AppSettings["Domain"]; }
        }

        public static string SiteURL
        {
            get { return ConfigurationManager.AppSettings["SiteURL"]; }
        }

        public static int DifferentHours
        {
            get { return int.Parse(ConfigurationManager.AppSettings["DifferentHours"].ToString()); }
        }

        #region SMTP
        public static string MailName
        {
            get { return ConfigurationManager.AppSettings["MailName"]; }
        }

        public static string SMTP
        {
            get { return ConfigurationManager.AppSettings["SMTP"]; }
        }

        public static int Port
        {
            get { return int.Parse(ConfigurationManager.AppSettings["Port"].ToString()); }
        }
        #endregion
    }
}
