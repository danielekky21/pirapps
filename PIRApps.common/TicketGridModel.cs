﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.common
{
    public class TicketGridModel
    {
        public int id { get; set; }
        public string ticketNo { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
