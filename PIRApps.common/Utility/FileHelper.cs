﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;


namespace PIRApps.common.Utility
{
    public class FileHelper
    {
        /// <summary>
        /// Removes the special characters from a title string to create a page name.
        /// </summary>
        /// <param name="str">The STR.</param>
        /// <returns></returns>
        public static string RemoveSpecialCharacters(string str)
        {
            string output = Regex.Replace(str, @"[^\w\s]", "", RegexOptions.Compiled);
            output = output.Replace("  ", " ");

            return output;
        }

        /// <summary>
        /// Reads all text from a file and releases all locks fast.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public static string ReadAllTextAndRelease(string path)
        {
            using (var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var textReader = new StreamReader(fileStream))
                {
                    return textReader.ReadToEnd();
                }
            }
        }
    }
}
