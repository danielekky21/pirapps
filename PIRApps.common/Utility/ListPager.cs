﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace PIRApps.common.Utility
{
    public class ListPager
    {
        public bool AutoLoad = true;
        public int CurrentPage = 1;
        public int PerPage = 20;
        public int TotalItems;
        public string SearchString;
        public string AdditionQueryParam;

        public string LoadPager()
        {
            StringBuilder strOutPut = new StringBuilder();
            string strLinkFormat = HttpContext.Current.Request.PathInfo + "?p={0}&search={1}";
            int intPages = (int)Math.Ceiling((double)((TotalItems) / PerPage));

            if (intPages == 0)
                intPages = 1;
            else
                if (TotalItems % PerPage > 0)
                    intPages++;

            for (int y = 1; y <= intPages; y++)
            {
                if (y > 1)
                    strOutPut.Append(" | ");
                if (y == CurrentPage)
                    strOutPut.AppendFormat("<a  href=\"{0}&{1}\"><b>{2}</b></a>&nbsp;", String.Format(strLinkFormat, y, SearchString), AdditionQueryParam, y);
                else
                    strOutPut.AppendFormat("<a href=\"{0}&{1}\">{2}</a>&nbsp;", String.Format(strLinkFormat, y, SearchString), AdditionQueryParam, y);
            }
            return string.Format("Viewing page {0} of {1}<br /><br />Page: {2}", CurrentPage.ToString(), intPages.ToString(), strOutPut.ToString());
        }
    }
}
