﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data.Classes
{
    public class JSONResponse<T>
    {
        public string Status { get; set; }
        public string ErrorEntity { get; set; }
        public string Message { get; set; }
        public T Obj { get; set; }
    }
}
