﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Objects;
using PIRApps.data;
using PIRApps.data.GenericRepository;

namespace PIRApps.data
{
    public static class CurrentDataContext
    {
        public static PirAppsEntities2 CurrentContext
        {
            get
            {
                var repository = DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] as PirAppsEntities2;//IRepository;
                if (repository == null)
                {

                    repository = new PirAppsEntities2();
                    DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] = repository;
                }

                return repository;

            }
        }

        public static void CloseCurrentRepository()
        {
            var repository = DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] as PirAppsEntities2;//IRepository;
            if (repository != null)
            {
                repository.Dispose();
                DataRepositoryStore.CurrentDataStore[DataRepositoryStore.KEY_DATACONTEXT] = null;
            }
        }
    }
}
