﻿using System;
using System.Data;
using System.Data.Objects.DataClasses;
using System.Linq;
using PIRApps.data;
public static class EntityObjectExtension
{
    public static IEntityWithKey Save(this IEntityWithKey entity)
    {
        object originalItem;

        if (CurrentDataContext.CurrentContext.TryGetObjectByKey(entity.EntityKey, out originalItem))
        {
            //CurrentDataContext.CurrentContext.ApplyPropertyChanges(entity.EntityKey.EntitySetName, entity);
            CurrentDataContext.CurrentContext.ApplyCurrentValues(entity.EntityKey.EntitySetName, entity);
        }
        else
        {
            CurrentDataContext.CurrentContext.AddObject(entity.EntityKey.EntitySetName, entity);
        }
        CurrentDataContext.CurrentContext.SaveChanges();
        return entity;
    }

    /// <summary>
    ///Saves the specified object to the current Context
    /// </summary>
    /// <param name="obj">The obj.</param>
    public static void Save<T>(this EntityObject obj) where T : class, new()
    {
        var isNewRecord = IsNewRecord(obj.EntityKey);
        if (isNewRecord)
        {
            var objectSet = CurrentDataContext.CurrentContext.CreateObjectSet<T>();
            CurrentDataContext.CurrentContext.AddObject(objectSet.EntitySet.Name, obj);
        }
        var objectState = isNewRecord ? EntityState.Added : EntityState.Modified;
        CurrentDataContext.CurrentContext.ObjectStateManager.ChangeObjectState(obj, objectState);
        CurrentDataContext.CurrentContext.SaveChanges();
    }

    /// <summary>
    /// Inserts the specified obj.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj">The obj.</param>
    public static void InsertSave<T>(this EntityObject obj) where T : class, new()
    {
        var isNewRecord = IsNewRecord(obj.EntityKey);
        if (isNewRecord)
        {
            var objectSet = CurrentDataContext.CurrentContext.CreateObjectSet<T>();
            CurrentDataContext.CurrentContext.AddObject(objectSet.EntitySet.Name, obj);
        }
        CurrentDataContext.CurrentContext.ObjectStateManager.ChangeObjectState(obj, EntityState.Added);
        CurrentDataContext.CurrentContext.SaveChanges();
    }

    /// <summary>
    /// Updates the save.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj">The obj.</param>
    public static void UpdateSave<T>(this EntityObject obj) where T : class, new()
    {
        var isNewRecord = IsNewRecord(obj.EntityKey);
        if (isNewRecord)
        {
            var objectSet = CurrentDataContext.CurrentContext.CreateObjectSet<T>();
            CurrentDataContext.CurrentContext.AddObject(objectSet.EntitySet.Name, obj);
        }
        CurrentDataContext.CurrentContext.ObjectStateManager.ChangeObjectState(obj, EntityState.Modified);
        CurrentDataContext.CurrentContext.SaveChanges();
    }

    /// <summary>
    /// Deletes the specified EntityObject.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj">The obj.</param>
    public static void Delete<T>(this EntityObject obj) where T : class, new()
    {
        var isNewRecord = IsNewRecord(obj.EntityKey);
        if (isNewRecord)
        {
            var objectSet = CurrentDataContext.CurrentContext.CreateObjectSet<T>();
            CurrentDataContext.CurrentContext.AddObject(objectSet.EntitySet.Name, obj);
        }
        CurrentDataContext.CurrentContext.ObjectStateManager.ChangeObjectState(obj, EntityState.Deleted);
        CurrentDataContext.CurrentContext.SaveChanges();
    }

    /// <summary>
    /// Deletes the specified EntityObject.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj">The obj.</param>
    public static void DeleteWithoutSaveChanges<T>(this EntityObject obj) where T : class, new()
    {
        var isNewRecord = IsNewRecord(obj.EntityKey);
        if (isNewRecord)
        {
            var objectSet = CurrentDataContext.CurrentContext.CreateObjectSet<T>();
            CurrentDataContext.CurrentContext.AddObject(objectSet.EntitySet.Name, obj);
        }
        CurrentDataContext.CurrentContext.ObjectStateManager.ChangeObjectState(obj, EntityState.Deleted);
    }

    /// <summary>
    /// Determines whether current object is new. 
    /// </summary>
    /// <param name="entityKey">The current EntityObject.</param>
    private static bool IsNewRecord(EntityKey entityKey)
    {
        if (entityKey == null)
        {
            return true;
        }
        int temp = 0;
        var keyMembers = entityKey.EntityKeyValues;
        return keyMembers.Where(x => string.IsNullOrWhiteSpace(x.Value.ToString()) || (Int32.TryParse(x.Value.ToString(), out temp) && Convert.ToInt32(x.Value.ToString()) == 0)).Any();
    }

    /// <summary>
    /// Determines whether current object is new. 
    /// </summary>
    /// <param name="obj">The current EntityObject.</param>
    public static bool IsNewRecord(this EntityObject obj)
    {
        if (obj == null)
        {
            return true;
        }
        return IsNewRecord(obj.EntityKey);
    }
}
