﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public interface IDataRepositoryStore
    {
        object this[string key] { get; set; }
    }
}
