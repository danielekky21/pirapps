﻿

namespace PIRApps.data.GenericRepository
{
    public interface IRepositoryFactory
    {
        IRepository Repository();
    }
}
