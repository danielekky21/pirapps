﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class BrandMaster
    {
        public static IQueryable<BrandMaster> GetAllActiveBrandMaster()
        {
            return CurrentDataContext.CurrentContext.BrandMasters.Where(x => x.IsActive == true);
        }
        public static BrandMaster GetBrandByID(int id)
        {
            return CurrentDataContext.CurrentContext.BrandMasters.FirstOrDefault(x => x.BrandID == id);
        }
    }
}
