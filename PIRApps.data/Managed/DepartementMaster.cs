﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class DepartementMaster
    {
        public static IQueryable<DepartementMaster> GetAllActiveDepartement()
        {
            return CurrentDataContext.CurrentContext.DepartementMaster.Where(x => x.IsDeleted == false);
        }
        public static DepartementMaster GetByID(int departementId)
        {
            return CurrentDataContext.CurrentContext.DepartementMaster.FirstOrDefault(x => x.DepartementID == departementId);
        }
    }
}
