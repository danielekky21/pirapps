﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public class EmployeeDataModel
    {
        public static IQueryable<model.EmployeeModel> EmployeeList(string prefix)
        {
            return CurrentDataContext.CurrentContext.vw_EmployeeMaster.Where(x => x.Name.StartsWith(prefix)).Select(x => new model.EmployeeModel()
            {
                NIK = x.NIK,
                Name = x.Name,
                LevelCode = x.LevelCode
                
            });
        }
    }
}
