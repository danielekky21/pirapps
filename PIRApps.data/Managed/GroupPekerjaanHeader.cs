﻿using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class GroupPekerjaanHeader
    {
        public static IQueryable<GroupPekerjaanHeader> GetAllGroupPekerjaanHeader()
        {
            return CurrentDataContext.CurrentContext.GroupPekerjaanHeaders;
        }
        public static GroupPekerjaanHeader GetPekerjaanHeaderByID(string id)
        {
            return CurrentDataContext.CurrentContext.GroupPekerjaanHeaders.FirstOrDefault(x => x.GroupPekerjaanCode == id);
        }
        public static IQueryable<GroupPekerjaanHeaderModel> MapPekerjaanHeaderToModel()
        {
            var dataAll = GetAllGroupPekerjaanHeader();
            var data = from i in dataAll
                       orderby i.CreatedDate descending
                       select new GroupPekerjaanHeaderModel
                       {
                           Subportfolio = i.Subportfolio,
                           Department = i.DepartementMaster.DepartementName,
                           GroupPekerjaanCode = i.GroupPekerjaanCode,
                           GroupPekerjaanName = i.GroupPekerjaanName,
                           CategoryPekerjaan = i.CategoryPekerjaan,
                           Status = i.IsActive
                       };

            return data;
        }
        public string DisplayedText { get; set; }

        public static IQueryable<GroupPekerjaanDetail> GetDetailItemByHeaderID(string id)
        {
            return CurrentDataContext.CurrentContext.GroupPekerjaanDetails.Where(x => x.GroupPekerjaanCode == id);
        }
    }
}
