﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    
    public partial class ITAssets
    {
        public static IQueryable<ITAsset> GetAllActiveITAssets()
        {
            return CurrentDataContext.CurrentContext.ITAssets.Where(x => x.isDeleted == false && x.WorkFlowStatus != "S" && x.WorkFlowStatus != "R");
        }
        public static IQueryable<model.ITAssetModel> GetAllActiveITAssetsToModel(model.ITAssetsModel model)
        {

            var query = CurrentDataContext.CurrentContext.ITAssets.Select(c => new model.ITAssetModel()
            {
                BarcodeNumber = c.BarcodeNumber,
                AssetNo = c.AssetNo,
                Name = c.Name,
                User = c.User,
                LevelPosisi = c.LevelPosisi,
                Departement = c.DepartementMaster.DepartementName,
                Location = c.LocationMaster.LocationName,
                UserName = c.UserName,
                Model = c.Model,
                modelasset = c.ModelAsset.ModelName,
                OSDescription = c.OSDecription,
                OSVersion = c.OSVersion,
                Brand = c.Brand,
                brandName = c.BrandMaster.BrandName,
                CreatedDate = c.CreatedDate,
                Processor = c.Processor,
                SN = c.SN,
                Tipe = c.Tipe,
                TahunPembelian = c.TahunPembelian,
                RAM = c.RAM,
                OS = c.OS,
                OSOEM = c.OSOEM,
                UserStatus = c.UserStatus,
                DomainStatus = c.DomainStatus,
                VGACard = c.VGACard,
                AvayaExt = c.AvayaExt,
                AvayaSN = c.AvayaSN,
                NoteAvaya = c.NoteAvaya,
                WorkFlowStatus = c.WorkFlowStatus,
                tipeName = c.MasterType.TypeName,
                processorName = c.ProcessorMaster.ProcessorName,
                endWarranty = c.HabisGaransi,
                modelName = c.ModelAsset.ModelName,
                Remarks = c.Remarks

            });
            if (!string.IsNullOrEmpty(model.BarcodeNo))
            {
               query = query.Where(x => x.BarcodeNumber == model.BarcodeNo );
            }
            if (model.RequestDateFrom.Year != 0001)
            {
               query = query.Where(x => x.CreatedDate >= model.RequestDateFrom);
            }
            if (model.RequestDateTo.Year != 0001)
            {
               query = query.Where(x => x.CreatedDate <= model.RequestDateTo);
            }
            return query;
        }
        public static ITAsset GetAssetByAssetNo(string assetno)
        {
            return CurrentDataContext.CurrentContext.ITAssets.FirstOrDefault(x => x.AssetNo == assetno);
        }
        public static ITAsset GetAssetByBarcodeno(string BarcodeNo)
        {
            return CurrentDataContext.CurrentContext.ITAssets.FirstOrDefault(x => x.BarcodeNumber == BarcodeNo);
        }
        public static data.vw_EmployeeMaster GetUserData(string userid)
        {
            return CurrentDataContext.CurrentContext.vw_EmployeeMaster.FirstOrDefault(x => x.Name == userid);
        }
        public static model.ITAssetModel GetAssetByBarcodeNoToModel(string BarcodeNo)
        {
            return CurrentDataContext.CurrentContext.ITAssets.Where(x => x.BarcodeNumber == BarcodeNo).OrderByDescending(x => x.CreatedDate).Select(c => new model.ITAssetModel()
            {
                BarcodeNumber = c.BarcodeNumber,
                AssetNo = c.AssetNo,
                Name = c.Name,
                User = c.User,
                LevelPosisi = c.LevelPosisi,
                Departement = c.DepartementMaster.DepartementName,
                Location = c.LocationMaster.LocationName,
                UserName = c.UserName,
                Model = c.Model,
                modelasset = c.ModelAsset.ModelName,
                OSDescription = c.OSDecription,
                OSVersion = c.OSVersion,
                Brand = c.Brand,
                brandName = c.BrandMaster.BrandName,
                CreatedDate = c.CreatedDate,
                Processor = c.Processor,
                SN = c.SN,
                Tipe = c.Tipe,
                TahunPembelian = c.TahunPembelian,
                RAM = c.RAM,
                OS = c.OS,
                OSOEM = c.OSOEM,
                UserStatus = c.UserStatus,
                DomainStatus = c.DomainStatus,
                VGACard = c.VGACard,
                AvayaExt = c.AvayaExt,
                AvayaSN = c.AvayaSN,
                NoteAvaya = c.NoteAvaya,
                WorkFlowStatus = c.WorkFlowStatus,
                tipeName = c.MasterType.TypeName,
                processorName = c.ProcessorMaster.ProcessorName,
                endWarranty = c.HabisGaransi,
                Remarks = c.Remarks

            }).FirstOrDefault();
        }
        public static model.AssignUserAssetModel GetUserInformation(string BarcodeNo)
        {
            return CurrentDataContext.CurrentContext.ITAssets.Where(x => x.BarcodeNumber == BarcodeNo).OrderByDescending(x => x.CreatedDate).Select(c => new model.AssignUserAssetModel()
            {
                Name = c.Name,
                BarcodeNo = c.BarcodeNumber,
                UserName = c.UserName,
                LevelPosisi = c.LevelPosisi,
                Departement = c.Departement,
                Location = c.Location
                
            }).FirstOrDefault();
         }
        public static bool CekIfBarcodeDuplicate(string barcodeNo)
        {
            var currentAsset = CurrentDataContext.CurrentContext.ITAssets.FirstOrDefault(x => x.BarcodeNumber == barcodeNo);
            if (currentAsset == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool CekIfAssetNoDuplicate(string assetNo)
        {
            var currentAsset = CurrentDataContext.CurrentContext.ITAssets.FirstOrDefault(x => x.AssetNo == assetNo);
            if (currentAsset == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static bool ReleaseAsset(string BarcodeNo)
        {
            try
            {
                var currenAsset = CurrentDataContext.CurrentContext.ITAssets.FirstOrDefault(x => x.BarcodeNumber == BarcodeNo);
                currenAsset.UserName = null;
                currenAsset.LevelPosisi = null;
                currenAsset.Departement = null;
                currenAsset.Location = null;
                currenAsset.UpdateSave<ITAsset>();
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public static void InsertIntoAssethistory(string BarcodeNo,string Desc,string by)
        {
            ITAssetHistory Asset = new ITAssetHistory();
            Asset.AssetHistoryBarcode = BarcodeNo;
            Asset.AssetHistoryDesc = Desc;
            Asset.CreatedDate = DateTime.Now;
            Asset.CreatedBy = by;
            Asset.InsertSave<ITAssetHistory>();
        }
    }
}
