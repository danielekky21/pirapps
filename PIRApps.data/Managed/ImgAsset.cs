﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class ImgAsset
    {
        public static IQueryable<ImgAsset> GetImageList(string BarcodeNo)
        {
            return CurrentDataContext.CurrentContext.ImgAssets.Where(x => x.BarcodeNo == BarcodeNo);
        }
    }
}
