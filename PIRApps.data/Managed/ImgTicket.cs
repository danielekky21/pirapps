﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class ImgTicket
    {
        public static IQueryable<ImgTicket> GetImageList(string ticketno)
        {
            return CurrentDataContext.CurrentContext.ImgTicket.Where(x => x.TicketNo == ticketno);
        }
    }
}
