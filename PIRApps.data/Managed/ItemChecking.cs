﻿using System;
using System.Collections.Generic;
using System.Linq;
using PIRApps.model;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class ItemChecking
    {
        public static IQueryable<ItemChecking> GetAllItemChecking()
        {
            return CurrentDataContext.CurrentContext.ItemCheckings.Where(x => x.IsActive == true);
        }
        public static ItemChecking GetItemCheckingFromID(string id)
        {
            return CurrentDataContext.CurrentContext.ItemCheckings.FirstOrDefault(x => x.ItemCheckingCode == id);
        }
        public static ItemChecking GetItemCheckingFromIDandCondition(string id,string condition)
        {
            return CurrentDataContext.CurrentContext.ItemCheckings.FirstOrDefault(x => x.ItemCheckingCode == id && x.ConditionCode == condition);
        }
        public static IQueryable<ItemCheckingListModel> MapToItemcheckingModel()
        {
            var data = from i in CurrentDataContext.CurrentContext.ItemCheckings
                       where i.IsActive == true
                       orderby i.CreatedDate descending
                       select new ItemCheckingListModel
                       {
                           Subportofolio = i.Subportofolio,
                           ItemCheckingCode = i.ItemCheckingCode,
                           ItemDescription = i.ItemDescription,
                           AreaCode = i.AreaCode,
                           TypeArea = i.TypeArea,
                           Condition = i.ConditionCode,
                           Status = i.IsActive
                       };
            return data;       
        }
        public static IQueryable<ItemCheckingItem> GetItemsCheckingByCheckingCode(string checkingcode)
        {
            return CurrentDataContext.CurrentContext.ItemCheckingItems.Where(x => x.ItemCheckingCode == checkingcode);
        }
        public static IQueryable<ItemCheckingItem> GetItemsCheckingByCheckingCodeAndCondition(string checkingcode,string conditionCode)
        {
            return CurrentDataContext.CurrentContext.ItemCheckingItems.Where(x => x.ItemCheckingCode == checkingcode && x.Conditioncode == conditionCode);
        }
    }
}
