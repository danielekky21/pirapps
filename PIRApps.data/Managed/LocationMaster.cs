﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class LocationMaster
    {
        public static IQueryable<LocationMaster> GetAllActiveLocation()
        {
            return CurrentDataContext.CurrentContext.LocationMaster.Where(x => x.IsDeleted == false);
        }
        public static LocationMaster GetLocationByID(int id)
        {
            return CurrentDataContext.CurrentContext.LocationMaster.FirstOrDefault(x => x.LocationID == id);
        }
    }
}
