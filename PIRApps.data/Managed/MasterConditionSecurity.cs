﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class MasterConditionSecurity
    {
        public static IQueryable<MasterConditionSecurity> GetAllCondition()
        {
            return CurrentDataContext.CurrentContext.MasterConditionSecurities;
        }
        public static MasterConditionSecurity GetConditionByID(string id)
        {
            return CurrentDataContext.CurrentContext.MasterConditionSecurities.FirstOrDefault(x => x.ConditionCode == id);
        }
    }
}
