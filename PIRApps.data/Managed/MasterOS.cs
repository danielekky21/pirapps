﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class MasterOS
    {
        public static IQueryable<MasterOS> GetAllActiveOS()
        {
            return CurrentDataContext.CurrentContext.MasterOS.Where(x => x.IsActive == true);
        }
        public static MasterOS GetOSByID(int OSID)
        {
            return CurrentDataContext.CurrentContext.MasterOS.FirstOrDefault(x => x.OSID == OSID);
        }
    }
}
