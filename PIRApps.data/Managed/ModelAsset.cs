﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class ModelAsset
    {
        public static IQueryable<ModelAsset> GetAllActiveModelAsset(int brandID)
        {
            return CurrentDataContext.CurrentContext.ModelAssets.Where(x => x.IsActive == true && x.BrandID == brandID);
        }
        public static ModelAsset GetModelAssetByID(int id)
        {
            return CurrentDataContext.CurrentContext.ModelAssets.FirstOrDefault(x => x.ModelID == id);
        }
    }
}
