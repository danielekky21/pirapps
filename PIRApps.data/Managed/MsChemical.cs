﻿using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class MsChemical
    {
        public static IQueryable<MsChemical> GetAllChemical()
        {
            return CurrentDataContext.CurrentContext.MsChemicals;
        }
        public static MsChemical GetChemicalByID(string code)
        {
            return CurrentDataContext.CurrentContext.MsChemicals.FirstOrDefault(x => x.ChemicalCode == code);
        }
        public static IQueryable<MsChemicalModel> MapChemicalToModel()
        {
            var dataAll = MsChemical.GetAllChemical();
            var data = from i in dataAll
                       orderby i.CreatedDate descending
                       select new MsChemicalModel
                       {
                           ChemicalCode = i.ChemicalCode,
                           ChemicalDesc = i.ChemicalDesc,
                           Status = i.IsActive
                       };
            return data;
        }
        public static int MsChemicalCount()
        {
            var dataAll = MsChemical.GetAllChemical();
            return dataAll.Count();
        }
        
    }
}
