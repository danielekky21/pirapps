﻿using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class MsEquipment
    {
        public static IQueryable<MsEquipment> GetAllEquipment()
        {
            return CurrentDataContext.CurrentContext.MsEquipments;
        }
        public static MsEquipment GetAllEquipmentByID(string code)
        {
            return CurrentDataContext.CurrentContext.MsEquipments.FirstOrDefault(x => x.EquipmentCode == code);
        }
        public static IQueryable<MsEquipmentModel> MapEquipmentToModel()
        {
            var dataAll = GetAllEquipment();
            var data = from i in dataAll
                       orderby i.CreatedDate descending
                       select new MsEquipmentModel
                       {
                           EquipmentCode = i.EquipmentCode,
                           EquipmentDesc = i.EquipmentDesc,
                           Status = i.IsActive
                       };

            return data;
        }
        public static int MeEquipmentCount()
        {
            var dataAll = MsEquipment.GetAllEquipment();
            return dataAll.Count();
        }
    }
}
