﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRApps.model;

namespace PIRApps.data
{
    public partial class MsMenuTb
    {
        public static IQueryable<MsMenuTb> GetAllMenuTb()
        {
            return CurrentDataContext.CurrentContext.MsMenuTbs;
        }
        public static IQueryable<MsMenuTb> GetallActive()
        {
            return CurrentDataContext.CurrentContext.MsMenuTbs.Where(x => x.IsActive == true);
        }
        //public static IQueryable<MsMenuTb> GetMenuByUserName()
        //{ }
    }
}
