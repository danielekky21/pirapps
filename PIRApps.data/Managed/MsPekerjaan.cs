﻿using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class MsPekerjaan
    {
        public static IQueryable<MsPekerjaan> GetAllPerkerjaan()
        {
            return CurrentDataContext.CurrentContext.MsPekerjaans;
        }
        public static MsPekerjaan GetPekerjaanByID(string code)
        {
            return CurrentDataContext.CurrentContext.MsPekerjaans.FirstOrDefault(x => x.PekerjaanCode == code);
        }
        public static IQueryable<MsPekerjaanModel> MapPekerjaanToModel()
        {
            var dataAll = GetAllPerkerjaan();
            var data = from i in dataAll
                       orderby i.CreatedDate descending
                       select new MsPekerjaanModel
                       {
                           PerkerjaanCode = i.PekerjaanCode,
                           PekerjaanName = i.PekerjaanName,
                           Department = i.DepartementMaster.DepartementName,
                           Status = i.IsActive
                       };
            return data;
        }
        public static int MsPekerjaanCount()
        {
            var dataAll = GetAllPerkerjaan();
            return dataAll.Count();
        }
    }
}
