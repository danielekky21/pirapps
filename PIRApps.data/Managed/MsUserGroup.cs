﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRApps.model;
namespace PIRApps.data
{
    public partial class MsUserGroup
    {
        public static IQueryable<MsUserGroup> GetAllUSerGroup()
        {
            return CurrentDataContext.CurrentContext.MsUserGroups;
        }
        public static MsUserGroup GetMsUserGroupByID(int id)
        {
            return CurrentDataContext.CurrentContext.MsUserGroups.FirstOrDefault(x => x.UserGroupID == id);
        }
        public static IQueryable<UserGroupModel> MapUserGroupToModel()
        {
            var data = from i in CurrentDataContext.CurrentContext.MsUserGroups
                       orderby i.CreatedDate descending
                       select new UserGroupModel
                       {
                           UserGroupID = i.UserGroupID,
                           UserGroupCode = i.UserGroupCode,
                           UserGroupName = i.UserGroupName,
                           GroupHead = i.UserTb.UserName,
                           Status = i.IsActive
                       };

            return data;
        }
        public static IQueryable<UserGroupMember> GetMemberByUserGroupID(int id)
        {
            return CurrentDataContext.CurrentContext.UserGroupMembers.Where(x => x.UserGroupID == id);
        }
        public static int GetGroupIDbyCode(string code)
        {
            return CurrentDataContext.CurrentContext.MsUserGroups.FirstOrDefault(x => x.UserGroupCode == code).UserGroupID;
        }
        public static string GetGroupCodeByID(int id)
        {
            return CurrentDataContext.CurrentContext.MsUserGroups.FirstOrDefault(x => x.UserGroupID == id).UserGroupCode;
        }
        public static IQueryable<LogsheetGroupHead> GetHeadByUserGroupID(int id)
        {
            return CurrentDataContext.CurrentContext.LogsheetGroupHeads.Where(x => x.GroupID == id);
        }
    }
}
