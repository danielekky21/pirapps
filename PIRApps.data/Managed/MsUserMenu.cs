﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRApps.model;

namespace PIRApps.data
{
    public partial class MsUserMenu
    {
        public IList<MsUserMenuDto> GetMenus(string username)
        {
            IList<MsUserMenuDto> TreeMenus = new List<MsUserMenuDto>();
            MsUserMenuDto DetailMenu = new MsUserMenuDto();
            var menu = CurrentDataContext.CurrentContext.vw_UserMenus.Where(x => x.UserName == username)
                            .Select(
                                    x => new MsUserMenuDto
                                    {
                                        MenuId = x.MenuID,
                                        Menuname = x.MenuName,
                                        MenuParent = x.MenuParent,
                                        MenuLink = x.MenuLink,
                                        MenuAction = x.MenuAction,
                                        MenuIcon = x.MenuIcon,
                                        CreateData = x.CreateData,
                                        ReadData = x.ReadData,
                                        UpdateData = x.UpdateData,
                                        DeleteData = x.DeleteData,
                                        IsShow = x.IsShow,
                                        IsActive = x.IsActive,
                                        ModulID = x.ModulID
                                    }).ToList();
            var treeHeaders = menu.Where(x => x.MenuId == x.MenuParent).ToList();
            foreach (var item in treeHeaders)
            {
                DetailMenu = item;
                TreeMenus.Add(GetMenuClients(menu, DetailMenu));
            }

            return TreeMenus;
        }
        private static MsUserMenuDto GetMenuClients(List<MsUserMenuDto> menus, MsUserMenuDto DetailMenus)
        {
            var ChildMenus = menus.Where(x => x.MenuParent == DetailMenus.MenuId).ToList();
            ChildMenus = ChildMenus.Where(x => x.MenuId > DetailMenus.MenuId).ToList();
            foreach (var item in ChildMenus)
            {
                DetailMenus.getMenuChild.Add(item);
                GetMenuClients(menus, item);
            }
            return DetailMenus;
        }
        public static IQueryable<MsMenuTb> GetAllActiveMenu()
        {
            return CurrentDataContext.CurrentContext.MsMenuTbs.Where(x => x.IsActive == true);
        }
        public static IQueryable<MsUserMenu> getUserMenu(int id)
        {
            return CurrentDataContext.CurrentContext.MsUserMenus.Where(x => x.UserID == id);
        }
        public static vw_UserMenus GetPrivilegeByMenuId(int menuid, int UserId)
        {
            return CurrentDataContext.CurrentContext.vw_UserMenus.FirstOrDefault(x => x.MenuID == menuid && x.UserID == UserId);
        }
        public static MsMenuTb GetDefaultPage(int userid)
        {
            var defaultMenuID = CurrentDataContext.CurrentContext.MsUserMenus.FirstOrDefault(x => x.UserID == userid && x.IsDefault == "Y");
            if (defaultMenuID != null)
            {
                return CurrentDataContext.CurrentContext.MsMenuTbs.FirstOrDefault(x => x.MenuId == defaultMenuID.MenuID);
            }
            else
            {
                return new MsMenuTb();
            }
        }
        public static bool IsMenuActive(int menuid, int userid)
        {
            var menu = CurrentDataContext.CurrentContext.MsUserMenus.Where(x => x.MenuID == menuid && x.IsShow == "Y" && x.UserID == userid);
            if (menu.Count() > 0)
            {
               return true;
            }
            else
            {
                return false;
            }
        }
    }
}
