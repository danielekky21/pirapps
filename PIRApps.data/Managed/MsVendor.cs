﻿using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class MsVendor
    {
        public static IQueryable<MsVendor> GetAllVendor()
        {
            return CurrentDataContext.CurrentContext.MsVendors;
        }
        public static MsVendor GetAllVendorByID(int id)
        {
            return CurrentDataContext.CurrentContext.MsVendors.FirstOrDefault(x => x.VendorID == id);
        }
        public static IQueryable<MsVendorModel> MapToVendorModel()
        {
            var dataAll = GetAllVendor();
            var data = from i in dataAll
                       orderby i.CreatedDate descending
                       select new MsVendorModel
                       {
                           VendorID = i.VendorID,
                           VendorName = i.VendorName,
                           NPWP = i.NPWP,
                           Status = i.IsActive
                       };
            return data;
        }
        public static IQueryable<DropDownModel> MapToDropDownModel()
        {
            var dataAll = GetAllVendor();
            var data = from i in dataAll
                       where i._IsActive == true
                       orderby i.CreatedDate descending 
                       select new DropDownModel
                       {
                           value = i.VendorName,
                           text = i.VendorName
                       };
            return data;
        }
    }
}
