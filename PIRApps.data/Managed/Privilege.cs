﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PIRApps.model;

namespace PIRApps.data
{
    public class Privilege
    {
        public static PrivilegeModel GetPrivilegeToModel(int menuid, int userid)
        {
            return CurrentDataContext.CurrentContext.vw_UserMenus.Where(x => x.MenuID == menuid && x.UserID == userid).Select(x => new PrivilegeModel()
            {
                MenuId = x.MenuID,
                UserId = x.UserID,
                ReadData = x.ReadData,
                UpdateData = x.UpdateData,
                DeleteData = x.DeleteData,
                CreateData = x.CreateData,
                IsAdmin = x.IsAdmin,
                ModulID = x.ModulID,
                isShow = x.IsShow
               
            }).FirstOrDefault();
            
        }
    }
}
