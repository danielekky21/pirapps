﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class ProblemType
    {
        public static IQueryable<ProblemType> GetAllActiveProblemType()
        {
            return CurrentDataContext.CurrentContext.ProblemType.Where(x => x.IsDeleted == false);
        }
        public static ProblemType GetProblemTypeByID(int id)
        {
            return CurrentDataContext.CurrentContext.ProblemType.FirstOrDefault(x => x.ProblemTypeID == id);
        }
        public static IQueryable<ProblemType> GetAllProblemTypeByTicket(int ticketId)
        {
            return CurrentDataContext.CurrentContext.ProblemType.Where(x => x.TicketTypeID == ticketId && x.IsDeleted == false);
        }
    }
}
