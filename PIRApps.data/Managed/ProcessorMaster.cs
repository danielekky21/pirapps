﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial  class ProcessorMaster
    {
        public static IQueryable<ProcessorMaster> GetAllProcessorActive()
        {
            return CurrentDataContext.CurrentContext.ProcessorMasters.Where(x => x.IsActive == true);
        }
        public static ProcessorMaster GetProcessorByID(int id)
        {
            return CurrentDataContext.CurrentContext.ProcessorMasters.FirstOrDefault(x => x.ProcessorID == id);
        }
    }
}
