﻿using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class Quotation
    {
        public static IQueryable<Quotation> GetAllQuotationActive()
        {
            return CurrentDataContext.CurrentContext.Quotations.Where(x => x.IsActive == true);
        }
        public static int QuotationCount()
        {
            return CurrentDataContext.CurrentContext.Quotations.Count();
        }
        public static Quotation GetQuotationByID(string ID)
        {
            return CurrentDataContext.CurrentContext.Quotations.FirstOrDefault(x => x.QuotationID == ID);
        }
        public static IQueryable<QuotationModel> MapToQuotationModel()
        {
            var DataAll = GetAllQuotationActive();

            var data = from i in DataAll
                       orderby i.CreatedDate descending
                       select new QuotationModel
                       {
                           Subportofolio = i.Subportofolio,
                           QuotationID = i.QuotationID,
                           QuotationDate = i.CreatedDate,
                           ShopName = i.ShopName,
                           Floor = i.Floor,
                           Unit = i.Unit,
                           NettPrice = i.NettPrice,
                           Status = i.Status,
                            QuotationStatus= i.QuotationStatus

                       };
            return data;
        }
        public static IQueryable<QuotationEquipment> GetAllQuotationEquipment()
        {
            return CurrentDataContext.CurrentContext.QuotationEquipments;
        }
        public static IQueryable<QuotationItemPekerjaan> GetQitemPekerjaanByQID(string id)
        {
            return CurrentDataContext.CurrentContext.QuotationItemPekerjaans.Where(x => x.QuotationID == id);
        }
        public static IQueryable<QuotationChemical> GetQChemByQID(int id)
        {
            return CurrentDataContext.CurrentContext.QuotationChemicals.Where(x => x.QuotationPekerjaanID == id);
        }
        public static QuotationModel GetQtoModelID(string id)
        {
            var data = from i in CurrentDataContext.CurrentContext.Quotations
                       where i.QuotationID == id
                       select new QuotationModel
                       {
                           Subportofolio = i.Subportofolio,
                           QuotationID = i.QuotationID,
                           QuotationDate  = i.CreatedDate,
                           ShopName = i.ShopName,
                           Floor = i.Floor,
                           Unit = i.Unit,
                           NettPrice = i.NettPrice,
                           Status = i.Status,
                           NPWP = i.NPWP,
                           TenantPIC = i.PIC,
                           ContantNo = i.ContactNum,
                           Email = i.Email
                       };
            return data.FirstOrDefault();
        }
    }
}
