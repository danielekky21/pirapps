﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class SecurityApproval
    {
        public static IQueryable<SecurityApproval> getAllActiveSecurityWorkflow()
        {
            return CurrentDataContext.CurrentContext.SecurityApprovals.Where(x => x.ApprovalStatus == "DRAFT");
        }
        public static IQueryable<SecurityApproval> GetApprovalByID(int id)
        {
            return CurrentDataContext.CurrentContext.SecurityApprovals.Where(x => x.ApprovalID == id);
        }
        public static IQueryable<SecurityApproval> GetApprovalByFormID(string id)
        {
            return CurrentDataContext.CurrentContext.SecurityApprovals.Where(x => x.ApprovalFormID == id);
        }
        public static IQueryable<SecurityApproval> GetApprovalByUserID(int id)
        {
            return CurrentDataContext.CurrentContext.SecurityApprovals.Where(x => x.ApproverID == id && x.ApprovalStatus == "SUBMIT");
        }
    }
}
