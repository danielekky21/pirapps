﻿using System;
using System.Collections.Generic;
using System.Linq;
using PIRApps.model;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class SecurityLocation
    {
        public static IQueryable<SecurityLocation> GetAllSecurityLocation()
        {
            return CurrentDataContext.CurrentContext.SecurityLocations.Where(x => x.IsActive == true);
        }
        public static SecurityLocation GetLocationByID(string id)
        {
            return CurrentDataContext.CurrentContext.SecurityLocations.FirstOrDefault(x => x.LocationCode == id);
        }
        public static IQueryable<SecurityLocationListModel> MapToSecurityLocationModel()
        {
            var data = from i in CurrentDataContext.CurrentContext.SecurityLocations
                       where i.IsActive == true
                       orderby i.CreatedDate descending
                       select new SecurityLocationListModel
                       {
                           subportfolio = i.Subportfolio,
                           itemtypecode = i.TypeCode,
                           itemareacode = i.AreaCode,
                           locationcode = i.LocationCode,
                           locationname = i.LocationName,
                           floor = i.Floor,
                           callsign = i.Callsign,
                           status = i.IsActive
                       };
            return data;
        }
            //public static int GetApproverIDByShift(string Shift)
            //{
            //    return CurrentDataContext.CurrentContext.MsUserGroups.
            //}
    }
}
