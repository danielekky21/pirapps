﻿using System;
using System.Collections.Generic;
using System.Linq;
using PIRApps.model;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class SecurityLogsheet
    {
        public static IQueryable<SecurityLogsheet> GetAllSecurityLogsheet()
        {
            return CurrentDataContext.CurrentContext.SecurityLogsheets;
        }
        public static SecurityLogsheet GetSecurityLogsheetByID(string id)
        {
            return CurrentDataContext.CurrentContext.SecurityLogsheets.FirstOrDefault(x => x.LogsheetID == id);
        }
        public static List<LogsheetListModel> MapLogsheetToModel()
        {
            var time = DateTime.Now;
            var returnData = new List<LogsheetListModel>();
            var dataAll = CurrentDataContext.CurrentContext.SecurityLogsheets.Where(x => x.WorkFlowStatus != "APPROVED");
            //var shiftPagi = Convert.ToDateTime("03:00:00 PM");
            //var shiftSiang = Convert.ToDateTime("10:00:00 PM");
            //if (DateTime.Compare(time, shiftPagi) > 0)
            //{
            //    dataAll = dataAll.Where(x => x.Shift.ToLower() != "pagi");
            //}
            //if (DateTime.Compare(time,shiftSiang) > 0)
            //{
            //    dataAll = dataAll.Where(x => x.Shift.ToLower() != "siang");
            //}
            var data = from i in dataAll
                       orderby i.CreatedDate descending
                       select new LogsheetListModel
                       {
                           LogsheetID = i.LogsheetID,
                           tanggal = i.CreatedDate,
                           Shift = i.Shift,
                           Subportofolio = i.Subportfolio,
                           LocationCode = i.LocationCode,
                           LocationName = i.SecurityLocation.LocationName,
                           Status = i.IsActive,
                           locationArea = i.SecurityLocation.AreaCode,
                           workflowstatus = i.WorkFlowStatus,
                           Callsign = i.SecurityLocation.Callsign
                       };
            returnData = data.ToList();
            return returnData;
        }
        public static List<LogsheetListModel> MapLogsheetToModelhistory()
        {
            var time = DateTime.Now;
            var returnData = new List<LogsheetListModel>();
            var dataAll = CurrentDataContext.CurrentContext.SecurityLogsheets;
            var data = from i in dataAll
                       orderby i.CreatedDate descending
                       select new LogsheetListModel
                       {
                           LogsheetID = i.LogsheetID,
                           tanggal = i.CreatedDate,
                           Shift = i.Shift,
                           Subportofolio = i.Subportfolio,
                           LocationCode = i.LocationCode,
                           LocationName = i.SecurityLocation.LocationName,
                           Status = i.IsActive,
                           workflowstatus = i.WorkFlowStatus,
                           Callsign = i.SecurityLocation.Callsign,
                           endDate = i.UpdatedDate
                       };
            returnData = data.ToList();
            return returnData;
        }
        public static IQueryable<LogsheetChecklist> GetLogSheetChecklistByLogsheetID(string id)
        {
            return CurrentDataContext.CurrentContext.LogsheetChecklists.Where(x => x.LogsheetID == id);
        }
        public static IEnumerable<string> GetConditionByLogsheetID(string id)
        {
            var test = CurrentDataContext.CurrentContext.SecurityLogsheets.FirstOrDefault(x => x.LogsheetID == id).SecurityLocation.ItemChecking.ItemCheckingItems;
            var test2 = CurrentDataContext.CurrentContext.SecurityLogsheets.FirstOrDefault(x => x.LogsheetID == id).SecurityLocation.ItemChecking.ItemCheckingItems.Distinct();
            var data = CurrentDataContext.CurrentContext.SecurityLogsheets.FirstOrDefault(x => x.LogsheetID == id).SecurityLocation.ItemChecking.ItemCheckingItems.Select(x => x.Conditioncode).Distinct();
            //var condition = CurrentDataContext.CurrentContext.ItemCheckings.Where(x => x.)
            return data;
        }
        public static List<ItemCheckingItem> GetItemByLogSheetID(string id)
        {
            var data = CurrentDataContext.CurrentContext.SecurityLogsheets.FirstOrDefault(x => x.LogsheetID == id).SecurityLocation.ItemChecking.ItemCheckingItems.ToList();
            return data;
        }
        public static IQueryable<LogsheetChecklist> GetLogsheetChecklistByID(string id)
        {
            return CurrentDataContext.CurrentContext.LogsheetChecklists.Where(x => x.LogsheetID == id);
        }
        public static MsUserGroup GetGroupByShift(string shift)
        {
            return CurrentDataContext.CurrentContext.MsUserGroups.FirstOrDefault(x => x.Shift.ToLower() == shift);
        }
        public static IQueryable<LogsheetImage> GetLogsheetImagebyID(string logsheetid)
        {
            return CurrentDataContext.CurrentContext.LogsheetImages.Where(x => x.LogsheetID == logsheetid);
        }
        public static IQueryable<LogsheetListModel> MapLogsheetReport()
        {
            var data = from logsheet in CurrentDataContext.CurrentContext.SecurityLogsheets
                       join approval in CurrentDataContext.CurrentContext.SecurityApprovals on logsheet.LogsheetID equals approval.ApprovalFormID
                       join name in CurrentDataContext.CurrentContext.UserTb on approval.ApproverID equals name.UserID
                       where logsheet.WorkFlowStatus == "APPROVED"
                       select new LogsheetListModel
                       {
                           LogsheetID = logsheet.LogsheetID,
                           tanggal = logsheet.CreatedDate,
                           locationType = logsheet.SecurityLocation.TypeCode,
                           locationArea = logsheet.SecurityLocation.AreaCode,
                           Floor = logsheet.SecurityLocation.Floor,
                           Subportofolio = logsheet.SecurityLocation.Subportfolio,
                           LocationName = logsheet.SecurityLocation.LocationName,
                           Callsign = logsheet.SecurityLocation.Callsign,
                           Shift = logsheet.Shift,
                           ApprovalDate = approval.UpdateDate,
                           SubmitBy = logsheet.SecurityName,
                           submitDate = approval.CreatedDate,
                           ApprovedBy = name.UserName
                       };
            return data;
        }

    }
}
