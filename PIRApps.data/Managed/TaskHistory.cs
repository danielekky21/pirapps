﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class TaskHistory
    {
        public static void addTaskLog(TicketMaster data , string command)
        {
            TaskHistory log = new TaskHistory();
            log.LogTicketNo = data.TicketNo;
            log.LogTask = command;
            log.LogCreatedDate = DateTime.Now;
            log.InsertSave<TaskHistory>();
        }
        public static IQueryable<TaskHistory> GetTaskHistoryByNo(string ticketNo)
        {
            return CurrentDataContext.CurrentContext.TaskHistory.Where(x => x.LogTicketNo == ticketNo);
        }
    }
}
