﻿using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class TenantMaster
    {
        public static IQueryable<TenantMaster> GetAllTenantMaster()
        {
            return CurrentDataContext.CurrentContext.TenantMasters;
        }
        public static TenantMaster GetTenantMasterByName(string name)
        {
            return CurrentDataContext.CurrentContext.TenantMasters.FirstOrDefault(x => x.ShopName == name);
        }
        public static IQueryable<DropDownModel> MapToDropDownModel()
        {
            var dataAll = GetAllTenantMaster();
            var data = from i in dataAll
                       where i.ShopName != string.Empty
                       select new DropDownModel {
                           value = i.ShopName,
                           text = i.ShopName
                       };
            return data;
        }
    }
}
