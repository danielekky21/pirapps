﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class TicketMaster
    {
        public static IQueryable<TicketMaster> GetAllTicketActive()
        {
            return CurrentDataContext.CurrentContext.TicketMasters.Where(x => x.IsDeleted == false && x.PIC == null);
        }
        public static IQueryable<model.TicketMasterModel> GetAllTicketWithFilter(model.ReportFilterModel filter)
        {
            var model = CurrentDataContext.CurrentContext.TicketMasters.Where(x => x.IsDeleted == false).Select(x => new model.TicketMasterModel()
            {
                TicketNo = x.TicketNo,
                TicketType = x.TicketType1.TicketTypeName,
                TicketSubject = x.TicketSubject,
                CreatedDate = x.CreatedDate,
                PIC = x.UserTb.Name,
                Departement = x.DepartementMaster.DepartementName,
                Location = x.LocationMaster.LocationName,
                UserId = x.UserId,
                Priority = x.Priority == "L" ? "LOW" : x.Priority == "M" ? "MEDIUM" : "HIGH",
                Status = x.Status

            });
            if (filter.RequestDateFrom.Year != 0001)
            {
                model = model.Where(x => x.CreatedDate >= filter.RequestDateFrom);
            }
            if (filter.RequestDateTo.Year != 0001)
            {
                model = model.Where(x => x.CreatedDate < filter.RequestDateTo);
            }
            return model;
        }
        public static IQueryable<TicketMaster> GetAllTicketActiveByGroup(int usergroup)
        {
            return CurrentDataContext.CurrentContext.TicketMasters.Where(x => x.IsDeleted == false && x.TicketType1.UserGroupId == usergroup);
        }
        public static TicketMaster GetTicketByID(string id)
        {
            return CurrentDataContext.CurrentContext.TicketMasters.FirstOrDefault(x => x.TicketNo == id);
        }
        public static int totalTicket()
        {
            return CurrentDataContext.CurrentContext.TicketMasters.Where(x => x.CreatedDate.Value.Year == DateTime.Now.Year && x.CreatedDate.Value.Month == DateTime.Now.Month && x.CreatedDate.Value.Day == DateTime.Now.Day).Count();
        }
        public static IQueryable<model.TicketMasterModel> GetTaskList(int id)
        {
            return CurrentDataContext.CurrentContext.TicketMasters.Where(x => x.PIC == id && x.Status != "C" && x.Status != "S").Select(x => new model.TicketMasterModel()
            {
                TicketNo = x.TicketNo,
                TicketType = x.TicketType1.TicketTypeName,
                TicketSubject = x.TicketSubject,
                CreatedDate = x.CreatedDate,
                PIC = x.UserTb.Name,
                Departement = x.DepartementMaster.DepartementName,
                Location = x.LocationMaster.LocationName,
                UserId = x.UserId,
                Priority = x.Priority,
                Status = x.Status

            });
        }
        public static IQueryable<model.TicketMasterModel> GetTaskHistoryList(int id)
        {
            return CurrentDataContext.CurrentContext.TicketMasters.Where(x => x.PIC == id && x.Status != "D").Select(x => new model.TicketMasterModel()
            {
                TicketNo = x.TicketNo,
                TicketType = x.TicketType1.TicketTypeName,
                TicketSubject = x.TicketSubject,
                CreatedDate = x.CreatedDate,
                PIC = x.UserTb.Name,
                Departement = x.DepartementMaster.DepartementName,
                Location = x.LocationMaster.LocationName,
                UserId = x.UserId,
                Priority = x.Priority,
                Status = x.Status

            });
        }
        public static IQueryable<model.TicketMasterModel> GetTicketMasterModelAll(model.TicketMasterModel model)
        {

            var query = CurrentDataContext.CurrentContext.TicketMasters.Where(x => x.IsDeleted == false);
            if (model.TicketNo != null)
            {
                query = query.Where(x => x.TicketNo.Contains(model.TicketNo));
            }
            if (model.Status != null)
            {
                query = query.Where(x => x.Status == model.Status);
            }
            if (model.CreatedDate != null)
            {
                query = query.Where(x => x.CreatedDate.Value.Day == model.CreatedDate.Value.Day && x.CreatedDate.Value.Month == model.CreatedDate.Value.Month && x.CreatedDate.Value.Year == model.CreatedDate.Value.Year);
            }
            if (model.UserId != null)
            {
                query = query.Where(x => x.UserId.Contains(model.UserId));
            }
            return query.Select(x => new model.TicketMasterModel()
            {
                TicketNo = x.TicketNo,
                TicketType = x.TicketType1.TicketTypeName,
                TicketSubject = x.TicketSubject,
                CreatedDate = x.CreatedDate,
                PIC = x.UserTb.Name,
                Departement = x.DepartementMaster.DepartementName,
                Location = x.LocationMaster.LocationName,
                UserId = x.UserId,
                picid = x.UserTb.UserID,
                Priority = x.Priority,
                Status = x.Status

            });


        }
        public static IQueryable<model.TicketMasterModel> GetTicketMasterModelByGroup(int usergroup, model.TicketMasterModel model)
        {

            var query = CurrentDataContext.CurrentContext.TicketMasters.Where(x => x.IsDeleted == false && x.TicketType1.UserGroupId == usergroup && x.Status != "C" && x.Status != "S");
            if (model.TicketNo != null)
            {
                query = query.Where(x => x.TicketNo.Contains(model.TicketNo));
            }
            if (model.Status != null)
            {
                query = query.Where(x => x.Status == model.Status);
            }
            if (model.CreatedDate != null)
            {
                query = query.Where(x => x.CreatedDate.Value.Day == model.CreatedDate.Value.Day && x.CreatedDate.Value.Month == model.CreatedDate.Value.Month && x.CreatedDate.Value.Year == model.CreatedDate.Value.Year);
            }
            if (model.UserId != null)
            {
                query = query.Where(x => x.UserId.Contains(model.UserId));
            }
            return query.Select(x => new model.TicketMasterModel()
            {
                TicketNo = x.TicketNo,
                TicketType = x.TicketType1.TicketTypeName,
                TicketSubject = x.TicketSubject,
                CreatedDate = x.CreatedDate,
                Departement = x.DepartementMaster.DepartementName,
                Location = x.LocationMaster.LocationName,
                UserId = x.UserId,
                picid = x.UserTb.UserID,
                PIC = x.UserTb.Name,
                Priority = x.Priority,
                Status = x.Status
            });
        }
        public static IQueryable<UserTb> GetEmailrecipient(int groupid)
        {
            return CurrentDataContext.CurrentContext.UserTb.Where(x => x.UserGroupID == groupid);
        }
    }
}
