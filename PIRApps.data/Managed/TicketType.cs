﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class TicketType
    {
        public static IQueryable<TicketType> GetAllTicketType()
        {
            return CurrentDataContext.CurrentContext.TicketType.Where(x => x.IsDeleted == false);
        }
        public static TicketType GetTicketTypeByID(int id)
        {
            return CurrentDataContext.CurrentContext.TicketType.FirstOrDefault(x => x.TicketTypeID == id);
        }
        public static string GetTicketTypeName(int id)
        {
            return CurrentDataContext.CurrentContext.TicketType.FirstOrDefault(x => x.TicketTypeID == id).TicketTypeName;
        }
    }
}
