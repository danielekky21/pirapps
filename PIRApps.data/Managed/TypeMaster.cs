﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class TypeMaster
    {
        public static IQueryable<MasterType> GetallActiveTypeMaster()
        {
            return CurrentDataContext.CurrentContext.MasterTypes.Where(x => x.IsActive == true);
        }
        public static MasterType GetMasterTypeByID(int TypeID)
        {
            return CurrentDataContext.CurrentContext.MasterTypes.FirstOrDefault(x => x.TypeID == TypeID);
        }
    }
}
