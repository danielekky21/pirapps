﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class UserGroup
    {
        public static IQueryable<UserGroup> GetAllActive()
        {
            return CurrentDataContext.CurrentContext.UserGroup.Where(x => x.IsDeleted == false);
        }
        public static UserGroup GetByID(int id)
        {
            return CurrentDataContext.CurrentContext.UserGroup.FirstOrDefault(x => x.UserGroupID == id);
        }

    }
}
