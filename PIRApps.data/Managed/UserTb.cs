﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class UserTb
    {
        public static IQueryable<UserTb> GetAllActiveUser()
        {
            return CurrentDataContext.CurrentContext.UserTb.Where(x => x.IsDeleted == false);
        }
        public static UserTb GetUserByID(int id)
        {
            return CurrentDataContext.CurrentContext.UserTb.FirstOrDefault(x => x.UserID == id);
        }
        public static UserTb GetByUsernameAndPassword(string username, string password)
        {
            return CurrentDataContext.CurrentContext.UserTb.FirstOrDefault(x => x.UserName == username && x.Password == password);
        }
        public static string GetEmailByUserID(int userId)
        {
            return CurrentDataContext.CurrentContext.UserTb.FirstOrDefault(x => x.UserID == userId).Email;
        }
        public static string GetEmailByUserName(string username)
        {
            return CurrentDataContext.CurrentContext.UserTb.FirstOrDefault(x => x.Name == username).Email;
        }
        public static string GetNameByUserID(int userId)
        {
            return CurrentDataContext.CurrentContext.UserTb.FirstOrDefault(x => x.UserID == userId).Name;
        }
        public static int GetUserGroup(int userId)
        {
            return CurrentDataContext.CurrentContext.UserTb.FirstOrDefault(x => x.UserID == userId).UserGroupID;
        }
        public static UserTb GetUserByEmail(string email)
        {
            return CurrentDataContext.CurrentContext.UserTb.FirstOrDefault(x => x.Email == email);
        }
    }
}
