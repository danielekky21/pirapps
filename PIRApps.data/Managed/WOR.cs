﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class WOR
    {
        public static WOR getWORbyQuotID(string id)
        {
            return CurrentDataContext.CurrentContext.WORs.FirstOrDefault(x => x.QuotationID == id);
        }
        public static int countWOR()
        {
            return CurrentDataContext.CurrentContext.WORs.Where(x => x.CreatedDate.Value.Year == DateTime.Now.Year).Count();
        }
        public static WOR getWORByID(string id)
        {
            return CurrentDataContext.CurrentContext.WORs.FirstOrDefault(x => x.WORID == id);
        }
        public static IQueryable<WOR> MapTo
    }
}
