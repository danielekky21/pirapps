﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class WorkFlowChild
    {
        public static WorkFlowChild GetWorkFlowChildByModule(string moduleid)
        {
            return CurrentDataContext.CurrentContext.WorkFlowChild.FirstOrDefault(x => x.ModuleID == moduleid && x.IsDeleted == false);
        }
        public static bool CheckIfWorkFlowExist(string moduleid)
        {
            var data = CurrentDataContext.CurrentContext.WorkFlowChild.FirstOrDefault(x => x.ModuleID == moduleid && x.IsDeleted == false);
            if (data != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static int GetApproverIDFromIndex(string moduleid, int idx)
        {
            return CurrentDataContext.CurrentContext.WorkFlowChild.Where(x => x.ModuleID == moduleid && x.WorkFlowIdx == idx).Select(x => x.WorkFlowIdx).FirstOrDefault().GetValueOrDefault();
        }
        
    }
}
