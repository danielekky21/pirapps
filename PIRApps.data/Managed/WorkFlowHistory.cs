﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class WorkFlowHistory
    {
        public static IQueryable<WorkFlowHistory> GetHistoryByTableKeyID(string tablekeyid)
        {
            return CurrentDataContext.CurrentContext.WorkFlowHistory.Where(x => x.WorkFlowHistoryKeyID == tablekeyid);
        }
    }
}
