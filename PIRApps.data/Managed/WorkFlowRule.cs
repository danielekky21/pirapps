﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class WorkFlowRule
    {
        public static WorkFlowRule GetWorkFLowRuleByModuleID(string moduleid)
        {
            return CurrentDataContext.CurrentContext.WorkFlowRules.FirstOrDefault(x => x.ModuleID == moduleid && x.IsActive == true);
        }
        public static bool CheckIfNextLevelExist(int idx, string moduleid)
        {
            var count = CurrentDataContext.CurrentContext.WorkFlowRules.Where(x => x.ApproveLevel > idx && x.ModuleID == moduleid).Count();
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
