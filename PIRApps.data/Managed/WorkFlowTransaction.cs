﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.data
{
    public partial class WorkFlowTransaction
    {
        public static IQueryable<WorkFlowTransaction> GetAllActiveWorkFlowByUserName(int username)
        {
            return CurrentDataContext.CurrentContext.WorkFlowTransaction.Where(x => x.UserApproveID == username && x.WorkFlowStatus != "APPROVED" && x.WorkFlowStatus != "REJECTED");
        }
        public static IQueryable<WorkFlowTransaction> GetAllNotActiveWorkFlowByUserName(int username)
        {
            return CurrentDataContext.CurrentContext.WorkFlowTransaction.Where(x => x.UserApproveID == username && x.WorkFlowStatus == "APPROVED" || x.WorkFlowStatus == "REJECTED");
        }
        public static bool AddtoWorkFlow(string moduleid,string workflowname,string name,string keytableid , int idx)
        {
            try
            {
                //int userid = WorkFlowChild.GetApproverIDFromIndex(moduleid, 1);
                string controller = CurrentDataContext.CurrentContext.WorkFlowRules.Where(x => x.ModuleID == moduleid).Select(x => x.WorkFlowController).FirstOrDefault();
                string action = CurrentDataContext.CurrentContext.WorkFlowRules.Where(x => x.ModuleID == moduleid).Select(x => x.WorkFlowAction).FirstOrDefault();
                int approveID = CurrentDataContext.CurrentContext.WorkFlowChild.Where(x => x.ModuleID == moduleid && x.WorkFlowIdx == idx).Select(x => x.UserApproval).FirstOrDefault().GetValueOrDefault();
                string emailAddress = UserTb.GetEmailByUserID(approveID);
                string nameReceipt = UserTb.GetNameByUserID(approveID);
                WorkFlowTransaction workflow = new WorkFlowTransaction();
                workflow.WorkFlowName = workflowname;
                workflow.WorkFlowIdx = idx;
                workflow.ModuleID = moduleid;
                workflow.UserApproveID = approveID;
                workflow.UniqueIdentifier = "WF-" + moduleid;
                workflow.CreatedBy = name;
                workflow.CreatedDate = DateTime.Now;
                workflow.IsDeleted = false;
                workflow.KeytTableID = keytableid;
                workflow.WorkFlowStatus = "SUBMIT";
                workflow.DetailController = controller;
                workflow.DetailAction = action;
                workflow.InsertSave<WorkFlowTransaction>();
               
                Task.Run(() => { PIRApps.common.GlobalFunction.SendMailWorkFlow(emailAddress, nameReceipt, "You have Workflow waiting to be approved"); });
                return true;
            }
            catch (Exception e)
            {
                return false;
                throw;
            }
           

        }
        public static int nextIdxVal(string moduleid,int idx)
        {
            var currentWF = CurrentDataContext.CurrentContext.WorkFlowChild.Where(x => x.ModuleID == moduleid && x.WorkFlowIdx > idx);
            return currentWF.OrderBy(x => x.WorkFlowIdx).FirstOrDefault().WorkFlowIdx.GetValueOrDefault();
        }
        public static WorkFlowTransaction GetWorkFlowByUniqueID(int workflowid)
        {
            return CurrentDataContext.CurrentContext.WorkFlowTransaction.FirstOrDefault(x => x.WorkFlowId == workflowid);
        }
        public static bool SetApproveWorkFlow(WorkFlowTransaction trans,string by)
        {
            try
            {
              
                trans.WorkFlowStatus = "APPROVED";
                trans.UpdatedDate = DateTime.Now;
                trans.UpdatedBy = by;
                trans.UpdateSave<WorkFlowTransaction>();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
           
        }
        public static bool SetRejectWorkFlow(WorkFlowTransaction trans, string by)
        {
            try
            {
                trans.WorkFlowStatus = "REJECTED";
                trans.UpdatedDate = DateTime.Now;
                trans.UpdatedBy = by;
                trans.UpdateSave<WorkFlowTransaction>();
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }
        public static bool UpdateTableAfterWorkFlowDone(WorkFlowTransaction trans,string by)
        {
            try
            {
                SetApproveWorkFlow(trans, by);
                string emailAddress = UserTb.GetEmailByUserName(trans.CreatedBy);
       
                string table = CurrentDataContext.CurrentContext.WorkFlowRules.FirstOrDefault(x => x.ModuleID == trans.ModuleID).UpdateTable;
                string field = CurrentDataContext.CurrentContext.WorkFlowRules.FirstOrDefault(x => x.ModuleID == trans.ModuleID).UpdateField;
                string keyTable = CurrentDataContext.CurrentContext.WorkFlowRules.FirstOrDefault(x => x.ModuleID == trans.ModuleID).KeyField;
                string query = "update " + table + " set " + field + "= {0} " + "where " + keyTable + " = {1}";
                CurrentDataContext.CurrentContext.ExecuteStoreCommand(query, 'C', trans.KeytTableID);
                Task.Run(() => { PIRApps.common.GlobalFunction.SendMailWorkFlow(emailAddress, trans.CreatedBy, "Work Flow Approved!"); });
                return true;
            }
            catch (Exception)
            {
                return false;
            }
           
                
        }
        public static bool UpdateTableAfterWorkFlowReject(WorkFlowTransaction trans, string by,string reason)
        {
            try
            {
                string emailAddress = UserTb.GetEmailByUserName(trans.CreatedBy);

                string table = CurrentDataContext.CurrentContext.WorkFlowRules.FirstOrDefault(x => x.ModuleID == trans.ModuleID).UpdateTable;
                string field = CurrentDataContext.CurrentContext.WorkFlowRules.FirstOrDefault(x => x.ModuleID == trans.ModuleID).UpdateField;
                string keyTable = CurrentDataContext.CurrentContext.WorkFlowRules.FirstOrDefault(x => x.ModuleID == trans.ModuleID).KeyField;
                string query = "update " + table + " set " + field + "= {0} " + "where " + keyTable + " = {1}";
                CurrentDataContext.CurrentContext.ExecuteStoreCommand(query, 'D', trans.KeytTableID);
                PIRApps.common.GlobalFunction.SendMailWorkFlow(emailAddress, by, "Ticket With No: "+ trans.KeytTableID +" <br/> Work Flow Rejected! <br/> Reason : " + reason);
                var TaskData = TicketMaster.GetTicketByID(trans.KeytTableID);
                TaskHistory.addTaskLog(TaskData, "TICKET REJECTED BY " + by + " Reason : " + reason);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
        public static void addHistory(string KeyID, string Reason, string by)
        {
            WorkFlowHistory log = new WorkFlowHistory();
            log.WorkFlowHistoryKeyID = KeyID;
            log.WorkFlowHistoryMessage = Reason;
            log.WorkFlowHistoryBy = by;
            log.WorkFlowHistoryDate = DateTime.Now;
            log.InsertSave<WorkFlowHistory>();
        }
        public static IQueryable<WorkFlowRule> GetWorkFlowModule()
        {
            return CurrentDataContext.CurrentContext.WorkFlowRules.Where(x => x.IsActive == true);
        }
        public static WorkFlowRule GetWorkflowRuleByModuleID(string moduleid)
        {
            return CurrentDataContext.CurrentContext.WorkFlowRules.FirstOrDefault(x => x.ModuleID == moduleid);
        }
        public static IQueryable<WorkFlowChild>GetWorkflowChildByModuleID(string moduleid)
        {
            return CurrentDataContext.CurrentContext.WorkFlowChild.Where(x => x.ModuleID == moduleid);
        }
        public static string GetModuleNameByModuleID(string moduleid)
        {
            return CurrentDataContext.CurrentContext.WorkFlowRules.FirstOrDefault(x => x.ModuleID == moduleid).ModuleName;
        }
    }
}
