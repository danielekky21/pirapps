﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class ActiveModel
    {
        public string ActiveModelValue { get; set; }
        public string ActiveModelText { get; set; }
    }
}
