﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class AssignUserAssetModel
    {
        public string BarcodeNo { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string LevelPosisi { get; set; }
        public int? Departement { get; set; }
        public int? Location { get; set; }


    }
}
