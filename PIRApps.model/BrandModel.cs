﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class BrandModel
    {
        public int BrandID { get; set; }
        public string BrandName { get; set; }
    }
}
