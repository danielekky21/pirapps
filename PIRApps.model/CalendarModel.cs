﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class CalendarModel
    {
        public string id { get; set; }
        public string title { get; set; }
        public string desc { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public string color { get; set; }
        public bool allDay { get; set; }
        public string dept { get; set; }
    }
}
