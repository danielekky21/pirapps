﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class DepartementModel
    {
        public int DepartementID { get; set; }
        public string DepartementName { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }


    }
}
