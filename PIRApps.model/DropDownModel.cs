﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class DropDownModel
    {
        public string value { get; set; }
        public string text { get; set; }
    }
}
