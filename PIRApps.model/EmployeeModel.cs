﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class EmployeeModel
    {
        public string NIK { get; set; }
        public string Name { get; set; }
        public string LevelCode { get; set; }
    }
}
