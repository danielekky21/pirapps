﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class GrandTotalModel
    {
        public string QuotationID { get; set; }
        public decimal GrandTotals { get; set; }
        public bool IsPPN { get; set; }
        public decimal NettPrice { get; set; }
    }
}
