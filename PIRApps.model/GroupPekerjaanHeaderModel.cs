﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class GroupPekerjaanHeaderModel
    {
        public string Subportfolio { get; set; }
        public string Department { get; set; }
        public string GroupPekerjaanCode { get; set; }
        public string GroupPekerjaanName { get; set; }
        public string CategoryPekerjaan { get; set; }
        public bool? Status { get; set; }
    }
}
