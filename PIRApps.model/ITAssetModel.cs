﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class ITAssetModel
    {
        public string AssetNo { get; set; }
        public string BarcodeNumber { get; set; }
        public string Name { get; set; }
        public string User { get; set; }
        public string LevelPosisi { get; set; }
        public string Departement { get; set; }
        public string Location { get; set; }
        public string UserName { get; set; }
        public int? Model { get; set; }
        public string modelasset { get; set; }
        public int? Processor { get; set; }
        public string SN { get; set; }
        public int? Tipe { get; set; }
        public string TahunPembelian { get; set; }
        public string RAM { get; set; }
        public int? OS { get; set; }
        public string OSOEM { get; set; }
        public string UserStatus { get; set; }
        public string DomainStatus { get; set; }
        public string VGACard { get; set; }
        public string Status { get; set; }
        public string StatusDesc { get; set; }
        public string AvayaExt { get; set; }
        public string AvayaSN { get; set; }
        public string NoteAvaya { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string WorkFlowStatus { get; set; }
        public int? Brand { get; set; }
        public DateTime? HabisGaransi { get; set; }
        public string OSVersion { get; set; }
        public string OSDescription { get; set; }
        public string tipeName { get; set; }
        public string brandName { get; set; }
        public string processorName { get; set; }
        public DateTime? endWarranty { get; set; }
        public string OSName { get; set; }
        public string Remarks { get; set; }
        public string modelName { get; set; }
        public string userLogin { get; set; }
    }
}
