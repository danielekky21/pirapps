﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class ITAssetReport
    {
        public string AssetNo { get; set; }
        public string BarcodeNumber { get; set; }
        public string Name { get; set; }
        public string LevelPosisi { get; set; }
        public string Departement { get; set; }
        public string Location { get; set; }
        public string UserName { get; set; }
        public string Model { get; set; }
        public string Processor { get; set; }
        public string SN { get; set; }
        public string Tipe { get; set; }
        public string RAM { get; set; }
        public string DomainStatus { get; set; }
        public string VGACard { get; set; }
        public string AvayaExt { get; set; }
        public string NoteAvaya { get; set; }
        public string CreatedDate { get; set; }
        
                
    }
}
