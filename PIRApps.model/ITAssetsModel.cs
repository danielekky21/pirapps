﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class ITAssetsModel
    {
        public string AssetNo { get; set; }
        public string BarcodeNo { get; set; }
        public string Brand { get; set; }
        public string Location { get; set; }
        public string TahunPembelian { get; set; }
        public string Status { get; set; }
        public DateTime RequestDateFrom { get; set; }
        public DateTime RequestDateTo { get; set; }
        public string User { get; set; }
        public string SerialNo { get; set; }

    }
}
