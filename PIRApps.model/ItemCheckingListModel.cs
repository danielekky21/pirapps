﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class ItemCheckingListModel
    {
        public string Subportofolio { get; set; }
        public string ItemCheckingCode { get; set; }
        public string ItemDescription { get; set; }
        public string AreaCode { get; set; }
        public string TypeArea { get; set; }
        public string Condition { get; set; }
        public bool? Status { get; set; }
    }
}
