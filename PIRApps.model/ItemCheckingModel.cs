﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class ItemCheckingModel
    {
        public string subportofolio { get; set; }
        public string itemCheckingCode { get; set; }
        public string itemCheckingDescription { get; set; }
        public string ItemAreaCode { get; set; }
        public string TypeArea { get; set; }
        public string Condition { get; set; }
        public string Status { get; set; }

    }
}
