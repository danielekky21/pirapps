﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class LogsheetListModel
    {
        public string LogsheetID { get; set; }
        public DateTime? tanggal { get; set; }
        public string Shift { get; set; }
        public string Subportofolio { get; set; }
        public string LocationCode { get; set; }
        public string LocationName { get; set; }
        public bool? Status { get; set; }
        public string workflowstatus { get; set; }
        public string Callsign { get; set; }
        public DateTime? endDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? DateEnd { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string ApprovedBy { get; set; }
        public string locationType { get; set; }
        public string locationArea { get; set; }
        public string Floor { get; set; }
        public string SubmitBy { get; set; }
        public DateTime? submitDate { get; set; }
    }
}
