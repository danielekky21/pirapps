﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class ModelAsset
    {
        public string ModelName { get; set; }
        public int ModelID { get; set; }
        public int BrandID { get; set; }
    }
}
