﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class MsChemicalModel
    {
        public string ChemicalCode { get; set; }
        public bool? Status { get; set; }
        public string ChemicalDesc { get; set; }
    }
}
