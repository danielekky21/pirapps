﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class MsEquipmentModel
    {
        public string EquipmentCode { get; set; }
        public string EquipmentDesc { get; set; }
        public bool? Status { get; set; }
    }
}
