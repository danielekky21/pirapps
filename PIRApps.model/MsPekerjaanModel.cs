﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class MsPekerjaanModel
    {
        public string PerkerjaanCode { get; set; }
        public string PekerjaanName { get; set; }
        public string Department { get; set; }
        public bool? Status { get; set; }
    }
}
