﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class MsUserMenuDto
    {
        public MsUserMenuDto()
        {
            getMenuChild = new List<MsUserMenuDto>();
        }
        public string UserName { get; set; }
        public int? MenuId { get; set; }
        public string Menuname { get; set; }
        public int? MenuParent { get; set; }
        public string MenuLink { get; set; }
        public string MenuAction { get; set; }
        public string MenuIcon { get; set; }
        public string CreateData { get; set; }
        public string ReadData { get; set; }
        public string UpdateData { get; set; }
        public string DeleteData { get; set; }
        public string IsShow { get; set; }
        public bool? IsActive { get; set; }
        public string ModulID { get; set; }
        public IList<MsUserMenuDto> getMenuChild { get; set; }
    }
}
