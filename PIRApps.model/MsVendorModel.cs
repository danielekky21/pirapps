﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class MsVendorModel
    {
        public int VendorID { get; set; }
        public string VendorName { get; set; }
        public string NPWP { get; set; }
        public bool? Status { get; set; }
    }
}
