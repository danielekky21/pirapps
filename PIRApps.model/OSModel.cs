﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class OSModel
    {
        public string OSName { get; set; }
        public string OSVersion { get; set; }
        public int OSID { get; set; }
        public string OSVersionDesc { get; set; }
    }
}
