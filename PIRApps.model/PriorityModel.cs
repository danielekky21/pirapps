﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class PriorityModel
    {
        public string PriorityValue { get; set; }
        public string PriorityText { get; set; }
    }
}
