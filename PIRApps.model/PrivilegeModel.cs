﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class PrivilegeModel
    {
        public int? MenuId { get; set; }
        public int? UserId { get; set; }
        public int UserMenuId { get; set; }
        public string CreateData { get; set; }
        public string ReadData { get; set; }
        public string UpdateData { get; set; }
        public string DeleteData { get; set; }
        public string IsDefault { get; set; }
        public string IsAdmin { get; set; }
        public string ModulID { get; set; }
        public string isShow { get; set; }
    }
}
