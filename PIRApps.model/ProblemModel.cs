﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class ProblemModel
    {
        public int ProblemTypeID { get; set; }
        public string ProblemTypeName { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
