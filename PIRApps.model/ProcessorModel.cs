﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class ProcessorModel
    {
        public int processorID { get; set; }
        public string processorName { get; set; }
    }
}
