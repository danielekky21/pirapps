﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class QuotationModel
    {
        public string Subportofolio { get; set; }
        public string QuotationID { get; set; }
        public DateTime? QuotationDate { get; set; }
        public string ShopName { get; set; }
        public string Floor { get; set; }
        public string Unit { get; set; }
        public decimal? NettPrice { get; set; }
        public string Status { get; set; }
        public string NPWP { get; set; }
        public string TenantPIC { get; set; }
        public string ContantNo { get; set; }
        public string Email { get; set; }
        public string QuotationStatus { get; set; }
        public string WORID { get; set; }
    }
}
