﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class ReportFilterModel
    {
        public DateTime RequestDateFrom { get; set; }
        public DateTime RequestDateTo { get; set; }
    }
}
