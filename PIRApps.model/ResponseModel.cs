﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class ResponseModel
    {
        public ResponseModel()  // static ctor
        {
            Success = true;
        }

        public bool Success { get; set; }
        public string ErrorEntity { get; set; }
        public string ErrorMessage { get; set; }
        public string Message { get; set; }
        public bool Continue { get; set; }
        public Guid ResponseID { get; set; }

        public void SetError()
        {
            this.Success = false;
            this.ErrorMessage = string.Empty;
        }

        public void SetError(string message)
        {
            this.Success = false;
            this.ErrorMessage = message;
        }

        public void SetError(Exception ex)
        {
            this.Success = false;
            this.ErrorMessage = ex.Message;
        }

        public void SetSuccess()
        {
            SetSuccess(string.Empty);
        }

        public void SetSuccess(string message)
        {
            this.Success = true;
            this.ErrorMessage = string.Empty;
            this.Message = message;
        }
    }
}
