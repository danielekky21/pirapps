﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class SecConditionModel
    {
        public string ConditionCode { get; set; }
        public string ConditionDesc { get; set; }
        public bool? Status { get; set; }
    }
}
