﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class SecurityLocationListModel
    {
        public string subportfolio { get; set; }
        public string floor { get; set; }
        public string locationname { get; set; }
        public string callsign { get; set; }
        public string locationcode { get; set; }
        public string itemareacode { get; set; }
        public string itemtypecode { get; set; }
        public bool? status { get; set; }
    }
}
