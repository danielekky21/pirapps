﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class TicketMasterModel
    {
        public string TicketNo { get; set; }
        public string TicketType { get; set; }
        public string TicketHeader { get; set; }
        public string TicketSubject { get; set; }
        public string UserId { get; set; }
        public string Departement { get; set; }
        public string Location { get; set; }
        public string PIC { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string Priority { get; set; }
        public string Status { get; set; }
        public int? picid { get; set; }
        public DateTime ComparedDate { get; set; }
    }
}
