﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class TicketStatusModel
    {
        public string statusCode { get; set; }
        public string statusName { get; set; }
    }
}
