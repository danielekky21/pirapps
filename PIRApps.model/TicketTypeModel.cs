﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class TicketTypeModel
    {
        public int TicketTypeID { get; set; }
        public string TicketTypeName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UserGroup { get; set; }

    }
}
