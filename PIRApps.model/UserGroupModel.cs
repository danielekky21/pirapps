﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class UserGroupModel
    {
        public int UserGroupID { get; set; }
        public string UserGroupName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string GroupHead { get; set; }
        public bool? Status { get; set; }
        public string UserGroupCode { get; set; }

    }
}
