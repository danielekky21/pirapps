﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class UserMenuModel
    {
        public int MenuId { get; set; }
        public bool CreateData { get; set; }
        public bool UpdateData { get; set; }
        public bool DeleteData { get; set; }
        public bool ReadData { get; set; }
        public bool IsShow { get; set; }
        public bool isAdmin { get; set; }
    }
}
