﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PIRApps.model
{
    public class UserPrivilegeObj
    {
        string CanEdit { get; set; }
        string CanInsert { get; set; }
        string CanRead { get; set; }
        string CanDelete { get; set; }

    }
}
