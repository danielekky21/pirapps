﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PIRApps
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                  name: "ITAssetPartial",
                  url: "ITAsset/ViewAssetDetailPartial/{*keyno}",
                  defaults: new { controller = "ITAsset", action = "ViewAssetDetailPartial", keyno = UrlParameter.Optional }
              );
            //routes.MapRoute(
            //     name: "TicketPartial",
            //     url: "Ticket/ViewTicketDetailPartial/{*slug}",
            //     defaults: new { controller = "Ticket", action = "ViewTicketDetailPartial", slug = UrlParameter.Optional }
            // );
            routes.MapRoute(
               name: "viewTaskRoute",
               url: "Ticket/ViewTask/{userid}",
               defaults: new { controller = "Ticket", action = "ViewTask", userid = "" }
           );
            //  routes.MapRoute(
            //    name: "TicketAddEditRoute",
            //    url: "Ticket/TicketAddEdit/{ticketNo}",
            //    defaults: new { controller = "Ticket", action = "TicketAddEdit", ticketNo = "" }
            //);
            routes.MapRoute(
               name: "workflowApproveRoute",
               url: "WorkFlow/SetApproveWorkflow/{WorkFlowid}",
               defaults: new { controller = "WorkFlow", action = "SetApproveWorkflow", WorkFlowid = "" }
           );
            routes.MapRoute(
              name: "workflowDetailRoute",
              url: "WorkFlow/WorkFlowDetail/{WorkFlowid}",
              defaults: new { controller = "WorkFlow", action = "WorkFlowDetail", WorkFlowid = "" }
          );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Login", id = UrlParameter.Optional }
            );




        }
    }
}
