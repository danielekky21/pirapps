﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Areas.HouseKeeping.Controllers
{
    [PrivilegeSession]
    public partial class ChemicalController : BaseController
    {
        // GET: HouseKeeping/Chemical
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.UserGroupModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<MsChemicalModel> dataItem = new List<MsChemicalModel>();
            int pageSize = rows;
            dataItem = MsChemical.MapChemicalToModel().ToList();
            //if (!string.IsNullOrEmpty(model.))
            //{

            //}
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult AddEdit(string id)
        {
            var currentData = MsChemical.GetChemicalByID(id);
            if (currentData != null)
            {
                ViewData.Model = currentData;
            }
            else
            {
                ViewData.Model = new MsChemical();
            }
            SelectLists selectList = new SelectLists();
            ViewBag.Status = selectList.GetActiveSelectList().ToList();
        
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(MsChemical model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!string.IsNullOrEmpty(model.ChemicalCode))
            {
                var currentData = MsChemical.GetChemicalByID(model.ChemicalCode);
                if (currentData != null)
                {
                    //update
                    currentData.ChemicalDesc = model.ChemicalDesc;
                    currentData.IsActive = model.IsActive;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdatedBy = userInfo.UserName;
                    currentData.UpdateSave<MsChemical>();


                }
                else
                {
                    //insert
                    MsChemical newData = new MsChemical();
                    var dataAll = MsChemical.MsChemicalCount();
                    var count = dataAll + 1;

                    newData.ChemicalCode = "CH/" + count;
                    newData.ChemicalDesc = model.ChemicalDesc;
                    newData.IsActive = model.IsActive;
                    newData.UpdatedBy = userInfo.UserName;
                    newData.UpdatedDate = DateTime.Now;
                }
            }
            return RedirectToAction(MVC.HouseKeeping.Chemical.List());
        }

    }
}