﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Areas.HouseKeeping.Controllers
{
    [PrivilegeSession]
    public partial class EquipmentController : BaseController
    {
        // GET: HouseKeeping/Equipment
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.UserGroupModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<MsEquipmentModel> dataItem = new List<MsEquipmentModel>();
            int pageSize = rows;
            dataItem = MsEquipment.MapEquipmentToModel().ToList();
            //if (!string.IsNullOrEmpty(model.))
            //{

            //}
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult AddEdit(string id)
        {
            var currentData = MsEquipment.GetAllEquipmentByID(id);
            if (currentData != null)
            {
                ViewData.Model = currentData;
            }
            else
            {
                ViewData.Model = new MsEquipment();
            }
            SelectLists selectList = new SelectLists();
            ViewBag.Status = selectList.GetActiveSelectList().ToList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(MsEquipment model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!string.IsNullOrEmpty(model.EquipmentCode))
            {
                var currentData = MsEquipment.GetAllEquipmentByID(model.EquipmentCode);
                if (currentData != null)
                {
                    //update
                    currentData.EquipmentDesc = model.EquipmentCode;
                    currentData.IsActive = model.IsActive;
                    currentData.CreatedDate = DateTime.Now;
                    currentData.CreatedBy = userInfo.UserName;
                    currentData.UpdateSave<MsEquipment>();
                }
            }
            else
            {
                //insert
                MsEquipment newData = new MsEquipment();
                var equipmentData = MsEquipment.MeEquipmentCount();
                var count = equipmentData + 1;
                newData.EquipmentCode = "EQ/" + count;
                newData.EquipmentDesc = model.EquipmentDesc;
                newData.IsActive = model.IsActive;
                newData.CreatedDate = DateTime.Now;
                newData.CreatedBy = userInfo.UserName;
                newData.InsertSave<MsEquipment>();
            }

            return RedirectToAction(MVC.HouseKeeping.Equipment.List());
        }
    }
}