﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Areas.HouseKeeping.Controllers
{
    [PrivilegeSession]
    public partial class GroupPekerjaanController : BaseController
    {
        // GET: HouseKeeping/GroupPekerjaan
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.UserGroupModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<GroupPekerjaanHeaderModel> dataItem = new List<GroupPekerjaanHeaderModel>();
            int pageSize = rows;
            dataItem = GroupPekerjaanHeader.MapPekerjaanHeaderToModel().ToList();
            //if (!string.IsNullOrEmpty(model.))
            //{

            //}
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult AddEdit(string id)
        {
            UserTb userInfo = (UserTb)Session["User"];
            var currentData = GroupPekerjaanHeader.GetPekerjaanHeaderByID(id);
            if (currentData != null)
            {
                ViewData.Model = currentData;
            }
            else
            {
               var newData = new GroupPekerjaanHeader();
                newData.IsManPower = false;
                newData.IsChemical = false;
                ViewData.Model = newData;
            }
            SelectLists selectList = new SelectLists();
            ViewBag.subportfolios = selectList.Subportfolio().ToList();
            ViewBag.categorypekerjaans = selectList.CategoryPekerjaan().ToList();
            ViewBag.dept = selectList.GetDepatSelectList().ToList();
            ViewBag.ItemPekerjaan = MsPekerjaan.GetAllPerkerjaan().Where(x => x.IsActive == true).ToList();
            ViewBag.ItemUsed = GroupPekerjaanHeader.GetDetailItemByHeaderID(id).ToList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(GroupPekerjaanHeader model, string[] mutliselect_to)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!string.IsNullOrEmpty(model.GroupPekerjaanCode))
            {
                //update
                var currentData = GroupPekerjaanHeader.GetPekerjaanHeaderByID(model.GroupPekerjaanCode);
                if (currentData != null)
                {
                    currentData.GroupPekerjaanName = model.GroupPekerjaanName;
                    currentData.CategoryPekerjaan = model.CategoryPekerjaan;
                    currentData.Subportfolio = model.Subportfolio;
                    currentData.IsChemical = model.IsChemical;
                    currentData.IsManPower = model.IsManPower;
                    currentData.Departement = model.Departement;
                    currentData.UpdateSave<GroupPekerjaanHeader>();
                    var currentGroupData = GroupPekerjaanHeader.GetDetailItemByHeaderID(model.GroupPekerjaanCode).ToList();
                    foreach (var item in currentGroupData)
                    {
                        item.Delete<GroupPekerjaanDetail>();
                    }
                    if (mutliselect_to != null)
                    {
                        for (int i = 0; i < mutliselect_to.Count(); i++)
                        {
                            GroupPekerjaanDetail detail = new GroupPekerjaanDetail();
                            detail.GroupPekerjaanCode = currentData.GroupPekerjaanCode;
                            detail.ItemPekerjaan = mutliselect_to[i];
                            detail.InsertSave<GroupPekerjaanDetail>();
                        }
                    }
                }
                else
                {
                    //newData
                    GroupPekerjaanHeader newData = new GroupPekerjaanHeader();
                    newData.GroupPekerjaanCode = model.GroupPekerjaanCode;
                    newData.GroupPekerjaanName = model.GroupPekerjaanName;
                    newData.Subportfolio = model.Subportfolio;
                    newData.IsChemical = model.IsChemical;
                    newData.IsManPower = model.IsChemical;
                    newData.CategoryPekerjaan = model.CategoryPekerjaan;
                    newData.Departement = model.Departement;
                    newData.IsActive = true;
                    newData.InsertSave<GroupPekerjaanHeader>();
                    var currentGroupData = GroupPekerjaanHeader.GetDetailItemByHeaderID(model.GroupPekerjaanCode).ToList();
                    foreach (var item in currentGroupData)
                    {
                        item.Delete<GroupPekerjaanDetail>();
                    }
                    if (mutliselect_to != null)
                    {
                        for (int i = 0; i < mutliselect_to.Count(); i++)
                        {
                            GroupPekerjaanDetail detail = new GroupPekerjaanDetail();
                            detail.GroupPekerjaanCode = newData.GroupPekerjaanCode;
                            detail.ItemPekerjaan = mutliselect_to[i];
                            detail.InsertSave<GroupPekerjaanDetail>();
                        }
                    }
                }
            }

            return RedirectToAction(MVC.HouseKeeping.GroupPekerjaan.List());

        }
       
    }
}