﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Areas.HouseKeeping.Controllers
{
    [PrivilegeSession]
    public class HKPrintOutController : BaseController
    {
        // GET: HouseKeeping/HKPrintOut
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.UserGroupModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<QuotationModel> dataItem = new List<QuotationModel>();
            int pageSize = rows;
            dataItem = Quotation.MapToQuotationModel().ToList();
            //if (!string.IsNullOrEmpty(model.))
            //{

            //}
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}