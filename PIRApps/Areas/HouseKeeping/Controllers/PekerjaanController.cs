﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Areas.HouseKeeping.Controllers
{
    [PrivilegeSession]
    public partial class PekerjaanController : BaseController
    {
        // GET: HouseKeeping/Pekerjaan
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        public virtual ActionResult AddEdit(string code)
         {
            var currentData = MsPekerjaan.GetPekerjaanByID(code);
            if (currentData != null)
            {
                ViewData.Model = currentData;
            }
            else
            {
                ViewData.Model = new MsPekerjaan();
            }
            SelectLists selectList = new SelectLists();
            ViewBag.Status = selectList.GetActiveSelectList().ToList();
            ViewBag.Dept = selectList.GetDepatSelectList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(MsPekerjaan model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!string.IsNullOrEmpty(model.PekerjaanCode))
            {
                var currentData = MsPekerjaan.GetPekerjaanByID(model.PekerjaanCode);
                if (currentData != null)
                {
                    currentData.PekerjaanName = model.PekerjaanName;
                    currentData.Departement = model.Departement;
                    currentData.BasicPrice = model.BasicPrice;
                    currentData.Foreman = model.Foreman;
                    currentData.Cleaner = model.Cleaner;
                    currentData.UpdatedBy = userInfo.UserName;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.IsActive = model.IsActive;
                    currentData.UpdateSave<MsPekerjaan>();
                }

            }
            else
            {
                MsPekerjaan newData = new MsPekerjaan();
                var totalPekerjaan = MsPekerjaan.MsPekerjaanCount();
                var count = totalPekerjaan + 1;
                newData.PekerjaanCode = "IP/" + count;
                newData.PekerjaanName = model.PekerjaanName;
                newData.Departement = model.Departement;
                newData.BasicPrice = model.BasicPrice;
                newData.Foreman = model.Foreman;
                newData.Cleaner = model.Cleaner;
                newData.CreatedBy = userInfo.UserName;
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = model.IsActive;
                newData.InsertSave<MsPekerjaan>();

            }
            return RedirectToAction(MVC.HouseKeeping.Pekerjaan.List());
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.UserGroupModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<MsPekerjaanModel> dataItem = new List<MsPekerjaanModel>();
            int pageSize = rows;
            dataItem = MsPekerjaan.MapPekerjaanToModel().ToList();
            //if (!string.IsNullOrEmpty(model.))
            //{

            //}
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}