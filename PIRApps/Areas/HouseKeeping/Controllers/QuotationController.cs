﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Areas.HouseKeeping.Controllers
{
    [PrivilegeSession]
    public partial class QuotationController : BaseController
    {
        // GET: HouseKeeping/Quotation
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.UserGroupModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<QuotationModel> dataItem = new List<QuotationModel>();
            int pageSize = rows;
            dataItem = Quotation.MapToQuotationModel().ToList();
            //if (!string.IsNullOrEmpty(model.))
            //{

            //}
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult AjaxGetShopData(string shopName)
        {
            var data = TenantMaster.GetTenantMasterByName(shopName);
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult AjaxGetGroupPekData(string pekCode)
        {
            var data = GroupPekerjaanHeader.GetPekerjaanHeaderByID(pekCode);
            var jsonData = new
            {
                CategoryPekerjaan = data.CategoryPekerjaan,
                TypeOfWork = data.GroupPekerjaanName
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

        public virtual ActionResult AddEdit(string code)
        {
            var currentData = Quotation.GetQuotationByID(code);
            decimal? grandTotal = 0;
            decimal? NettPrice = 0;
            if (currentData != null)
            {
                ViewData.Model = currentData;
            }
            else
            {
                var newData = new Quotation();
                newData.CreatedDate = DateTime.Now;
                newData.QuotationStatus = "CREATED BY HK";
                ViewData.Model = newData;
            }
            SelectLists selectList = new SelectLists();
            var quotationpekerjaan = Quotation.GetQitemPekerjaanByQID(code).ToList();
            if (quotationpekerjaan.Count > 0)
            {
                foreach (var item in quotationpekerjaan)
                {
                    if (item.TotalPrice != null)
                    {
                        grandTotal = grandTotal + item.TotalPrice;
                    }

                }
            }
            if (currentData.NettPrice != 0)
            {
                NettPrice = currentData.NettPrice;
            }
            else
            {
                NettPrice = grandTotal;
            }
            ViewBag.shop = selectList.ShopNameSeleclist().ToList();
            ViewBag.pekGroup = selectList.GroupPekerjaanCodeToSelectList().ToList();
            ViewBag.pek = selectList.ItemPekerjaanToSelectList().ToList();
            ViewBag.equipmentAll = MsEquipment.GetAllEquipment().Where(x => x.IsActive == true).ToList();
            ViewBag.equipmentUsed = Quotation.GetAllQuotationEquipment().ToList();
            ViewBag.QuotationPekerjaans = quotationpekerjaan;
            ViewBag.chemicalAll = MsChemical.GetAllChemical().ToList();
            ViewBag.grandTotal = grandTotal;
            ViewBag.NettPrice = NettPrice;
            ViewBag.subp = selectList.Subportfolio().ToList();

            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(Quotation model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!string.IsNullOrEmpty(model.QuotationID))
            {
                var currentData = Quotation.GetQuotationByID(model.QuotationID);

                if (currentData != null)
                {
                    //update
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.Subportofolio = model.Subportofolio;
                    currentData.UpdatedBy = userInfo.UserName;
                    currentData.ShopName = model.ShopName;
                    currentData.CompanyName = model.CompanyName;
                    currentData.CompanyAddress = model.CompanyAddress;
                    currentData.Floor = model.Floor;
                    currentData.Unit = model.Unit;
                    currentData.PIC = model.PIC;
                    currentData.ContactNum = model.ContactNum;
                    currentData.Email = model.Email;
                    currentData.CreatedBy = userInfo.UserName;
                    currentData.CreatedDate = DateTime.Now;
                    currentData.UpdateSave<Quotation>();
                }

            }
            else
            {
                Quotation newData = new Quotation();
                var quotID = "PIR-HK/QTN/" + DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString("d2");
                quotID = quotID + "/" + Quotation.QuotationCount().ToString().PadLeft(4, '0');
                newData.QuotationID = quotID;
                newData.Subportofolio = model.Subportofolio;
                newData.CreatedDate = DateTime.Now;
                newData.NPWP = model.NPWP;
                newData.CompanyAddress = model.CompanyAddress;
                newData.CompanyName = model.CompanyName;
                newData.ShopName = model.ShopName;
                newData.Floor = model.Floor;
                newData.Unit = model.Unit;
                newData.PIC = model.PIC;
                newData.ContactNum = model.ContactNum;
                newData.Email = model.Email;
                newData.CreatedBy = userInfo.UserName;
                newData.QuotationStatus = "CREATED BY HK";
                newData.IsActive = true;
                newData.InsertSave<Quotation>();
                return RedirectToAction(MVC.HouseKeeping.Quotation.AddEdit(quotID));
            }
            return RedirectToAction(MVC.HouseKeeping.Quotation.AddEdit(model.QuotationID));
        }
        [HttpPost]
        public virtual JsonResult addQuotation(string[] multiselect_to, string[] multiselect2_to, string GroupPekerjaanCode, string CategoryPekerjaan, string PekerjaanCode, int ForemanQTY, int CleanerQTY, int Hours, string QuotationID)
        {
            try
            {
                QuotationItemPekerjaan itemPekerjaan = new QuotationItemPekerjaan();
                var equipmentList = string.Empty;
                var chemicalList = string.Empty;

                //itemPekerjaan.
                var pekerjaanData = MsPekerjaan.GetPekerjaanByID(PekerjaanCode);
                itemPekerjaan.QuotationID = QuotationID;
                itemPekerjaan.PekerjaanGroupCode = GroupPekerjaanCode;
                itemPekerjaan.CategoryPekerjaan = CategoryPekerjaan;
                itemPekerjaan.ItemPekerjaan = PekerjaanCode;
                itemPekerjaan.Foreman = ForemanQTY.ToString();
                itemPekerjaan.Cleaner = CleanerQTY.ToString();
                itemPekerjaan.Hours = Hours;
                itemPekerjaan.SellingPrice = pekerjaanData.BasicPrice;
                itemPekerjaan.TotalPrice = pekerjaanData.BasicPrice + (pekerjaanData.BasicPrice + pekerjaanData.Foreman * decimal.Parse(itemPekerjaan.Foreman) + pekerjaanData.Cleaner * decimal.Parse(itemPekerjaan.Cleaner) * Hours);
                itemPekerjaan.CreatedDate = DateTime.Now;
                itemPekerjaan.InsertSave<QuotationItemPekerjaan>();

                foreach (var item in multiselect2_to)
                {

                    QuotationEquipment eq = new QuotationEquipment();
                    eq.QuotationEquipmentUsed = item;
                    eq.QuotationItemPekerjaan = itemPekerjaan.QuotationItemPekerjaan1;
                    eq.InsertSave<QuotationEquipment>();
                    if (equipmentList == string.Empty)
                    {
                        equipmentList = equipmentList + item;
                    }
                    else
                    {
                        equipmentList = equipmentList + " , " + item;
                    }
                }
                foreach (var item in multiselect_to)
                {
                    QuotationChemical chem = new QuotationChemical();
                    chem.QuotationChemicalUsed = item;
                    chem.QuotationPekerjaanID = itemPekerjaan.QuotationItemPekerjaan1;
                    chem.InsertSave<QuotationChemical>();
                    if (chemicalList == string.Empty)
                    {
                        chemicalList = chemicalList + item;
                    }
                    else
                    {
                        chemicalList = chemicalList + " , " + item;
                    }
                }
                var basicPrice = pekerjaanData.BasicPrice;
                var JsonData = new
                {
                    CategoryPekerjaan = itemPekerjaan.CategoryPekerjaan,
                    itemPekerjaan = itemPekerjaan.ItemPekerjaan,
                    equipmentList = equipmentList,
                    chemicalList = chemicalList,
                    ForemanQTY = itemPekerjaan.Foreman,
                    CleanerQTY = itemPekerjaan.Cleaner,
                    Hours = Hours,
                    basicPrice = basicPrice,
                    TotalPrice = itemPekerjaan.TotalPrice
                };
                return Json(JsonData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                throw;
            }

        }
        public virtual ActionResult UpdateTotal(GrandTotalModel model)
        {
            var data = Quotation.GetQuotationByID(model.QuotationID);
            UserTb userInfo = (UserTb)Session["User"];
            if (data !=null)
            {
                data.GrandTotal = model.GrandTotals;
                data.IsPPN = model.IsPPN;
                data.NettPrice = model.NettPrice;
                data.UpdatedDate = DateTime.Now;
                data.UpdatedBy = userInfo.UserName;
                data.FormStatus = "Final";
                data.UpdateSave<Quotation>();
            }
            return RedirectToAction(MVC.HouseKeeping.Quotation.List());
        }
        public virtual ActionResult GetPdf(string code)
        {
            UserTb userInfo = (UserTb)Session["User"];
            PDFBuilder builder = new PDFBuilder();
            var data = Quotation.GetQtoModelID(code);
            var dataQuot = Quotation.GetQuotationByID(code);
            dataQuot.QuotationStatus = "On Progress To Tenant";
            dataQuot.UpdatedDate = DateTime.Now;
            dataQuot.UpdatedBy = userInfo.UserName;
            dataQuot.UpdateSave<Quotation>();
            builder.PdfBuilder(Server.MapPath("/Areas/HouseKeeping/Views/PDFile/Quotation.cshtml"),Server.MapPath("/Assets/Img/pir-logo.jpg"),data);
            return builder.GetPdf();
        }
        public virtual ActionResult ViewQuotationPartial(string code)
        {
            var currentData = Quotation.GetQuotationByID(code);
            decimal? grandTotal = 0;
            if (currentData != null)
            {
                ViewData.Model = currentData;
            }
            else
            {
                var newData = new Quotation();
                newData.CreatedDate = DateTime.Now;
                newData.QuotationStatus = "CREATED BY HK";
                ViewData.Model = newData;
            }
            SelectLists selectList = new SelectLists();
            var quotationpekerjaan = Quotation.GetQitemPekerjaanByQID(code).ToList();
            if (quotationpekerjaan.Count > 0)
            {
                foreach (var item in quotationpekerjaan)
                {
                    if (item.TotalPrice != null)
                    {
                        grandTotal = grandTotal + item.TotalPrice;
                    }

                }
            }
            ViewBag.shop = selectList.ShopNameSeleclist().ToList();
            ViewBag.pekGroup = selectList.GroupPekerjaanCodeToSelectList().ToList();
            ViewBag.pek = selectList.ItemPekerjaanToSelectList().ToList();
            ViewBag.equipmentAll = MsEquipment.GetAllEquipment().Where(x => x.IsActive == true).ToList();
            ViewBag.equipmentUsed = Quotation.GetAllQuotationEquipment().ToList();
            ViewBag.QuotationPekerjaans = quotationpekerjaan;
            ViewBag.chemicalAll = MsChemical.GetAllChemical().ToList();
            ViewBag.grandTotal = grandTotal;
            ViewBag.subp = selectList.Subportfolio().ToList();
            return PartialView();
        }
    }
}