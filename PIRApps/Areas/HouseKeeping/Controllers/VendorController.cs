﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Areas.HouseKeeping.Controllers
{
    [PrivilegeSession]
    public partial class VendorController : BaseController
    {
        // GET: HouseKeeping/Vendor
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        public virtual ActionResult AddEdit(int id)
        {
            if (id != 0)
            {
                var CurrentData = MsVendor.GetAllVendorByID(id);
                if (CurrentData != null)
                {
                    ViewData.Model = CurrentData;

                }
                else
                {
                    ViewData.Model = new MsVendor();
                }
            }
            else
            {
                ViewData.Model = new MsVendor();
            }
            SelectLists selectList = new SelectLists();
            ViewBag.Status = selectList.GetActiveSelectList().ToList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(MsVendor model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.VendorID != 0)
            {
                var currentData = MsVendor.GetAllVendorByID(model.VendorID);
                if (currentData != null)
                {
                    currentData.VendorName = model.VendorName;
                    currentData.CompanyName = model.CompanyName;
                    currentData.Address = model.Address;
                    currentData.NPWP = model.NPWP;
                    currentData.Phone = model.Phone;
                    currentData.Email = model.Email;
                    currentData.IsActive = model.IsActive;
                    currentData.UpdatedBy = userInfo.UserName;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdateSave<MsVendor>();
                }
            }
            else
            {
                MsVendor newData = new MsVendor();
                newData.VendorName = model.VendorName;
                newData.CompanyName = model.CompanyName;
                newData.Address = model.Address;
                newData.NPWP = model.NPWP;
                newData.Phone = model.Phone;
                newData.Email = model.Email;
                newData.IsActive = model.IsActive;
                newData.CreatedBy = userInfo.UserName;
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = model.IsActive;
                newData.InsertSave<MsVendor>();

                
            }
            return RedirectToAction(MVC.HouseKeeping.Vendor.List());
        }
            
        
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.UserGroupModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<MsVendorModel> dataItem = new List<MsVendorModel>();
            int pageSize = rows;
            dataItem = MsVendor.MapToVendorModel().ToList();
            //if (!string.IsNullOrEmpty(model.))
            //{

            //}
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}