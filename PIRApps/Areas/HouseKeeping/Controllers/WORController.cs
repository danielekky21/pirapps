﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Areas.HouseKeeping.Controllers
{
    [PrivilegeSession]
    public partial class WORController : BaseController
    {
        // GET: HouseKeeping/WOR
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();

        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.UserGroupModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<QuotationModel> dataItem = new List<QuotationModel>();
            int pageSize = rows;
            dataItem = Quotation.MapToQuotationModel().Where(x => x.QuotationStatus == "On Progress To Tenant").ToList();
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult CreateWOR(string code)
        {
            SelectLists selectlist = new SelectLists();
            var data = WOR.getWORbyQuotID(code);
            var QuotData = Quotation.GetQuotationByID(code);
            ViewBag.quotCod = code;
            ViewBag.NettPrices = QuotData.NettPrice;
            ViewBag.paidBy = selectlist.GetPaidBySelectList();
            if (data != null)
            {
                ViewData.Model = data;
            }
            else
            {
                ViewData.Model = new WOR();
            }
            return View();
        }
        public virtual JsonResult CascadeDropdownPaidBy(string value)
        {
            if (value.ToLower() == "vendor")
            {
                var data = MsVendor.MapToDropDownModel().ToList();
                return Json(data, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var data = TenantMaster.MapToDropDownModel().ToList();
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public virtual ActionResult CreateWOR(WOR Model, decimal NettPriceFin)
        {
            UserTb userInfo = (UserTb)Session["User"];
            var count = WOR.countWOR();
            if (count == 0)
            {
                count = 1;
            }
            else
            {
                count = count + 1;
            }
            var ID = "HK/WOR/" + DateTime.Now.Year + "/" + DateTime.Now.Month.ToString("d2");
            var generated = ID + "/" + count.ToString().PadLeft(4, '0');
            WOR worData = new WOR();
            worData.WORID = generated;
            worData.QuotationID = Model.QuotationID;
            worData.Remark = Model.Remark;
            worData.DiscountValue = Model.DiscountValue;
            worData.ShopVendorName = Model.ShopVendorName;
            worData.DiscountRate = Model.DiscountRate;
            worData.Address = Model.Address;
            worData.WorkDateEstimation = Model.WorkDateEstimation;
            worData.WorkDescription = Model.WorkDescription;
            worData.Company = Model.Company;
            worData.PaidBy = Model.PaidBy;
            worData.ApprovalStatus = "S";
            worData.CreatedBy = userInfo.UserName;
            worData.CreatedDate = DateTime.Now;
            worData.InsertSave<WOR>();
            bool isWorkflow = WorkFlowChild.CheckIfWorkFlowExist("Mod-WOR");
            if (isWorkflow)
            {
                WorkFlowTransaction.AddtoWorkFlow("Mod-WOR", "WOR Approval", userInfo.Name, generated, 1);
            }

            return RedirectToAction(MVC.HouseKeeping.WOR.List());
        }
        public virtual ActionResult WORPartial(string keyno)
        {
            SelectLists selectlist = new SelectLists();
            var data = WOR.getWORByID(keyno);
            var QuotData = Quotation.GetQuotationByID(data.QuotationID);
            ViewBag.quotCod = QuotData.QuotationID;
            ViewBag.NettPrices = QuotData.NettPrice;
            ViewBag.paidBys = selectlist.GetPaidBySelectList();
            if (data != null)
            {
                ViewData.Model = data;
            }
            else
            {
                ViewData.Model = new WOR();
            }
            return PartialView();
        }
    }
}