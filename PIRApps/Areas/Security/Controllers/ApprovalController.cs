﻿using PIRApps.common;
using PIRApps.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Areas.Security.Controllers
{
    [PrivilegeSession]
    public partial class ApprovalController : BaseController
    {
        // GET: Security/Approval
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            UserTb userInfo = (UserTb)Session["User"];
            ViewData.Model = SecurityApproval.GetApprovalByUserID(userInfo.UserID).OrderByDescending(x => x.CreatedDate).ToList();
            return View();
        }
        public virtual ActionResult ViewApprovalDetail(int approvalid)
        {
            UserTb userInfo = (UserTb)Session["User"];
            var approvalData = SecurityApproval.GetApprovalByID(approvalid);
            var logsheet = SecurityLogsheet.GetSecurityLogsheetByID(approvalData.FirstOrDefault().ApprovalFormID);
            ViewData.Model = approvalData.FirstOrDefault(x => x.ApproverID == userInfo.UserID);
            ViewBag.checklistdata = SecurityLogsheet.GetLogSheetChecklistByLogsheetID(logsheet.LogsheetID);
            ViewBag.PICName = userInfo.Name;
            var conditionCode = SecurityLogsheet.GetConditionByLogsheetID(logsheet.LogsheetID).ToList();
            var conds = new List<MasterConditionSecurity>();
            foreach (var item in conditionCode)
            {
                var condi = new MasterConditionSecurity();
                condi = MasterConditionSecurity.GetConditionByID(item);
                conds.Add(condi);
            }
            ViewBag.Logsheet = logsheet;
            ViewBag.LogsheetImg = SecurityLogsheet.GetLogsheetImagebyID(logsheet.LogsheetID).ToList();
            ViewBag.condition = conds;
            ViewBag.securityItem = SecurityLogsheet.GetItemByLogSheetID(logsheet.LogsheetID).ToList();
            ViewBag.Logsheetcheck = SecurityLogsheet.GetLogSheetChecklistByLogsheetID(logsheet.LogsheetID).ToList();
            return View();
        }
        public virtual ActionResult ApproveApproval(string approvalid)
        {
            UserTb userInfo = (UserTb)Session["User"];
            var data = SecurityApproval.GetApprovalByFormID(approvalid);
            if (data != null)
            {
                foreach (var item in data)
                {
                    if (item.ApproverID == userInfo.UserID)
                    {
                        item.ApprovalStatus = "APPROVED";
                        item.ApprovalRemark = "";
                        item.UpdateDate = DateTime.Now;
                        item.UpdatedBy = userInfo.UserName;
                        item.UpdateSave<SecurityApproval>();
                    }
                    else
                    {
                        item.ApprovalStatus = "APPROVEDBY";
                        item.ApprovalRemark = "";
                        item.UpdateDate = DateTime.Now;
                        item.UpdatedBy = userInfo.UserName;
                        item.UpdateSave<SecurityApproval>();
                    }
                   
                }
             
            }
            
            return RedirectToAction(MVC.Security.Approval.List());
        }
        public virtual ActionResult ApproveOrRejectApproval(SecurityApproval model, string command)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (command == "approve")
            {

                var approval = SecurityApproval.GetApprovalByFormID(model.ApprovalFormID).ToList();
                if (approval != null)
                {
                    foreach (var item in approval)
                    {
                        if (item.ApproverID == userInfo.UserID)
                        {
                            item.ApprovalStatus = "APPROVED";
                            item.ApprovalRemark = model.ApprovalRemark;
                            item.UpdateDate = DateTime.Now;
                            item.UpdatedBy = userInfo.UserName;
                            item.UpdateSave<SecurityApproval>();
                        }
                        else
                        {
                            item.ApprovalStatus = "APPROVEDBY";
                            item.ApprovalRemark = model.ApprovalRemark;
                            item.UpdateDate = DateTime.Now;
                            item.UpdatedBy = userInfo.UserName;
                            item.UpdateSave<SecurityApproval>();
                        }
                        
                    }
                    //update logsheet
                    var logsheet = SecurityLogsheet.GetSecurityLogsheetByID(approval.FirstOrDefault().ApprovalFormID);
                    logsheet.WorkFlowStatus = "APPROVED";
                    logsheet.UpdatedBy = userInfo.UserName;
                    logsheet.UpdatedDate = DateTime.Now;
                    logsheet.UpdateSave<SecurityLogsheet>();
                }
            }
            if (command == "reject")
            {
                var approval = SecurityApproval.GetApprovalByFormID(model.ApprovalFormID).ToList();
                if (approval != null)
                {
                    foreach (var item in approval)
                    {
                        if (item.ApproverID == userInfo.UserID)
                        {
                            item.ApprovalStatus = "REJECTED";
                            item.ApprovalRemark = model.ApprovalRemark;
                            item.UpdateDate = DateTime.Now;
                            item.UpdatedBy = userInfo.UserName;
                            item.UpdateSave<SecurityApproval>();
                        }
                        else
                        {
                            item.ApprovalStatus = "REJECTEDBY";
                            item.ApprovalRemark = model.ApprovalRemark;
                            item.UpdateDate = DateTime.Now;
                            item.UpdatedBy = userInfo.UserName;
                            item.UpdateSave<SecurityApproval>();
                        }
                    }
                    //update logsheet
                    var logsheet = SecurityLogsheet.GetSecurityLogsheetByID(approval.FirstOrDefault().ApprovalFormID);
                    logsheet.WorkFlowStatus = "DRAFT";
                    logsheet.UpdatedBy = userInfo.UserName;
                    logsheet.UpdatedDate = DateTime.Now;
                    logsheet.UpdateSave<SecurityLogsheet>();
                }
            }
            return RedirectToAction(MVC.Security.Approval.List());
        }
    }
}