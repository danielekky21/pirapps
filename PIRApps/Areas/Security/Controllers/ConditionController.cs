﻿using PIRApps.common;
using PIRApps.model;
using PIRApps.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Areas.Security.Controllers
{
    [PrivilegeSession]
    public partial class ConditionController : BaseController
    {
        // GET: Security/Condition
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult ConditionList()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home",new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.SecConditionModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<MasterConditionSecurity> dataCondition = new List<MasterConditionSecurity>();
            int pageSize = rows;
            dataCondition = MasterConditionSecurity.GetAllCondition().OrderByDescending(x => x.CreatedDate).ToList();
            if (!string.IsNullOrEmpty(model.ConditionDesc))
            {
                dataCondition = dataCondition.Where(x => x.ConditionDesc.ToLower().Contains(model.ConditionDesc.ToLower())).ToList();
            }
            List<SecConditionModel> ListNewMod = new List<SecConditionModel>();
            foreach (var item in dataCondition)
            {
                SecConditionModel newMod = new SecConditionModel();
                newMod.ConditionCode = item.ConditionCode;
                newMod.ConditionDesc = item.ConditionDesc;
                newMod.Status = item.IsActive;
                ListNewMod.Add(newMod);
            }
            var rowSize = dataCondition.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = ListNewMod.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult ConditionAddEdit(string ConditionCode)
        {
            if (!string.IsNullOrEmpty(ConditionCode))
            {
                var CondList = MasterConditionSecurity.GetConditionByID(ConditionCode);
                if (CondList != null)
                {
                    ViewData.Model = CondList;
                }
                else
                {
                    ViewData.Model = new MasterConditionSecurity();
                }
            }
            else
            {
                ViewData.Model = new MasterConditionSecurity();
            }
            SelectLists selectList = new SelectLists();
            ViewBag.Status = selectList.GetActiveSelectList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult ConditionAddEdit(MasterConditionSecurity model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!string.IsNullOrEmpty(model.ConditionCode))
            {
                var conditiondata = MasterConditionSecurity.GetConditionByID(model.ConditionCode);
                if (conditiondata != null)
                {
                    conditiondata.ConditionDesc = model.ConditionDesc;
                    conditiondata.UpdatedDate = DateTime.Now;
                    conditiondata.UpdatedBy = userInfo.UserName;
                    conditiondata.IsActive = model.IsActive;
                    conditiondata.UpdateSave<MasterConditionSecurity>();
                }
                else
                {
                    var newData = new MasterConditionSecurity();
                    newData.ConditionCode = model.ConditionCode;
                    newData.ConditionDesc = model.ConditionDesc;
                    newData.CreatedBy = userInfo.UserName;
                    newData.CreatedDate = DateTime.Now;
                    newData.IsActive = model.IsActive;
                    newData.InsertSave<MasterConditionSecurity>();
                }
            }
            return RedirectToAction(MVC.Security.Condition.ConditionList());
        }
    }
}