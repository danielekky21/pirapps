﻿using PIRApps.common;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data;
namespace PIRApps.Areas.Security.Controllers
{
    [PrivilegeSession]
    public partial class ItemCheckingController : BaseController
    {
        // GET: Security/ItemChecking
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult ItemCheckingList()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(ItemCheckingListModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<ItemCheckingListModel> dataItem = new List<ItemCheckingListModel>();
            int pageSize = rows;
            dataItem = ItemChecking.MapToItemcheckingModel().ToList();
            if (!string.IsNullOrEmpty(model.Subportofolio))
            {
                dataItem = dataItem.Where(x => x.Subportofolio == model.Subportofolio).ToList();
            }
            if (!string.IsNullOrEmpty(model.TypeArea))
            {
                dataItem = dataItem.Where(x => x.TypeArea.ToLower().Contains(model.TypeArea.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(model.AreaCode))
            {
                dataItem = dataItem.Where(x => x.AreaCode.ToLower().Contains(model.AreaCode.ToLower())).ToList();
            }
            //if (!string.IsNullOrEmpty(model.))
            //{

            //}
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult AddEdit(string id)
        { 

            if (!string.IsNullOrEmpty(id))
            {
                var data = ItemChecking.GetItemCheckingFromID(id);
                
                if (data != null)
                {
                    ViewData.Model = data;
                }
            }
            else
            {
                var data = new ItemChecking();
                //int count = 0;
                //var itemchecking = ItemChecking.GetAllItemChecking().Count();
                //if (itemchecking == 0)
                //{
                //    count = 1;
                //}
                //else
                //{
                //    count = itemchecking;
                //}
                //var id  = 
                ViewData.Model = data;

            }
            SelectLists selectList = new SelectLists();
            ViewBag.condition = selectList.getConditionSelectList().ToList();
            ViewBag.Status = selectList.GetActiveSelectList().ToList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(ItemChecking model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!String.IsNullOrEmpty(model.ItemCheckingCode))
            {
                var currentData = ItemChecking.GetItemCheckingFromID(model.ItemCheckingCode);
                if (currentData != null)
                {
                    currentData.Subportofolio = model.Subportofolio;
                    currentData.AreaCode = model.AreaCode;
                    currentData.TypeArea = model.TypeArea;
                    currentData.ItemDescription = model.ItemDescription;
                    currentData.IsActive = model.IsActive;
                    currentData.UpdateDate = DateTime.Now;
                    currentData.UpdateBy = userInfo.UserName;
                    currentData.UpdateSave<ItemChecking>();
                }
            }
            else
            {
                int count = 1;
                var itemchecking = ItemChecking.GetAllItemChecking().Count();
                if (itemchecking == 0)
                {
                    count = 1;
                }
                else
                {
                    count = count + itemchecking;
                }
                var id = model.Subportofolio + "-" + model.AreaCode + "-" + model.TypeArea + "-" + count.ToString().PadLeft(4, '0');
                var newData = new ItemChecking();
                newData.ItemCheckingCode = id;
                newData.ItemDescription = model.ItemDescription;
                newData.Subportofolio = model.Subportofolio;
                newData.AreaCode = model.AreaCode;
                newData.TypeArea = model.TypeArea;
                newData.ConditionCode = model.ConditionCode;
                newData.IsActive = model.IsActive;
                newData.CreatedDate = DateTime.Now;
                newData.CreatedBy = userInfo.UserName;
                newData.InsertSave<ItemChecking>();
            }
            return RedirectToAction(MVC.Security.ItemChecking.ItemCheckingList());
        }
        public virtual ActionResult conditioList(string itemcheckingid)
        {
            ViewData.Model = MasterConditionSecurity.GetAllCondition().ToList();
            ViewBag.itemcheck = itemcheckingid;
            return View();
        }
        public virtual ActionResult itemAddEdit(string itemcheckingid,string condition)
        {
            if (!string.IsNullOrEmpty(itemcheckingid))
            {
                var data = ItemChecking.GetItemsCheckingByCheckingCodeAndCondition(itemcheckingid, condition);
                if (data.Count() > 0)
                {
                    var currmodel = new ItemCheckingItem();
                    currmodel.ItemCheckingCode = itemcheckingid;
                    currmodel.Conditioncode = data.FirstOrDefault().Conditioncode;
                    ViewData.Model = currmodel;
                    ViewBag.data = data.ToList();
                }
                else
                {
                    var model = new ItemCheckingItem();
                    model.ItemCheckingCode = itemcheckingid;
                    ViewData.Model = model;
                    ViewBag.data = new List<ItemCheckingItem>();
                }
                SelectLists selectList = new SelectLists();
                ViewBag.condition = selectList.getConditionSelectList().ToList();
                ViewBag.code = itemcheckingid;
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult itemAddEdit(string[] item, string itemCheckingCode ,string Conditioncode)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!string.IsNullOrEmpty(itemCheckingCode))
            {
                var data = ItemChecking.GetItemsCheckingByCheckingCodeAndCondition(itemCheckingCode,Conditioncode).ToList();
                if (data != null)
                {
                    foreach (var items in data)
                    {
                        items.Delete<ItemCheckingItem>();
                    }
                }
                foreach (var it in item)
                {
                    ItemCheckingItem model = new ItemCheckingItem();
                    model.ItemCheckingCode = itemCheckingCode;
                    model.Conditioncode = Conditioncode;
                    model.CreatedBy = userInfo.Name;
                    model.CreatedDate = DateTime.Now;
                    model.IsActive = true;
                    model.Item = it;
                    model.InsertSave<ItemCheckingItem>();
                }
            }
            return RedirectToAction(MVC.Security.ItemChecking.ItemCheckingList());
        }
    }
}