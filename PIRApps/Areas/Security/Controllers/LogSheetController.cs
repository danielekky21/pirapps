﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Areas.Security.Controllers
{
    [PrivilegeSession]
    public partial class LogSheetController : BaseController
    {
        // GET: Security/LogSheet
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        public virtual ActionResult LogsheetHistory()
        {
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.LogsheetListModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<LogsheetListModel> dataItem = new List<LogsheetListModel>();

            int pageSize = rows;
            dataItem = SecurityLogsheet.MapLogsheetToModel().ToList();
            if (!String.IsNullOrEmpty(model.LogsheetID))
            {
                dataItem = dataItem.Where(x => x.LogsheetID.Contains(model.LogsheetID)).ToList();
            }
            if (!string.IsNullOrEmpty(model.LocationName))
            {
                dataItem = dataItem.Where(x => x.LocationName.ToLower().Contains(model.LocationName.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(model.Callsign))
            {
                dataItem = dataItem.Where(x => x.Callsign.ToLower().Contains(model.Callsign.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(model.StartDate.ToString()))
            {
                if (model.DateEnd == null)
                {
                    dataItem = dataItem.Where(x => x.tanggal >= model.StartDate).ToList();
                }
                else
                {
                    dataItem = dataItem.Where(x => x.tanggal >= model.StartDate && x.tanggal <= model.DateEnd).ToList();
                }
            }
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONDataHistory(model.LogsheetListModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<LogsheetListModel> dataItem = new List<LogsheetListModel>();
            int pageSize = rows;
            dataItem = SecurityLogsheet.MapLogsheetToModelhistory().Where(x => x.workflowstatus != "DRAFT").ToList();
            if (!String.IsNullOrEmpty(model.LogsheetID))
            {
                dataItem = dataItem.Where(x => x.LogsheetID.Contains(model.LogsheetID)).ToList();
            }
            if (!string.IsNullOrEmpty(model.LocationName))
            {
                dataItem = dataItem.Where(x => x.LocationName.ToLower().Contains(model.LocationName.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(model.Callsign))
            {
                dataItem = dataItem.Where(x => x.Callsign.ToLower().Contains(model.Callsign.ToLower())).ToList();
            }
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data.OrderByDescending(x => x.endDate)
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult EditLogsheet(string id,string reff)
        {
            UserTb userInfo = (UserTb)Session["User"];
            var logsheet = SecurityLogsheet.GetSecurityLogsheetByID(id);
            ViewData.Model = logsheet;
            ViewBag.checklistdata = SecurityLogsheet.GetLogSheetChecklistByLogsheetID(id);
            ViewBag.PICName = userInfo.Name;
            var conditionCode = SecurityLogsheet.GetConditionByLogsheetID(id).ToList();
            var conds = new List<MasterConditionSecurity>();
            foreach (var item in conditionCode)
            {
                var condi = new MasterConditionSecurity();
                condi = MasterConditionSecurity.GetConditionByID(item);
                conds.Add(condi);
            }
            ViewBag.LogsheetImg = SecurityLogsheet.GetLogsheetImagebyID(id).ToList();
            ViewBag.condition = conds;
            ViewBag.reff = reff;
            ViewBag.securityItem = SecurityLogsheet.GetItemByLogSheetID(id).ToList();
            ViewBag.Logsheetcheck = SecurityLogsheet.GetLogSheetChecklistByLogsheetID(id).ToList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult EditLogsheet(SecurityLogsheet model, FormCollection frm, IEnumerable<HttpPostedFileBase> File,string reff)
        {
            try
            {


                UserTb userInfo = (UserTb)Session["User"];

                //update header
                var logsheetForm = SecurityLogsheet.GetSecurityLogsheetByID(model.LogsheetID);
                logsheetForm.WorkFlowStatus = "SUBMIT";
                logsheetForm.SecurityName = userInfo.Name;
                logsheetForm.UpdatedDate = DateTime.Now;
                logsheetForm.UpdatedBy = userInfo.UserName;
                logsheetForm.UpdateSave<SecurityLogsheet>();
                //update logsheet item
                var logsheetchecklist = SecurityLogsheet.GetLogsheetChecklistByID(model.LogsheetID).ToList();
                if (logsheetchecklist != null)
                {
                    foreach (var item in logsheetchecklist)
                    {
                        item.Delete<LogsheetChecklist>();
                    }
                }
                var itemcheck = SecurityLogsheet.GetItemByLogSheetID(model.LogsheetID);
                for (int i = 0; i < itemcheck.Count(); i++)
                {
                    var check = frm["check" + itemcheck[i].ItemCheckingItem1];
                    if (check != null)
                    {

                        var logsheetcheck = new LogsheetChecklist();
                        logsheetcheck.ConditionCode = itemcheck[i].Conditioncode;
                        logsheetcheck.LogsheetID = model.LogsheetID;
                        logsheetcheck.ItemChecking = itemcheck[i].ItemCheckingItem1;
                        logsheetcheck.IsChecked = true;
                        logsheetcheck.CreatedBy = userInfo.UserName;
                        logsheetcheck.Remark = frm["remark" + itemcheck[i].ItemCheckingItem1];
                        logsheetcheck.CreatedDate = DateTime.Now;
                        logsheetcheck.InsertSave<LogsheetChecklist>();
                    }
                }
                if (File.Count() > 0)
                {
                    var logimg = SecurityLogsheet.GetLogsheetImagebyID(model.LogsheetID).ToList();
                    if (logimg != null)
                    {
                        foreach (var item in logimg)
                        {
                            item.Delete<LogsheetImage>();
                        }
                    }
                    foreach (var item in File)
                    {
                        if (item != null)
                        {
                            string UploadPath = "/assets/Image/securitylogsheet/";
                            if (File == null) continue;
                            var filename = Regex.Replace(item.FileName.Replace(' ', '_'), "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled) + Guid.NewGuid() + Path.GetExtension(item.FileName);
                            string path = Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), filename);
                            System.Drawing.Image sourceimage = System.Drawing.Image.FromStream(item.InputStream);
                            Image thumb = sourceimage.GetThumbnailImage(500, 500, null, IntPtr.Zero);
                            thumb.Save(Path.Combine(System.Web.Hosting.HostingEnvironment.MapPath(UploadPath), filename));
                            var logsImg = new LogsheetImage();
                            logsImg.Url = UploadPath + filename;
                            logsImg.LogsheetID = model.LogsheetID;
                            logsImg.InsertSave<LogsheetImage>();
                        }
                    }
                }

                //add to workflow
                var usergroup = SecurityLogsheet.GetGroupByShift(logsheetForm.Shift.ToLower());
                if (usergroup.LogsheetGroupHeads.Count > 0)
                {
                    foreach (var item in usergroup.LogsheetGroupHeads)
                    {
                        SecurityApproval approval = new SecurityApproval();
                        approval.ApprovalFormID = model.LogsheetID;
                        approval.ApprovalRemark = "";
                        approval.ApprovalStatus = "SUBMIT";
                        approval.CreatedBy = userInfo.UserName;
                        approval.ApproverID = item.UserId;
                        approval.CreatedDate = DateTime.Now;
                        approval.InsertSave<SecurityApproval>();
                    }
                }
            }
            catch (Exception e)
            {

                throw;
            }
            if (reff == "todo")
            {
                return RedirectToAction(MVC.Security.ToDo.Index());
            }
            return RedirectToAction(MVC.Security.LogSheet.List());
        }
    }
}