﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClosedXML.Excel;
using PIRApps.common;
using System.IO;
using PIRApps.data;

namespace PIRApps.Areas.Security.Controllers
{
    [PrivilegeSession]
    public partial class ReportController : BaseController
    {
        // GET: Security/Report
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult LogsheetReport()
        {
            return View();
        }
        [HttpPost]
        public virtual ActionResult LogsheetReport(DateTime? from, DateTime? to, string location, string callsign)
        {
            string folderPath = "C:\\Excel\\";
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                var data = SecurityLogsheet.MapLogsheetReport();
                if (!string.IsNullOrEmpty(location))
                {
                    data = data.Where(x => x.LocationName.ToLower().Contains(location.ToLower()));
                }
                if (!string.IsNullOrEmpty(callsign))
                {
                    data = data.Where(x => x.Callsign.ToLower().Contains(callsign.ToLower()));
                }
                if (from != null)
                {
                    data = data.Where(x => x.tanggal >= from);
                }
                if (to != null)
                {
                    data = data.Where(x => x.tanggal <= to);
                }
                var ss = wb.Worksheets.Add("Data");
                ss.Cells("D1").Value = "Security Log Sheet Report";
                ss.Cells("D2").Value = "PT. PLAZA INDONESIA REALTY TBK";
                ss.Cells("D3").Value = "Periode Date: From:" + from + "To : " + to;
                ss.Cells("D4").Value = "Location Name: " + location;
                ss.Cells("D5").Value = "Callsign: " + callsign;
                if (data.Count() == 0)
                {
                    var tableWithStrings = ss.Cell("A11").Value = "Empty Data";
                }
                else
                {
                    var tableWithStrings = ss.Cell("A11").InsertTable(data);
                }
                var ws = wb.Worksheet("Data");
                var path = Server.MapPath(@"~/Assets/img/Logo PI-Lama-01.png");
                var image = ws.AddPicture(path);

                image.MoveTo(ws.Cell(1, 1).Address);

                string myName = Server.UrlEncode("Main Asset Aging Report" + "_" +
                DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + DateTime.Now.Year + ".xlsx");
                MemoryStream stream = GetStream(wb);// The method is defined below
                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition",
                "attachment; filename=" + myName);
                Response.ContentType = "application/vnd.ms-excel";
                Response.BinaryWrite(stream.ToArray());
                Response.End();
            }
            return RedirectToAction(MVC.Security.Report.LogsheetReport());
        }
        public MemoryStream GetStream(XLWorkbook excelWorkbook)
        {
            MemoryStream fs = new MemoryStream();
            excelWorkbook.SaveAs(fs);
            fs.Position = 0;
            return fs;
        }
    }
}