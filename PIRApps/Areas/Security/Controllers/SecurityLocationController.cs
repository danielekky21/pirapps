﻿using PIRApps.common;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data;

namespace PIRApps.Areas.Security.Controllers
{
    [PrivilegeSession]
    public partial class SecurityLocationController : BaseController
    {
        // GET: Security/SecurityLocation
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();

        }
        public virtual JsonResult AjaxGetJSONData(model.SecurityLocationListModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<SecurityLocationListModel> dataItem = new List<SecurityLocationListModel>();
            int pageSize = rows;
            dataItem = SecurityLocation.MapToSecurityLocationModel().ToList();
            if (!string.IsNullOrEmpty(model.subportfolio))
            {
                dataItem = dataItem.Where(x => x.subportfolio.ToLower() == model.subportfolio.ToLower()).ToList();
            }
            if (!string.IsNullOrEmpty(model.locationname))
            {
                dataItem = dataItem.Where(x => x.locationname.ToLower().Contains(model.locationname.ToLower())).ToList();
            }
            if (!string.IsNullOrEmpty(model.callsign))
            {
                dataItem = dataItem.Where(x => x.callsign.ToLower().Contains(model.callsign.ToLower())).ToList();
            }
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult AddEdit(string id)
        {   
            if (!string.IsNullOrEmpty(id))
            {
                var data = SecurityLocation.GetLocationByID(id);
                if (data != null)
                {
                    ViewData.Model = data;
                }
            }
            else
            {
                var data = new SecurityLocation();
                //int count = 0;
                //var itemchecking = ItemChecking.GetAllItemChecking().Count();
                //if (itemchecking == 0)
                //{
                //    count = 1;
                //}
                //else
                //{
                //    count = itemchecking;
                //}
                //var id  = 
                ViewData.Model = data;

            }
            SelectLists selectList = new SelectLists();
            ViewBag.ItemCheck = selectList.GetItemCheckingSelectList().ToList();
            ViewBag.GroupCodes = selectList.GetGroupList().ToList();
            ViewBag.Status = selectList.GetActiveSelectList().ToList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(SecurityLocation model,string GroupCode)
        {
            try
            {
                UserTb userInfo = (UserTb)Session["User"];
                if (!string.IsNullOrEmpty(model.LocationCode))
                {
                    var data = SecurityLocation.GetLocationByID(model.LocationCode);
                    if (data != null)
                    {
                        data.LocationName = model.LocationName;
                        data.Callsign = model.Callsign;
                        data.ItemCheckingCode = model.ItemCheckingCode;
                        data.Floor = model.Floor;
                        data.TypeCode = model.TypeCode;
                        data.Subportfolio = model.Subportfolio;
                        data.AreaCode = model.AreaCode;
                        data.ItemCheckingCode = model.ItemCheckingCode;
                        data.IsActive = model.IsActive;
                        data.UpdatedBy = userInfo.Name;
                        data.UserGroupID = MsUserGroup.GetGroupIDbyCode(GroupCode);
                        data.UpdatedDate = DateTime.Now;
                        data.UpdateSave<SecurityLocation>();
                    }
                }
                else
                {
                    int count = 0;
                    var location = SecurityLocation.GetAllSecurityLocation().Count();
                    if (location == 0)
                    {
                        count = 1;
                    }
                    else
                    {
                        count = location;
                    }
                    var id = model.Subportfolio + "-" + model.AreaCode + "-" + model.TypeCode + "-" + count.ToString().PadLeft(4, '0');
                    var newData = new SecurityLocation();
                    newData.LocationCode = id;
                    newData.LocationName = model.LocationName;
                    newData.Subportfolio = model.Subportfolio;
                    newData.AreaCode = model.AreaCode;
                    newData.TypeCode = model.TypeCode;
                    newData.Floor = model.Floor;
                    newData.Callsign = model.Callsign;
                    newData.ItemCheckingCode = model.ItemCheckingCode;
                    newData.IsActive = model.IsActive;
                    newData.CreatedBy = userInfo.Name;
                    newData.CreatedDate = DateTime.Now;
                    newData.UpdatedBy = userInfo.Name;
                    newData.UpdatedDate = DateTime.Now;
                    newData.UserGroupID = MsUserGroup.GetGroupIDbyCode(GroupCode);
                    newData.InsertSave<SecurityLocation>();
                }
            }
            catch (Exception e)
            {

                throw;
            }
            
            return RedirectToAction(MVC.Security.SecurityLocation.List());
        }
    }
}