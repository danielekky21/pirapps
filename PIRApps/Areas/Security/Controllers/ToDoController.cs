﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace PIRApps.Areas.Security.Controllers
{   
    [PrivilegeSession]
    public partial class ToDoController : BaseController
    {
        // GET: Security/ToDo
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual JsonResult GetEvent(string insidesearch)
        {
            var time = DateTime.Now;
            string shifttime = "";
            var group = Session["GroupName"].ToString();
            var dataItem = SecurityLogsheet.MapLogsheetToModel().Where(x => x.tanggal.Value.Day >= DateTime.Now.Day).ToList();
            dataItem = dataItem.Where(x => x.workflowstatus != "SUBMIT" && x.workflowstatus != "APPROVED" && x.locationArea == "INS").ToList();
            if (group == "SECURITY-OFT")
            {
                dataItem = dataItem.Where(x => x.Subportofolio == "OFT").ToList();
            }
            if (group == "SECURITY-SPC")
            {
                dataItem = dataItem.Where(x => x.Subportofolio == "SPC").ToList();
            }
            if (!string.IsNullOrEmpty(insidesearch))
            {
                dataItem = dataItem.Where(x => x.LocationName.ToLower().Contains(insidesearch.ToLower())).ToList();
            }
            var shiftPagi = Convert.ToDateTime("03:00:00 PM");
            var shiftSiang = Convert.ToDateTime("10:00:00 PM");
            var shiftMalam = Convert.ToDateTime("06:00:00 AM");    
            if (time <= shiftPagi)
            {
                dataItem = dataItem.Where(x => x.Shift.ToLower() == "pagi").ToList();
                shifttime = System.Configuration.ConfigurationManager.AppSettings["securityshiftpagi"].ToString();
            }
            if (time > shiftPagi && time <= shiftSiang)
            {
                dataItem = dataItem.Where(x => x.Shift.ToLower() == "siang").ToList();
                shifttime = System.Configuration.ConfigurationManager.AppSettings["securityshiftsiang"].ToString();
            }
            if (time > shiftSiang && time <= shiftMalam)
            {
                dataItem = dataItem.Where(x => x.Shift.ToLower() == "malam").ToList();
                shifttime = System.Configuration.ConfigurationManager.AppSettings["securityshiftmalam"].ToString();
            }
            var eventList = dataItem.Select(x => new CalendarModel
            {
                id = x.LogsheetID,
                title = x.LocationName + " - " + x.Callsign + " ( Shift:" + x.Shift + " " + shifttime + " )",
                start = x.tanggal.ToString(),
                desc = x.Callsign,
                end = x.tanggal.ToString(),
                color = "#2980b9",
                allDay = false

            }).ToArray();
            return Json(eventList, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult GetEventOut(string outsidesearch)
        {
            var time = DateTime.Now;
            string shifttime = "";
            var group = Session["GroupName"].ToString();
            var dataItem = SecurityLogsheet.MapLogsheetToModel().Where(x => x.tanggal.Value.Day >= DateTime.Now.Day).ToList();
            dataItem = dataItem.Where(x => x.workflowstatus != "SUBMIT" && x.workflowstatus != "APPROVED" && x.locationArea == "OTS").ToList();
            if (group == "SECURITY-OFT")
            {
                dataItem = dataItem.Where(x => x.Subportofolio == "OFT").ToList();
            }
            if (group == "SECURITY-SPC")
            {
                dataItem = dataItem.Where(x => x.Subportofolio == "SPC").ToList();
            }
            if (!string.IsNullOrEmpty(outsidesearch))
            {
                dataItem = dataItem.Where(x => x.LocationName.ToLower().Contains(outsidesearch.ToLower())).ToList();
            }
            var shiftPagi = Convert.ToDateTime("03:00:00 PM");
            var shiftSiang = Convert.ToDateTime("10:00:00 PM");
            var shiftMalam = Convert.ToDateTime("06:00:00 AM");
            if (time <= shiftPagi)
            {
                dataItem = dataItem.Where(x => x.Shift.ToLower() == "pagi").ToList();
                shifttime = System.Configuration.ConfigurationManager.AppSettings["securityshiftpagi"].ToString();
            }
            if (time > shiftPagi && time <= shiftSiang)
            {
                dataItem = dataItem.Where(x => x.Shift.ToLower() == "siang").ToList();
                shifttime = System.Configuration.ConfigurationManager.AppSettings["securityshiftsiang"].ToString();
            }
            if (time > shiftSiang && time <= shiftMalam)
            {
                dataItem = dataItem.Where(x => x.Shift.ToLower() == "malam").ToList();
                shifttime = System.Configuration.ConfigurationManager.AppSettings["securityshiftmalam"].ToString();
            }
            var eventList = dataItem.Select(x => new CalendarModel
            {
                id = x.LogsheetID,
                title = x.LocationName + " - " + x.Callsign + " ( Shift:" + x.Shift + " " + shifttime + " )",
                start = x.tanggal.ToString(),
                desc = x.Callsign,
                end = x.tanggal.ToString(),
                color = "#2980b9",
                allDay = false

            }).ToArray();
            return Json(eventList, JsonRequestBehavior.AllowGet);
        }
        public virtual JsonResult GetEventLOD(string lodsearch)
        {
            var time = DateTime.Now;
            string shifttime = "";
            var group = Session["GroupName"].ToString();
            var dataItem = SecurityLogsheet.MapLogsheetToModel().Where(x => x.tanggal.Value.Day >= DateTime.Now.Day).ToList();
            dataItem = dataItem.Where(x => x.workflowstatus != "SUBMIT" && x.workflowstatus != "APPROVED" && x.locationArea == "LOD").ToList();
            if (group == "SECURITY-OFT")
            {
                dataItem = dataItem.Where(x => x.Subportofolio == "OFT").ToList();
            }
            if (group == "SECURITY-SPC")
            {
                dataItem = dataItem.Where(x => x.Subportofolio == "SPC").ToList();
            }
            if (!string.IsNullOrEmpty(lodsearch))
            {
                dataItem = dataItem.Where(x => x.LocationName.ToLower().Contains(lodsearch.ToLower())).ToList();
            }
            var shiftPagi = Convert.ToDateTime("03:00:00 PM");
            var shiftSiang = Convert.ToDateTime("10:00:00 PM");
            var shiftMalam = Convert.ToDateTime("06:00:00 AM");
            if (time <= shiftPagi)
            {
                dataItem = dataItem.Where(x => x.Shift.ToLower() == "pagi").ToList();
                shifttime = System.Configuration.ConfigurationManager.AppSettings["securityshiftpagi"].ToString();
            }
            if (time > shiftPagi && time <= shiftSiang)
            {
                dataItem = dataItem.Where(x => x.Shift.ToLower() == "siang").ToList();
                shifttime = System.Configuration.ConfigurationManager.AppSettings["securityshiftsiang"].ToString();
            }
            if (time > shiftSiang && time <= shiftMalam)
            {
                dataItem = dataItem.Where(x => x.Shift.ToLower() == "malam").ToList();
                shifttime = System.Configuration.ConfigurationManager.AppSettings["securityshiftmalam"].ToString();
            }
            var eventList = dataItem.Select(x => new CalendarModel
            {
                id = x.LogsheetID,
                title = x.LocationName + " - " + x.Callsign + " ( Shift:" + x.Shift + " " + shifttime + " )",
                start = x.tanggal.ToString(),
                desc = x.Callsign,
                end = x.tanggal.ToString(),
                color = "#2980b9",
                allDay = false

            }).ToArray();
            return Json(eventList, JsonRequestBehavior.AllowGet);
        }
    }
}