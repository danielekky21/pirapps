﻿using PIRApps.common;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data;

namespace PIRApps.Areas.Security.Controllers
{
    [PrivilegeSession]
    public partial class UserGroupController : BaseController
    {
        // GET: Security/UserGroup
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.UserGroupModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<UserGroupModel> dataItem = new List<UserGroupModel>();
            int pageSize = rows;
            dataItem = MsUserGroup.MapUserGroupToModel().ToList();
            //if (!string.IsNullOrEmpty(model.))
            //{

            //}
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult AddEditGroup(int id)
        {
            if (id != 0)
            {
                var data = MsUserGroup.GetMsUserGroupByID(id);
                if (data != null)
                {
                    ViewData.Model = data;
                }
            }
            else
            {
                ViewData.Model = new MsUserGroup();
            }
            SelectLists selectList = new SelectLists();
            ViewBag.shiftlist = selectList.GetShiftSelectList().ToList();
            ViewBag.userlist = UserTb.GetAllActiveUser().Where(x => x.UserGroup.UserGroupName == "Security").ToList();
            ViewBag.currentMember = MsUserGroup.GetMemberByUserGroupID(id).ToList();
            ViewBag.UserHead = selectList.GetUserList().ToList();
            ViewBag.GroupHead = MsUserGroup.GetHeadByUserGroupID(id).ToList();
            ViewBag.Status = selectList.GetActiveSelectList().ToList();
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEditGroup(MsUserGroup model, string[] mutliselect_to, string[] approvalGroup)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.UserGroupID != 0)
            {
                var currentData = MsUserGroup.GetMsUserGroupByID(model.UserGroupID);
                if (currentData != null)
                {
                    currentData.UserGroupName = model.UserGroupName;
                    //currentData.UserGroupHead = model.UserGroupHead;
                    currentData.IsActive = model.IsActive;
                    currentData.UpdatedBy = userInfo.Name;
                    currentData.Shift = model.Shift;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdateSave<MsUserGroup>();
                    var currentMemberData = MsUserGroup.GetMemberByUserGroupID(currentData.UserGroupID).ToList();
                    var currentApprovalData = MsUserGroup.GetHeadByUserGroupID(currentData.UserGroupID).ToList();
                    foreach (var item in currentMemberData)
                    {
                        item.Delete<UserGroupMember>();
                    }

                    if (mutliselect_to != null)
                    {
                        for (int i = 0; i < mutliselect_to.Count(); i++)
                        {
                            UserGroupMember member = new UserGroupMember();
                            member.UserGroupID = currentData.UserGroupID;
                            member.MemberID = int.Parse(mutliselect_to[i]);
                            member.CreatedBy = userInfo.Name;
                            member.CreatedDate = DateTime.Now;
                            member.InsertSave<UserGroupMember>();

                        }
                    }
                    foreach (var item in currentApprovalData)
                    {
                        item.Delete<LogsheetGroupHead>();
                    }
                    if (approvalGroup != null)
                    {
                        for (int i = 0; i < approvalGroup.Count(); i++)
                        {
                            LogsheetGroupHead leader = new LogsheetGroupHead();
                            leader.GroupID = currentData.UserGroupID;
                            leader.UserId = int.Parse(approvalGroup[i]);
                            leader.InsertSave<LogsheetGroupHead>();
                        }
                    }
                }

            }
            else
            {
                MsUserGroup newData = new MsUserGroup();
                newData.UserGroupCode = model.UserGroupCode;
               /*newData.UserGroupHead = model.UserGroupHead*/;
                newData.UserGroupName = model.UserGroupName;
                newData.IsActive = model.IsActive;
                newData.CreatedBy = userInfo.Name;
                newData.CreatedDate = DateTime.Now;
                newData.InsertSave<MsUserGroup>();
                if (mutliselect_to != null)
                {
                    for (int i = 0; i < mutliselect_to.Count(); i++)
                    {
                        UserGroupMember member = new UserGroupMember();
                        member.UserGroupID = newData.UserGroupID;
                        member.MemberID = int.Parse(mutliselect_to[i]);
                        member.CreatedBy = userInfo.Name;
                        member.CreatedDate = DateTime.Now;
                        member.InsertSave<UserGroupMember>();
                    }
                }
                if (approvalGroup != null)
                {
                    for (int i = 0; i < approvalGroup.Count(); i++)
                    {
                        LogsheetGroupHead leader = new LogsheetGroupHead();
                        leader.GroupID = newData.UserGroupID;
                        leader.UserId = int.Parse(approvalGroup[i]);
                        leader.InsertSave<LogsheetGroupHead>();
                    }
                }
            }
            return RedirectToAction(MVC.Security.UserGroup.List());
        }
       
    }
}