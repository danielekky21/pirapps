﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class BrandController : BaseController
    {
        // GET: Brand
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult BrandList()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.BrandModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<BrandMaster> dataBrand = new List<BrandMaster>();

            int pageSize = rows;
            dataBrand = BrandMaster.GetAllActiveBrandMaster().OrderByDescending(x => x.CreatedDate).ToList();
            if (!string.IsNullOrEmpty(model.BrandName))
            {
                dataBrand = dataBrand.Where(x => x.BrandName.ToLower().Contains(model.BrandName.ToLower())).OrderByDescending(x => x.CreatedDate).ToList();
            }
            List<BrandModel> ListNewMod = new List<BrandModel>();
            foreach (var item in dataBrand)
            {
                BrandModel newMod = new BrandModel();
                newMod.BrandID = item.BrandID;
                newMod.BrandName = item.BrandName;
                ListNewMod.Add(newMod);
            }
            var rowSize = dataBrand.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = ListNewMod.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public virtual ActionResult BrandAddEdit(int BrandID,string reff)
        {
            ViewBag.reff = reff;
            if (BrandID > 0)
            {
                var BrandList = BrandMaster.GetBrandByID(BrandID);
                if (BrandList != null)
                {
                    ViewData.Model = BrandList;
                }
                else
                {
                    ViewData.Model = new BrandMaster();
                }
            }
            else
            {
                ViewData.Model = new BrandMaster();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult BrandAddEdit(BrandMaster model,string reff)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.BrandID != 0)
            {
                var brandData = BrandMaster.GetBrandByID(model.BrandID);
                if (brandData != null)
                {
                    brandData.BrandName = model.BrandName;
                    brandData.UpdatedBy = userInfo.Name;
                    brandData.UpdatedDate = DateTime.Now;
                    brandData.UpdateSave<BrandMaster>();
                }
            }
            else
            {
                var newData = new BrandMaster();
                newData.BrandName = model.BrandName;
                newData.CreatedDate = DateTime.Now;
                newData.CreatedBy = userInfo.Name;
                newData.IsActive = true;
                newData.InsertSave<BrandMaster>();
            }
            if (reff == "asset")
            {
                return RedirectToAction(MVC.ITAsset.AddEdit());
            }
            else
            {
                return RedirectToAction(MVC.Brand.BrandList());
            }
           
        }
        public virtual ActionResult DeleteBrand(string BrandID)
        {
            BrandID = BrandID.Trim('"');
            if (BrandID != null)
            {
                var currentData = BrandMaster.GetBrandByID(int.Parse(BrandID));
                if (currentData != null)
                {
                    currentData.IsActive = false;
                    currentData.UpdateSave<BrandMaster>();
                }
            }
            return RedirectToAction(MVC.Brand.BrandList());
        }
    }
}