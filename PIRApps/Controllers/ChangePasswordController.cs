﻿using PIRApps.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Controllers
{
    public partial class ChangePasswordController : Controller
    {
        // GET: ChangePassword
        public virtual ActionResult Index(string msg)
        {
            if (msg == null)
            {
                msg = string.Empty;
            }
            ViewBag.outMsg = msg;
            return View();
        }
        [HttpPost]
        public virtual ActionResult submitPassword(string oldPass, string newPass)
        {
            var outMsg = string.Empty;
            string pwd = PIRApps.common.Security.DataEncription.Encrypt(oldPass);
            var userData = UserTb.GetUserByID(int.Parse(Session["UserId"].ToString()));
            if (userData.Password == pwd)
            {
                userData.Password = PIRApps.common.Security.DataEncription.Encrypt(newPass);
                userData.UpdateSave<UserTb>();
                outMsg = "success";
            }
            else
            {
                outMsg = "failed";
            }
            return RedirectToAction(MVC.ChangePassword.Index(outMsg));
        }
    }
}