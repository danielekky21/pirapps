﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Controllers
{
    public partial class DashboardController : Controller
    {
        // GET: Dashboard
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult Dashboard()
        {
            return View();
        }
    }
}