﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data;
using PIRApps.model;
using PIRApps.common;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class DepartementController : BaseController
    {
        // GET: Departement
        public virtual ActionResult List()
        {
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.privilege = privilege;
            
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetDepartementData(string sidx, string sord, int? page, int rows)
        {
            List<model.DepartementModel> DeptModel = new List<model.DepartementModel>();
            int pageSize = rows;
            DeptModel = DepartementMaster.GetAllActiveDepartement().OrderByDescending(x => x.CreatedDate).Select(x => new model.DepartementModel
            {
                DepartementID = x.DepartementID,
                DepartementName = x.DepartementName,
                CreatedDate = x.CreatedDate,
                CreatedBy = x.CreatedBy

            }).ToList();
            var rowSize = DeptModel.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = DeptModel.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult DeleteDepartement(int DepartementID)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (DepartementID != 0)
            {
                DepartementMaster dept = DepartementMaster.GetByID(DepartementID);
                if (dept != null)
                {
                    dept.UpdatedBy = userInfo.Name;
                    dept.UpdatedDate = DateTime.Now;
                    dept.IsDeleted = true;
                    dept.UpdateSave<DepartementMaster>();
                }
            }
            return RedirectToAction(MVC.Departement.List());
        }
        public virtual ActionResult AddEdit(int DepartementID)
        {
            if (DepartementID != 0)
            {
                ViewData.Model = data.DepartementMaster.GetByID(DepartementID);
            }
            else
            {
                ViewData.Model = new data.DepartementMaster();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(DepartementMaster model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.DepartementID != 0)
            {

                DepartementMaster dept = DepartementMaster.GetByID(model.DepartementID);
                if (dept != null)
                {
                    dept.DepartementName = model.DepartementName;
                    dept.DepartementShortName = model.DepartementShortName;
                    dept.UpdatedBy = userInfo.Name;
                    dept.UpdatedDate = DateTime.Now;
                    dept.UpdateSave<DepartementMaster>();
                }
            }
            else
            {
                model.CreatedBy = userInfo.Name;
                model.CreatedDate = DateTime.Now;
                model.IsDeleted = false;
                model.InsertSave<DepartementMaster>();
            }
            return RedirectToAction(MVC.Departement.List());
        }

    }
}