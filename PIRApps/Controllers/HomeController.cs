﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.model;
using PIRApps.common;

namespace PIRApps.Controllers
{
    public partial class HomeController : Controller
    {
        // GET: Home
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult Login()
        {

            return View();
        }
        [HttpPost]
        public virtual ActionResult Login(LoginModel model)
        {
            if (!ValidateLogOn(model.Username, model.Password))
            {
                return View();
            }
            else
            {
                string pwd = PIRApps.common.Security.DataEncription.Encrypt(model.Password);
                data.UserTb _User = data.UserTb.GetByUsernameAndPassword(model.Username, pwd);
                if (_User != null)
                {
                    CreateAuthenticationCookie(_User);
                    Session["UserId"] = _User.UserID;
                    Session["UserName"] = _User.UserName;
                    Session["User"] = _User;
                    Session["GroupName"] = _User.UserGroup.UserGroupName;
                    var defaultMenu = data.MsUserMenu.GetDefaultPage(_User.UserID);
                    if (defaultMenu.MenuLink != null)
                    {
                        return RedirectToAction(defaultMenu.MenuAction, defaultMenu.MenuLink,new {id = defaultMenu.MenuId });
                    }
                    else
                    {
                        return RedirectToAction(MVC.Dashboard.Index());
                    }
                    
                }
                else
                {
                    ViewBag.Error = "Username atau password salah";
                }
            }
            return View();
        }
        private bool ValidateLogOn(string userName, string password)
        {
            if (String.IsNullOrEmpty(userName))
            {
                ViewBag.ErrorUsername = "Username tidak boleh kosong";
            }

            if (String.IsNullOrEmpty(password))
            {
                @ViewBag.ErrorPassword = "Password tidak boleh kosong";
            }

            return ModelState.IsValid;
        }

        private void CreateAuthenticationCookie(data.UserTb _User)
        {
            HttpCookie myCookie = new HttpCookie("PlazaindonesiaManagementCookie");
            if (!SiteSettings.Domain.Contains("localhost"))
            {
                myCookie.Domain = "." + SiteSettings.Domain + ".com";
            }

            myCookie["user"] = _User.UserName.ToString();
            myCookie.Expires = DateTime.Now.AddDays(1);
            Response.Cookies.Add(myCookie);
        }
        public virtual ActionResult LogOut()
        {
            Session.Clear();
            return RedirectToAction(MVC.Home.Login());
        }
    }
}