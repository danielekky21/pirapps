﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data;
using PIRApps.common;
using PIRApps.model;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class ITAssetController : BaseController
    {
        // GET: ITAsset
        public virtual ActionResult List()
        {
            int usergroup = int.Parse(UserGroupId);
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                RedirectToAction("Login", "Home");
            }
            ViewBag.privilege = privilege;

            return View();
        }
        [HttpPost]
        public virtual ActionResult AssignAssetToUser(model.AssignUserAssetModel model )
        {
            UserTb userInfo = (UserTb)Session["User"];
            var privilege = (PIRApps.model.PrivilegeModel)Session["privilege"];
            if (!string.IsNullOrEmpty(model.BarcodeNo))
            {
                var currentAsset = ITAssets.GetAssetByBarcodeno(model.BarcodeNo);
                if (currentAsset != null)
                {
                    currentAsset.Name = model.Name;
                    currentAsset.UserName = model.UserName;
                    currentAsset.Location = model.Location;
                    currentAsset.LevelPosisi = model.LevelPosisi;
                    currentAsset.UpdatedDate = DateTime.Now;
                    currentAsset.UpdatedBy = userInfo.Name;
                    currentAsset.Status = "Aktif";
                    currentAsset.Departement = model.Departement;
                    currentAsset.UpdateSave<ITAssets>();
                    ITAssets.InsertIntoAssethistory(model.BarcodeNo, "Asset Assigned to: " + model.UserName, userInfo.Name);
                }
                bool isWorkflow = WorkFlowChild.CheckIfWorkFlowExist(privilege.ModulID);
                if (isWorkflow)
                {
                    currentAsset.WorkFlowStatus = "S";
                    currentAsset.UpdateSave<ITAsset>();
                    WorkFlowTransaction.AddtoWorkFlow(privilege.ModulID, "Asset WorkFlow", userInfo.Name, model.BarcodeNo, 1);
                }
                else
                {
                    currentAsset.WorkFlowStatus = "C";
                    currentAsset.UpdateSave<ITAsset>();
                }
            }
            return RedirectToAction(MVC.ITAsset.List());
        }
        public virtual ActionResult GetOSdata(int id)
        {
            var osData = MasterOS.GetOSByID(id);
            return Json(new {
                OSVersion = osData.OSVersion,
                OSVersionDesc = osData.OSVersionDesc
            }, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxITAssets(ITAssetsModel model, string sidx, string sord, int? page, int rows)
        {
            try
            {
                PrivilegeModel privilege = GetPrivilege();
            List<ITAssetModel> ListModel = new List<ITAssetModel>();
            int pageSize = rows;
            var rowSize = ListModel.Count();
            ListModel = ITAssets.GetAllActiveITAssetsToModel(model).OrderByDescending(x => x.CreatedDate).ToList();
            if (!String.IsNullOrEmpty(model.BarcodeNo))
            {
                ListModel = ListModel.Where(x => x.BarcodeNumber.ToLower().Contains(model.BarcodeNo.ToLower())).ToList();
            }
            if (!String.IsNullOrEmpty(model.SerialNo))
            {
                ListModel = ListModel.Where(x => x.SN.ToLower().Contains(model.SerialNo.ToLower())).ToList();
            }
            if (!String.IsNullOrEmpty(model.User))
            {
                    //ListModel = ListModel.Where(x => x.Name == model.User).ToList();
                    ListModel = ListModel.Where(c => c.Name != null && c.Name.ToLower().Contains(model.User.ToLower())).ToList();
            }
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(ListModel.Count());
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);


            var data = ListModel.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            Session["AssetReport"] = ListModel;
            return Json(jsonData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {

                throw;
            }

            

        }
        public virtual ActionResult ExportToExcel()
        {
            var gv = new GridView();
            var data = (List<model.ITAssetModel>)Session["AssetReport"];
            List<ITAssetReportModel> reportModelList = new List<ITAssetReportModel>();
            foreach (var item in data)
            {
                ITAssetReportModel model = new ITAssetReportModel();
                model.AssetNo = item.AssetNo;
                model.BarcodeNumber = item.BarcodeNumber;
                model.Name = item.Name;
                model.LevelPosisi = item.LevelPosisi;
                model.Departement = item.Departement;
                model.Location = item.Location;
                model.UserName = item.UserName;
                model.Model = item.modelasset;
                model.SN = item.SN;
                model.Tipe = item.tipeName;
                model.TahunPembelian = item.TahunPembelian;
                model.EndWarranty = item.HabisGaransi.ToString();
                model.RAM = item.RAM;
                model.DomainStatus = item.DomainStatus;
                model.CreatedDate = item.CreatedDate.ToString();
                model.Brand = item.brandName;
                model.OSVersion = item.OSVersion;
                model.OSDescription = item.OSDescription;
                model.Remark = item.Remarks;
                reportModelList.Add(model);
            }
            gv.DataSource = reportModelList;
            gv.DataBind();

            common.GlobalFunction.ConvertToExcelHelper(Response, gv, "AssetReport");

            return RedirectToAction(MVC.Ticket.Report());
        }
        public virtual ActionResult ViewAssetDetailPartial(string keyno)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var ListActive = common.GlobalFunction.ActiveModelGenerate();
            ViewBag.imgList = ImgAsset.GetImageList(keyno).ToList();
            ViewBag.UserList = new vw_EmployeeMaster();

            foreach (var item in ListActive)
            {
                items.Add(new SelectListItem { Text = item.ActiveModelText, Value = item.ActiveModelValue });
            }
            if (!string.IsNullOrEmpty(keyno))
            {
                ITAssetModel model = new ITAssetModel();
                model = ITAssets.GetAssetByBarcodeNoToModel(keyno);
                ViewBag.UserList = ITAssets.GetUserData(model.UserName);
                ViewData.Model = model;

            }
            else
            {
                ViewData.Model = new ITAssetModel();
            }
            ViewBag.Status = items;
            return PartialView();
        }
        public virtual ActionResult AssignAssetDetail(string BarcodeNo)
        {
            ViewData.Model = ITAssets.GetUserInformation(BarcodeNo);
            ViewBag.Departement = DepartementMaster.GetAllActiveDepartement();
            ViewBag.Location = LocationMaster.GetAllActiveLocation();
            return View();
        }
        public virtual ActionResult ReleaseAsset(string BarcodeNo)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!string.IsNullOrEmpty(BarcodeNo))
            {
                if (ITAssets.ReleaseAsset(BarcodeNo))
                {
                    ITAssets.InsertIntoAssethistory(BarcodeNo, "Asset Release", userInfo.Name);
                    return RedirectToAction(MVC.ITAsset.AssignAssetDetail(BarcodeNo));
                }
                return RedirectToAction(MVC.ITAsset.AssignAssetDetail(BarcodeNo));
            }
            else
            {
                return RedirectToAction(MVC.ITAsset.List());
            }

            

        }
        public virtual ActionResult AddEdit(string BarcodeNo)
        {
            List<SelectListItem> TypeSL = new List<SelectListItem>();
            List<SelectListItem> BrandSL = new List<SelectListItem>();
            List<SelectListItem> ModelSL = new List<SelectListItem>();
            List<SelectListItem> OSSL = new List<SelectListItem>();
            List<SelectListItem> ProcSL = new List<SelectListItem>();
            var mastertype = MasterType.GetallActiveTypeMaster();
            var masterbrand = BrandMaster.GetAllActiveBrandMaster();
            //var mastermodel = PIRApps.data.ModelAsset.GetAllActiveModelAsset(10);
            var masteros = MasterOS.GetAllActiveOS();
            var masterproc = ProcessorMaster.GetAllProcessorActive();

            foreach (var item in mastertype)
            {
                TypeSL.Add(new SelectListItem { Text = item.TypeName, Value = item.TypeID.ToString() });
            }
            foreach (var item in masterbrand)
            {
                BrandSL.Add(new SelectListItem { Text = item.BrandName, Value = item.BrandID.ToString() });
            }
            //foreach (var item in mastermodel)
            //{
            //    ModelSL.Add(new SelectListItem { Text = item.ModelName, Value = item.ModelID.ToString() });
            //}
            foreach (var item in masteros)
            {
                OSSL.Add(new SelectListItem { Text = item.OSName + " - " + item.OSVersionDesc, Value = item.OSID.ToString() });
            }
            foreach (var item in masterproc)
            {
                ProcSL.Add(new SelectListItem { Text = item.ProcessorName, Value = item.ProcessorID.ToString() });
            }
            List<SelectListItem> items = new List<SelectListItem>();
            var ListActive = common.GlobalFunction.ActiveModelGenerate();
            ViewBag.imgList = ImgAsset.GetImageList(BarcodeNo).ToList();
            foreach (var item in ListActive)
            {
                items.Add(new SelectListItem { Text = item.ActiveModelText, Value = item.ActiveModelValue });
            }
            ViewBag.Status = items;
            ViewBag.type = TypeSL;
            ViewBag.brandsl = BrandSL;
            //ViewBag.modelsl = ModelSL;
            ViewBag.ossl = OSSL;
            ViewBag.proc = ProcSL;
            if (!string.IsNullOrEmpty(BarcodeNo))
            {
                ITAssetModel model = new ITAssetModel();
                model = ITAssets.GetAssetByBarcodeNoToModel(BarcodeNo);
                ViewData.Model = model;
            }
            else
            {
                ViewData.Model = new ITAssetModel();
            }
         
            return View();
        }
        public virtual ActionResult ViewDetailAsset(string BarcodeNo)
        {
            ViewData.Model = ITAssets.GetAssetByBarcodeno(BarcodeNo);
            ViewBag.History = data.WorkFlowHistory.GetHistoryByTableKeyID(BarcodeNo).OrderByDescending(x => x.WorkFlowHistoryDate).ToList();
            return View();
        }
        public virtual ActionResult GetModelByBrand(int brandid)
        {
            var data = PIRApps.data.ModelAsset.GetAllActiveModelAsset(brandid);
            List<model.ModelAsset> mod = new List<model.ModelAsset>();
            foreach (var item in data)
            {
                model.ModelAsset asst = new model.ModelAsset();
                asst.ModelID = item.ModelID;
                asst.ModelName = item.ModelName;
                mod.Add(asst);
            }
            return Json(mod, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult InsertAssetImg(string BarcodeNo, string imgid)
        {
            if (!string.IsNullOrEmpty(BarcodeNo) && !string.IsNullOrEmpty(imgid))
            {
                ImgAsset imgAsst = new ImgAsset();
                imgAsst.ImgId = imgid;
                imgAsst.BarcodeNo = BarcodeNo;
                imgAsst.InsertSave<ImgAsset>();
                var currentAsset = ITAssets.GetAssetByBarcodeno(BarcodeNo);
                if (currentAsset != null)
                {
                    currentAsset.ImgId = imgAsst.ImgAssetId;
                    currentAsset.UpdatedDate = DateTime.Now;
                    currentAsset.UpdateSave<ITAsset>();
                }
            }
           
            return RedirectToAction(MVC.ITAsset.AddEdit(BarcodeNo));
        }
        [HttpPost]
        public virtual ActionResult AddEdit(string AssetNo,string BarcodeNumber,string Model,string Processor,string SN,string Tipe,string TahunPembelian,string RAM,string OS, string OSOEM, string DomainStatus, string VGACard,string AvayaExt,string AvayaSN,string NoteAvaya,string Brand,string HabisGaransi,string OSVersion,string OSDescription,string Remarks)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!string.IsNullOrEmpty(AssetNo))
            {
                //Edit Stage
                ITAsset existAsset = ITAssets.GetAssetByAssetNo(AssetNo);
                if (existAsset != null)
                {


                    existAsset.Model = int.Parse(Model);
                    existAsset.Processor = int.Parse(Processor);
                    existAsset.SN = SN;
                    existAsset.Tipe = int.Parse(Tipe);
                    existAsset.TahunPembelian = TahunPembelian;
                    existAsset.RAM = RAM;
                    existAsset.OS = int.Parse(OS);
                    existAsset.OSOEM = OSOEM;
                    existAsset.DomainStatus = DomainStatus;
                    existAsset.VGACard = VGACard;
                    existAsset.AvayaExt = AvayaExt;
                    existAsset.Brand = int.Parse(Brand);
                    existAsset.AvayaSN = AvayaSN;
                    existAsset.WorkFlowStatus = "D";
                    existAsset.NoteAvaya = NoteAvaya;
                    existAsset.UpdatedBy = userInfo.Name;
                    existAsset.OSVersion = OSVersion;
                    existAsset.OSDecription = OSDescription;
                    if (!string.IsNullOrEmpty(HabisGaransi))
                    {
                        existAsset.HabisGaransi = DateTime.Parse(HabisGaransi);
                    }
                    else
                    {
                        existAsset.HabisGaransi = null;
                    }
                    existAsset.UpdatedDate = DateTime.Now;
                    existAsset.Remarks = Remarks;
                    existAsset.UpdateSave<ITAsset>();
                }
                else
                {
                        if (ITAssets.CekIfAssetNoDuplicate(AssetNo))
                        {
                            ITAsset newAsset = new ITAsset();
                            newAsset.AssetNo = AssetNo;
                            newAsset.BarcodeNumber = BarcodeNumber;
                            newAsset.Model = int.Parse(Model);
                            newAsset.Processor = int.Parse(Processor);
                            newAsset.SN = SN;
                            newAsset.Tipe = int.Parse(Tipe);
                            newAsset.Brand = int.Parse(Brand);
                            newAsset.TahunPembelian = TahunPembelian;
                            newAsset.RAM = RAM;
                            newAsset.OS = int.Parse(OS);
                            newAsset.OSVersion = OSVersion;
                            newAsset.OSDecription = OSDescription;
                            newAsset.OSOEM = OSOEM;
                            newAsset.DomainStatus = DomainStatus;
                            newAsset.VGACard = VGACard;
                            newAsset.AvayaExt = AvayaExt;
                            newAsset.AvayaSN = AvayaSN;
                            newAsset.NoteAvaya = NoteAvaya;
                            newAsset.CreatedBy = userInfo.Name;
                            newAsset.isDeleted = false;
                            if (!string.IsNullOrEmpty(HabisGaransi))
                            {
                                newAsset.HabisGaransi = DateTime.Parse(HabisGaransi);
                            }
                            else
                            {
                                newAsset.HabisGaransi = null;
                            }
                            newAsset.CreatedDate = DateTime.Now;
                            newAsset.Remarks = Remarks;
                            newAsset.InsertSave<ITAsset>();
                        }
                        else
                        {
                            return RedirectToAction(MVC.ITAsset.List());
                        }
                   
                }
            }
            
            return RedirectToAction(MVC.ITAsset.List());
        }
        public virtual ActionResult Report()
        {
            return View();
        }
    }
}