﻿using PIRApps.common;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data; 

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class ItemCheckingController : BaseController
    {
        // GET: ItemChecking
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult ItemCheckingList()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.ItemCheckingModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<ItemChecking> dataItem = new List<ItemChecking>();
            int pageSize = rows;
            dataItem = ItemChecking.GetAllItemChecking().OrderByDescending(x => x.CreatedDate).ToList();
            //if (!string.IsNullOrEmpty(model.))
            //{

            //}
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
    }
}