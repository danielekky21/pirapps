﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data;
using PIRApps.model;
using PIRApps.common;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class LocationController : BaseController
    {
        // GET: Location
        public virtual ActionResult List()
        {
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
               return RedirectToAction("Login", "Home");
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetLocationData(string sidx, string sord, int? page, int rows)
        {

            List<model.LocationModel> locMod = new List<model.LocationModel>();
            int pageSize = rows;
            locMod = LocationMaster.GetAllActiveLocation().OrderByDescending(x => x.CreatedDate).Select(x => new model.LocationModel
            {
                LocationID = x.LocationID,
                LocationName = x.LocationName

            }).ToList();
            var rowSize = locMod.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = locMod.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public virtual ActionResult DeleteLocation(int LocationID)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (LocationID != 0)
            {
                LocationMaster loc = LocationMaster.GetLocationByID(LocationID);
                if (loc != null)
                {
                    loc.UpdatedBy = userInfo.Name;
                    loc.UpdatedDate = DateTime.Now;
                    loc.IsDeleted = true;
                    loc.UpdateSave<LocationMaster>();
                }
            }
            return RedirectToAction(MVC.Location.List());
        }
        public virtual ActionResult AddEdit(int LocationID)
        {
            if (LocationID != 0)
            {
                ViewData.Model = LocationMaster.GetLocationByID(LocationID);
            }
            else
            {
                ViewData.Model = new LocationMaster();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(LocationMaster model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.LocationID != 0)
            {
                LocationMaster loc = LocationMaster.GetLocationByID(model.LocationID);
                if (loc != null)
                {
                    loc.LocationName = model.LocationName;
                    loc.UpdatedBy = userInfo.Name;
                    loc.UpdatedDate = DateTime.Now;
                    loc.UpdateSave<LocationMaster>();
                }
            }
            else
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = userInfo.Name;
                model.IsDeleted = false;
                model.InsertSave<LocationMaster>();
               
            }
            return RedirectToAction(MVC.Location.List());
        }

    }
}