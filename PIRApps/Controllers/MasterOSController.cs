﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class MasterOSController : BaseController
    {
        // GET: MasterOS
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.OSModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<PIRApps.data.MasterOS> dataOS = new List<PIRApps.data.MasterOS>();

            int pageSize = rows;
            dataOS = PIRApps.data.MasterOS.GetAllActiveOS().OrderByDescending(x => x.CreatedDate).ToList();
            if (!string.IsNullOrEmpty(model.OSName))
            {
                dataOS = dataOS.Where(x => x.OSName.ToLower().Contains(model.OSName.ToLower())).OrderByDescending(x => x.CreatedDate).ToList();
            }
            if (!string.IsNullOrEmpty(model.OSVersion))
            {
                dataOS = dataOS.Where(x => x.OSVersionDesc.ToLower().Contains(model.OSVersion.ToLower())).OrderByDescending(x => x.CreatedDate).ToList();
            }
            var rowSize = dataOS.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            List<OSModel> ListnewMod = new List<OSModel>();
            foreach (var item in dataOS)
            {
                OSModel newMod = new OSModel();
                newMod.OSID = item.OSID;
                newMod.OSName = item.OSName;
                newMod.OSVersion = item.OSVersion;
                newMod.OSVersionDesc = item.OSVersionDesc;
                ListnewMod.Add(newMod);
                
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = ListnewMod.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public virtual ActionResult AddEdit(int OSID,string reff)
        {
            ViewBag.reff = reff;
            if (OSID != 0)
            {
                var currentData = MasterOS.GetOSByID(OSID);
                if (currentData != null)
                {
                    ViewData.Model = currentData;
                }
                else
                {
                    ViewData.Model = new MasterOS();
                }
            }
            else
            {
                ViewData.Model = new MasterOS();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(MasterOS model,string reff)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.OSID != 0)
            {
                var currentData = MasterOS.GetOSByID(model.OSID);
                if (currentData != null)
                {
                    currentData.OSName = model.OSName;
                    currentData.OSVersion = model.OSVersion;
                    currentData.OSVersionDesc = model.OSVersionDesc;
                    currentData.UpdatedBy = userInfo.UserName;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdateSave<MasterOS>();

                }
            }
            else
            {
                var newData = new MasterOS();
                newData = model;
                newData.CreatedBy = userInfo.UserName;
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = true;
                newData.InsertSave<MasterOS>();
            }
            if (reff == "asset")
            {
                return RedirectToAction(MVC.ITAsset.AddEdit());
            }
            else {
                return RedirectToAction(MVC.MasterOS.List());
            }
           
        }
        public virtual ActionResult DeleteOS(string OSID)
        {
            UserTb userInfo = (UserTb)Session["User"];
            OSID = OSID.Trim('"');
            if (OSID != null)
            {
                var currentData = MasterOS.GetOSByID(int.Parse(OSID));
                if (currentData != null)
                {
                    currentData.IsActive = false;
                    currentData.UpdatedBy = userInfo.UserName;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdateSave<MasterOS>();
                }
            }
            return RedirectToAction(MVC.MasterOS.List());
        }
    }
}