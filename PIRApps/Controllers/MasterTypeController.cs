﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class MasterTypeController : BaseController
    {
        // GET: MasterType
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.TypeModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<PIRApps.data.MasterType> dataType = new List<PIRApps.data.MasterType>();

            int pageSize = rows;
            dataType = MasterType.GetallActiveTypeMaster().OrderByDescending(x => x.CreatedDate).ToList();
            if (!string.IsNullOrEmpty(model.TypeName))
            {
                dataType = dataType.Where(x => x.TypeName.ToLower().Contains(model.TypeName.ToLower())).OrderByDescending(x => x.CreatedDate).ToList();
            }
            List<TypeModel> ListNewMod = new List<TypeModel>();
            foreach (var item in dataType)
            {
                TypeModel newMod = new TypeModel();
                newMod.TypeID = item.TypeID;
                newMod.TypeName = item.TypeName;
                ListNewMod.Add(newMod);
            }
            var rowSize = dataType.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = ListNewMod.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public virtual ActionResult AddEdit(int TypeID, string reff)
        {
            ViewBag.reff = reff;
            if (TypeID != 0)
            {
                var currentData = TypeMaster.GetMasterTypeByID(TypeID);
                if (currentData != null)
                {
                    ViewData.Model = currentData;
                }
                else
                {
                    ViewData.Model = new MasterType();
                }
            }
            else
            {
                ViewData.Model = new MasterType();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(MasterType model, string reff)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.TypeID != 0)
            {
                var currentData = TypeMaster.GetMasterTypeByID(model.TypeID);
                if (currentData != null)
                {
                    currentData.TypeName = model.TypeName;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdatedBy = userInfo.UserName;
                    currentData.UpdateSave<MasterType>();
                }

            }
            else
            {
                MasterType newData = new MasterType();
                newData.TypeName = model.TypeName;
                newData.CreatedDate = DateTime.Now;
                newData.CreatedBy = userInfo.UserName;
                newData.IsActive = true;
                newData.InsertSave<MasterType>();
            }
            if (reff == "asset")
            {
                return RedirectToAction(MVC.ITAsset.AddEdit());
            }
            else { return RedirectToAction(MVC.MasterType.List()); }


        }
        public virtual ActionResult DeleteType(string TypeID)
        {
            UserTb userInfo = (UserTb)Session["User"];
            TypeID = TypeID.Trim('"');
            if (TypeID != null)
            {
                var currentData = MasterType.GetMasterTypeByID(int.Parse(TypeID));
                if (currentData != null)
                {
                    currentData.IsActive = false;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdatedBy = userInfo.UserName;
                    currentData.UpdateSave<MasterType>();

                }
            }
            return RedirectToAction(MVC.MasterType.List());
        }
    }
}