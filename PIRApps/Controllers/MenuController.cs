﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data;
using PIRApps.common;

namespace PIRApps.Controllers
{
    public partial class MenuController : BaseController
    {
        // GET: Menu
 
        public virtual ActionResult Index()
        {
            string UserName = Session["UserName"].ToString();
            data.MsUserMenu Menus = new data.MsUserMenu();
            Session["OldUserName"] = UserName;
            Session["Menus"] = Menus.GetMenus(UserName);
            //Session["GetMenu"] = 
            return PartialView("Index", Session["Menus"]);
        }
    }
}