﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class ModelAssetController : BaseController
    {
        // GET: MasterAsset
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult ModelList(int? brandID)
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.privilege = privilege;
            Session["brandid"] =  brandID;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.ModelAsset model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<PIRApps.data.ModelAsset> dataModel = new List<PIRApps.data.ModelAsset>();

            int pageSize = rows;
            dataModel = PIRApps.data.ModelAsset.GetAllActiveModelAsset(int.Parse(Session["brandid"].ToString())).OrderByDescending(x => x.CreatedDate).ToList();
            if (!string.IsNullOrEmpty(model.ModelName))
            {
                dataModel = dataModel.Where(x => x.ModelName.ToLower().Contains(model.ModelName.ToLower())).OrderByDescending(x => x.CreatedDate).ToList();
            }
            List<PIRApps.model.ModelAsset> ListNewMod = new List<PIRApps.model.ModelAsset>();
            foreach (var item in dataModel)
            {
                PIRApps.model.ModelAsset newMod = new PIRApps.model.ModelAsset();
                newMod.ModelID = item.ModelID;
                newMod.ModelName = item.ModelName;
                ListNewMod.Add(newMod);
            }
            var rowSize = dataModel.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = ListNewMod.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public virtual ActionResult AddEdit(int ModelID,string reff,int brandid)
        {
            ViewBag.reff = reff;
            if (ModelID != 0)
            {
                var modelList = PIRApps.data.ModelAsset.GetModelAssetByID(ModelID);
                if (modelList != null)
                {
                    var models = modelList;
                    models.BrandID = brandid;
                    ViewData.Model = models;
                }
                else
                {
                    var models = new PIRApps.data.ModelAsset();
                    models.BrandID = brandid;
                    ViewData.Model = models;
                }
            }
            else
            {
                var models = new PIRApps.data.ModelAsset();
                models.BrandID = brandid;
                ViewData.Model = models;
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(PIRApps.data.ModelAsset model,string reff)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.ModelID != 0)
            {
                var modelList = PIRApps.data.ModelAsset.GetModelAssetByID(model.ModelID);
                if (modelList != null)
                {
                    modelList.ModelName = model.ModelName;
                    modelList.UpdatedDate = DateTime.Now;
                    modelList.CreatedBy = userInfo.UserName;
                    modelList.UpdateSave<PIRApps.data.ModelAsset>();
                }
            }
            else
            {
                var newData = new PIRApps.data.ModelAsset();
                newData.ModelName = model.ModelName;
                newData.CreatedBy = userInfo.UserName;
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = true;
                newData.BrandID = model.BrandID;
                newData.InsertSave<PIRApps.data.ModelAsset>();
            }
            if (reff == "asset")
            {
                return RedirectToAction(MVC.ITAsset.AddEdit());
            }
            else
            {
                return RedirectToAction(MVC.ModelAsset.ModelList(model.BrandID.GetValueOrDefault()));
            }
        }
        public virtual ActionResult DeleteModel(string ModelID,int brandid)
        {
            ModelID = ModelID.Trim('"');
            if (ModelID != null)
            {
                var currentData = PIRApps.data.ModelAsset.GetModelAssetByID(int.Parse(ModelID));
                if (currentData != null)
                {
                    currentData.IsActive = false;
                    currentData.UpdateSave<PIRApps.data.ModelAsset>();
                }
            }
            return RedirectToAction(MVC.ModelAsset.ModelList(brandid));
        }
    }
}