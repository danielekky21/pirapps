﻿using PIRApps.common;
using PIRApps.data;
using PIRApps.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class ProcessorController : BaseController
    {
        // GET: Processor
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult ProcessorList()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.privilege = privilege;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.ProcessorModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<data.ProcessorMaster> dataProcessor = new List<data.ProcessorMaster>();

            int pageSize = rows;
            dataProcessor = ProcessorMaster.GetAllProcessorActive().OrderByDescending(x => x.CreatedDate).ToList();
            if (!string.IsNullOrEmpty(model.processorName))
            {
                dataProcessor = dataProcessor.Where(x => x.ProcessorName.ToLower().Contains(model.processorName.ToLower())).OrderByDescending(x => x.CreatedDate).ToList();
            }
            List<ProcessorModel> ListNewMod = new List<ProcessorModel>();
            foreach (var item in dataProcessor)
            {
                ProcessorModel newMod = new ProcessorModel();
                newMod.processorID = item.ProcessorID;
                newMod.processorName = item.ProcessorName;
                ListNewMod.Add(newMod);
            }
            var rowSize = dataProcessor.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = ListNewMod.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);

        }
        public virtual ActionResult ProcessorAddEdit(int ProcessorID,string reff)
        {
            ViewBag.reff = reff;
            if (ProcessorID > 0)
            {
                var processorList = ProcessorMaster.GetProcessorByID(ProcessorID);
                if (processorList != null)
                {
                    ViewData.Model = processorList;
                }
                else
                {
                    ViewData.Model = new ProcessorMaster();
                }
            }
            else
            {
                ViewData.Model = new ProcessorMaster();
            }
            return View();
        }

        [HttpPost]
        public virtual ActionResult ProcessorAddEdit(ProcessorMaster model, string reff)
        {

            UserTb userInfo = (UserTb)Session["User"];
            if (model.ProcessorID != 0)
            {
                var procData = ProcessorMaster.GetProcessorByID(model.ProcessorID);
                if (procData != null)
                {
                    procData.ProcessorName = model.ProcessorName;
                    procData.UpdatedBy = userInfo.Name;
                    procData.UpdatedDate = DateTime.Now;
                    procData.UpdateSave<ProcessorMaster>();

                }
            }
            else
            {
                var newData = new ProcessorMaster();
                newData.ProcessorName = model.ProcessorName;
                newData.CreatedBy = userInfo.Name;
                newData.CreatedDate = DateTime.Now;
                newData.IsActive = true;
                newData.InsertSave<ProcessorMaster>();
            }
            if (reff == "asset")
            {
                return RedirectToAction(MVC.ITAsset.AddEdit());
            }
            else
            {
                return RedirectToAction(MVC.Processor.ProcessorList());
            }
        }
        public virtual ActionResult DeleteProcessor(string ProcessorID)
        {
            UserTb userInfo = (UserTb)Session["User"];
            ProcessorID = ProcessorID.ToString().Trim('"');

            if (ProcessorID != null)
            {
                var currentData = ProcessorMaster.GetProcessorByID(int.Parse(ProcessorID));
                if (currentData != null)
                {
                    currentData.IsActive = false;
                    currentData.UpdatedDate = DateTime.Now;
                    currentData.UpdatedBy = userInfo.UserName;
                    currentData.UpdateSave<ProcessorMaster>();

                }
            }

            return RedirectToAction(MVC.Processor.ProcessorList());
        }
    }
}