﻿using PIRApps.common;
using PIRApps.model;
using PIRApps.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Controllers
{
    public partial class SecurityLocationController : BaseController
    {
        // GET: SecurityLocation
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult List()
        {
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home", new { area = "" });
            }
            ViewBag.privilege = privilege;
            return View();

        }
        public virtual JsonResult AjaxGetJSONData(model.SecurityLocationListModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<SecurityLocationListModel> dataItem = new List<SecurityLocationListModel>();
            int pageSize = rows;
            dataItem = SecurityLocation.MapToSecurityLocationModel().ToList();
            var rowSize = dataItem.Count;
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = dataItem.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

    }
}