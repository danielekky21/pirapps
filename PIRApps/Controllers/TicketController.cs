﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data;
using PIRApps.common;
using PIRApps.model;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class TicketController : BaseController
    {
        // GET: Ticket
        public virtual ActionResult TicketDashboard()
        {
            int usergroup = int.Parse(UserGroupId);
            ViewBag.UserId = UserId;
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.privilege = privilege;
            List<SelectListItem> items = new List<SelectListItem>();
            var listatus = common.GlobalFunction.TicketStatusGenerate();
            foreach (var item in listatus)
            {
                items.Add(new SelectListItem { Text = item.statusName, Value = item.statusCode });
            }
            ViewBag.Status = items;
            return View();
        }
        public virtual ActionResult ViewTicketDetailPartial(string keyno)
        {
            ViewBag.tickettype = TicketType.GetAllTicketType();
            ViewBag.prioritys = common.GlobalFunction.GetPriorityModel();
            ViewBag.Departement = DepartementMaster.GetAllActiveDepartement();
            ViewBag.Locations = LocationMaster.GetAllActiveLocation();
            ViewBag.Problem = ProblemType.GetAllActiveProblemType();
            ViewData.Model = TicketMaster.GetTicketByID(keyno);
            ViewBag.imgList = ImgTicket.GetImageList(keyno).ToList();
            return PartialView();
        }
        public virtual JsonResult autocompletename(string prefix)
        {
            var employeename = EmployeeDataModel.EmployeeList(prefix).ToList();
            return Json(employeename, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult SendApplicationForm(string email)
        {
            UserTb userInfo = UserTb.GetUserByEmail(email);
            Task.Run(() => { GlobalFunction.SendAttachmentFile(email, userInfo.Name, "PIR_IT-UserRequestAppForm_v1.2.docx"); }).Wait();
            return RedirectToAction(MVC.Ticket.TicketDashboard());
        }
        public virtual ActionResult InsertImgTicket(string ticketNo, string imgid)
        {
            if (!string.IsNullOrEmpty(ticketNo) && !string.IsNullOrEmpty(imgid))
            {
                ImgTicket imgTick = new ImgTicket();
                imgTick.ImgId = imgid;
                imgTick.TicketNo = ticketNo;
                imgTick.InsertSave<ImgTicket>();

                var currentTicket = TicketMaster.GetTicketByID(ticketNo);
                if (currentTicket != null)
                {
                    currentTicket.imgTicket = imgTick.ImgTicketid;
                    currentTicket.UpdatedDate = DateTime.Now;

                    currentTicket.UpdateSave<TicketMaster>();
                }
            }
            return RedirectToAction(MVC.Ticket.ViewTaskDetail(ticketNo));
        }
        public virtual ActionResult ViewDetailDetailPage(string ticketNo)
        {
            ViewData.Model = TicketMaster.GetTicketByID(ticketNo);
            PrivilegeModel privilege = GetPrivilege();
            ViewBag.privilege = privilege;
            ViewBag.UserId = UserId;
            ViewBag.History = data.WorkFlowHistory.GetHistoryByTableKeyID(ticketNo).OrderByDescending(x => x.WorkFlowHistoryDate).ToList();
            return View();
        }
        public virtual ActionResult TakeOverTask(string ticketNo)
        {
            UserTb userInfo = (UserTb)Session["User"];
            ticketNo = ticketNo.Trim('"');
            if (!string.IsNullOrEmpty(ticketNo))
            {

                TicketMaster ticket = TicketMaster.GetTicketByID(ticketNo);
                if (ticket != null)
                {
                    ticket.PIC = userInfo.UserID;
                    ticket.UpdatedBy = userInfo.Name;
                    ticket.UpdatedDate = DateTime.Now;
                    ticket.UpdateSave<TicketMaster>();
                    TaskHistory.addTaskLog(ticket, "TICKET TAKE OVER BY " + ticket.UpdatedBy);
                }

            }
            return RedirectToAction(MVC.Ticket.ViewDetailDetailPage(ticketNo));
        }
        public virtual ActionResult TicketAddEdit(string ticketNo)
        {
            ViewBag.tickettype = TicketType.GetAllTicketType();
            ViewBag.prioritys = common.GlobalFunction.GetPriorityModel();
            ViewBag.Departement = DepartementMaster.GetAllActiveDepartement();
            ViewBag.Locations = LocationMaster.GetAllActiveLocation();
            /*ViewBag.History = TaskHistory.GetTaskHistoryByNo(ticketNo).OrderByDescending(x => x.LogCreatedDate).tol*/
            ;
            if (!string.IsNullOrEmpty(ticketNo))
            {
                ViewData.Model = TicketMaster.GetTicketByID(ticketNo);
            }
            else
            {
                ViewData.Model = new TicketMaster();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult TicketAddEdit(TicketMaster model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.TicketNo != null)
            {
                TicketMaster ticket = TicketMaster.GetTicketByID(model.TicketNo);
                if (ticket != null)
                {
                    ticket.TicketHeader = model.TicketHeader;
                    ticket.TicketSubject = model.TicketSubject;
                    ticket.TicketType = model.TicketType;
                    ticket.UserId = model.UserId;
                    ticket.Location = model.Location;
                    ticket.Priority = model.Priority;
                    ticket.UpdatedBy = userInfo.Name;
                    ticket.DepartementID = model.DepartementID;
                    ticket.UpdatedDate = DateTime.Now;
                    ticket.UpdateSave<TicketMaster>();
                    var emaillist = TicketMaster.GetEmailrecipient(ticket.TicketType1.UserGroupId.GetValueOrDefault());
                    var msg = "<br>Ticket No : " + model.TicketNo;
                    msg = msg + "<br>Ticket Header : " + ProblemType.GetProblemTypeByID(int.Parse(model.TicketHeader)).ProblemTypeName;
                    msg = msg + "<br>Ticket Subject : " + model.TicketSubject;
                    msg = msg + "<br>Requestor : " + model.UserId;
                    foreach (var item in emaillist)
                    {
                        Task.Run(() => { GlobalFunction.SendMailWorkFlow(item.Email, item.Name, " Your Group Have new Complaint Ticket, Please Check Application" + msg); }).Wait();
                    }
                    TaskHistory.addTaskLog(ticket, "TICKET UPDATED BY " + userInfo.Name);
                }


            }
            else
            {
                int getTotalRequestToday = TicketMaster.totalTicket();
                getTotalRequestToday = getTotalRequestToday + 1;
                string No = "TIC-" + DateTime.Now.ToString("dd/MM/yyyy") + "-" + getTotalRequestToday.ToString();
                model.TicketNo = No;
                model.Status = "D";
                model.CreatedDate = DateTime.Now;
                model.TicketDate = model.TicketDate;
                model.CreatedBy = userInfo.Name;
                model.IsDeleted = false;
                model.InsertSave<TicketMaster>();
                var emailList = TicketMaster.GetEmailrecipient(model.TicketType1.UserGroupId.GetValueOrDefault()).ToList();
                var msg = "<br>Ticket No : " + model.TicketNo;
                msg = msg + "<br>Ticket Header : " + ProblemType.GetProblemTypeByID(int.Parse(model.TicketHeader)).ProblemTypeName;
                msg = msg + "<br>Ticket Subject : " + model.TicketSubject;
                msg = msg + "<br>Requestor : " + model.UserId;
                foreach (var item in emailList)
                {
                    Task.Run(() => { GlobalFunction.SendMailWorkFlow(item.Email, item.Name, " Your Group Have new Complaint Ticket, Please Check Application" + msg); }).Wait();
                }
                TaskHistory.addTaskLog(model, "TICKET CREATED BY " + userInfo.Name);
            }
            return RedirectToAction(MVC.Ticket.TicketDashboard());

        }
        public virtual ActionResult DeleteTicket(string ticketNo)
        {
            PrivilegeModel privilege = GetPrivilege();
            UserTb userInfo = (UserTb)Session["User"];
            if (privilege.IsAdmin == "Y")
            {
                if (!string.IsNullOrEmpty(ticketNo))
                {
                    TicketMaster ticket = TicketMaster.GetTicketByID(ticketNo);
                    ticket.IsDeleted = true;
                    ticket.UpdatedBy = userInfo.Name;
                    ticket.UpdatedDate = DateTime.Now;
                    ticket.UpdateSave<TicketMaster>();
                    TaskHistory.addTaskLog(ticket, "TICKET CREATED BY " + ticket.UpdatedBy);
                }
            }
            return RedirectToAction(MVC.Ticket.TicketDashboard());
        }
        public virtual ActionResult TicketTypeList()
        {
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.privilege = privilege;
            return View();
        }
        public virtual ActionResult TicketTypeDelete(int typeId)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (typeId != 0)
            {
                var type = TicketType.GetTicketTypeByID(typeId);
                if (type != null)
                {
                    type.IsDeleted = true;
                    type.CreatedDate = DateTime.Now;
                    type.CreatedBy = userInfo.Name;
                    type.UpdateSave<TicketType>();
                }
            }
            return RedirectToAction(MVC.Ticket.TicketTypeList());
        }
        public virtual ActionResult TicketTypeAddEdit(int typeId)
        {
            var usergroup = UserGroup.GetAllActive().ToList();
            ViewBag.UserGroup = usergroup;
            if (typeId != 0)
            {
                ViewData.Model = TicketType.GetTicketTypeByID(typeId);
            }
            else
            {
                ViewData.Model = new TicketType();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult TicketTypeAddEdit(TicketType model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.TicketTypeID != 0)
            {
                TicketType type = TicketType.GetTicketTypeByID(model.TicketTypeID);
                if (type != null)
                {
                    type.TicketTypeName = model.TicketTypeName;
                    type.UserGroupId = model.UserGroupId;
                    type.UpdatedBy = userInfo.Name;
                    type.UpdatedDate = DateTime.Now;
                    type.UpdateSave<TicketType>();
                }

            }
            else
            {
                model.CreatedBy = userInfo.UserName;
                model.CreatedDate = DateTime.Now;

                model.IsDeleted = false;
                model.InsertSave<TicketType>();
            }
            return RedirectToAction(MVC.Ticket.TicketTypeList());
        }

        public virtual ActionResult ProblemList(int id)
        {
            PrivilegeModel privilege = GetPrivilege();
            if (privilege == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.privilege = privilege;
            ViewBag.TicketName = TicketType.GetTicketTypeName(id);
            ViewBag.ticketid = id;
            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult GetAjaxProblemData(string sidx, string sord, int? page, int rows, int id)
        {
            List<model.ProblemModel> probModel = new List<model.ProblemModel>();
            int pageSize = rows;
            probModel = ProblemType.GetAllProblemTypeByTicket(id).OrderByDescending(x => x.CreatedDate).Select(x => new model.ProblemModel
            {
                ProblemTypeID = x.ProblemTypeID,
                ProblemTypeName = x.ProblemTypeName,
                CreatedDate = x.CreatedDate

            }).ToList();
            var rowSize = probModel.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = probModel.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult ProblemAddEdit(int id, int ticketId)
        {

            if (id != 0)
            {
                ProblemType problemModel = ProblemType.GetProblemTypeByID(id);
                problemModel.TicketTypeID = ticketId;
                ViewData.Model = problemModel;

            }
            else
            {
                ProblemType problemModel = new ProblemType();
                problemModel.TicketTypeID = ticketId;
                ViewData.Model = problemModel;
            }
            return View();
        }
        public virtual ActionResult ProblemDelete(int id, int ticketId)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (id != 0)
            {
                ProblemType prob = ProblemType.GetProblemTypeByID(id);
                if (prob != null)
                {

                    prob.IsDeleted = true;

                    prob.UpdatedBy = userInfo.UserName;
                    prob.UpdatedDate = DateTime.Now;
                    prob.UpdateSave<ProblemType>();
                }
            }

            return RedirectToAction(MVC.Ticket.ProblemList(ticketId));
        }
        [HttpPost]
        public virtual ActionResult ProblemAddEdit(ProblemType model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.ProblemTypeID != 0)
            {
                ProblemType prob = ProblemType.GetProblemTypeByID(model.ProblemTypeID);
                if (prob != null)
                {
                    if (!string.IsNullOrEmpty(prob.ProblemTypeName))
                    {
                        prob.ProblemTypeName = model.ProblemTypeName;
                    }
                    prob.UpdatedBy = userInfo.UserName;
                    prob.UpdatedDate = DateTime.Now;
                    prob.UpdateSave<ProblemType>();
                }
            }
            else
            {
                model.CreatedBy = userInfo.UserName;
                model.CreatedDate = DateTime.Now;
                model.IsDeleted = false;
                model.InsertSave<ProblemType>();
            }
            return RedirectToAction(MVC.Ticket.ProblemList(model.TicketTypeID.GetValueOrDefault()));
        }
        public virtual JsonResult GetProblemType(int ticketId)
        {
            var result = ProblemType.GetAllProblemTypeByTicket(ticketId).
                         Select(e => new
                         {
                             e.ProblemTypeID,
                             e.ProblemTypeName
                         });
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult TakeTicketCase(string ticketNo)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!string.IsNullOrEmpty(ticketNo))
            {
                TicketMaster ticket = TicketMaster.GetTicketByID(ticketNo);

                if (ticket != null)
                {
                    ticket.PIC = userInfo.UserID;
                    ticket.UpdatedBy = userInfo.Name;
                    ticket.UpdatedDate = DateTime.Now;
                    ticket.Status = "T";
                    ticket.UpdateSave<TicketMaster>();
                    TaskHistory.addTaskLog(ticket, "TICKET TAKEN BY " + ticket.UpdatedBy);
                }
            }
            return RedirectToAction(MVC.Ticket.TicketDashboard());
        }
        public virtual ActionResult AddCommentOnTask(string comment, string TicketNo)
        {
            TicketMaster ticket = new TicketMaster();
            ticket.TicketNo = TicketNo;
            TaskHistory.addTaskLog(ticket, comment);
            return RedirectToAction(MVC.Ticket.ViewTaskDetail(TicketNo));
        }
        public virtual ActionResult ViewTask(int userid)
        {

            ViewData.Model = userid;
            return View();
        }
        public virtual ActionResult ViewTaskHistory()
        {
            ViewData.Model = UserId;
            return View();
        }
        public virtual JsonResult AjaxGetTaskListData(common.TicketGridModel model, string sidx, string sord, int? page, int rows)
        {

            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<model.TicketMasterModel> ticketModel = new List<model.TicketMasterModel>();
            int pageSize = rows;
            ticketModel = TicketMaster.GetTaskList(model.id).OrderByDescending(x => x.CreatedDate).ToList();
            var rowSize = ticketModel.Count();

            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            //if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            //{
            //  dataTicket =  dataTicket.OrderBy(sortColumn + " " + sortColumnDir);
            //}

            var data = ticketModel.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        //public virtual JsonResult GetProblemListActive()
        //{
        //    PrivilegeModel privilege = GetPrivilege();
        //    int usergroup = int.Parse(Session["group"].ToString());

        //}
        public virtual ActionResult ViewTaskDetail(string ticketNo)
        {
            ViewBag.History = TaskHistory.GetTaskHistoryByNo(ticketNo).OrderByDescending(x => x.LogCreatedDate).ToList();
            ViewData.Model = TicketMaster.GetTicketByID(ticketNo);

            return View();
        }
        public virtual ActionResult SetTaskAsDone(string ticketNo, string moduleId)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (!string.IsNullOrEmpty(ticketNo))
            {
                TicketMaster TaskData = TicketMaster.GetTicketByID(ticketNo);
                if (TaskData != null)
                {
                    TaskData.Status = "S";
                    TaskData.UpdateSave<TicketMaster>();
                    TaskHistory.addTaskLog(TaskData, "TICKET SOLVED BY " + userInfo.Name);
                    bool isWorkflow = WorkFlowChild.CheckIfWorkFlowExist(moduleId);
                    if (isWorkflow)
                    {
                        WorkFlowTransaction.AddtoWorkFlow(moduleId, "Ticketing WorkFlow", userInfo.Name, ticketNo, 1);

                    }
                    else
                    {
                        TaskData.Status = "C";
                        TaskData.UpdateSave<TicketMaster>();
                        TaskHistory.addTaskLog(TaskData, "TICKET CLOSED BY " + userInfo.Name);
                    }
                }

            }
            return RedirectToAction(MVC.Ticket.ViewTaskDetail(ticketNo));
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetJSONData(model.TicketMasterModel model, string sidx, string sord, int? page, int rows)
        {

            if (model.CreatedDate != null)
            {
                model.ComparedDate = model.CreatedDate.GetValueOrDefault();
            }
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<TicketMaster> dataTicket = new List<TicketMaster>();
            List<model.TicketMasterModel> ticketModel = new List<model.TicketMasterModel>();
            int pageSize = rows;
            if (privilege.IsAdmin == "Y")
            {
                ticketModel = TicketMaster.GetTicketMasterModelAll(model).OrderByDescending(x => x.CreatedDate).ToList();
            }
            else
            {
                ticketModel = TicketMaster.GetTicketMasterModelByGroup(usergroup, model).OrderByDescending(x => x.CreatedDate).ToList();
            }
            var rowSize = ticketModel.Count();

            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            //if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            //{
            //  dataTicket =  dataTicket.OrderBy(sortColumn + " " + sortColumnDir);
            //}

            var data = ticketModel.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetTaskHistory(common.TicketGridModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());
            List<model.TicketMasterModel> ticketModel = new List<model.TicketMasterModel>();
            int pageSize = rows;
            var rowSize = ticketModel.Count();
            ticketModel = TicketMaster.GetTaskHistoryList(model.id).OrderByDescending(x => x.CreatedDate).ToList();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);


            var data = ticketModel.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult Report()
        {

            return View();
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetTicketReportData(ReportFilterModel model, string sidx, string sord, int? page, int rows)
        {
            PrivilegeModel privilege = GetPrivilege();
            int usergroup = int.Parse(Session["group"].ToString());

            List<TicketReportModel> ticketModel = new List<TicketReportModel>();
            int pageSize = rows;
            ticketModel = TicketMaster.GetAllTicketWithFilter(model).OrderByDescending(x => x.CreatedDate).Select(x => new model.TicketReportModel
            {
                TicketNo = x.TicketNo,
                TicketType = x.TicketType,
                TicketSubject = x.TicketSubject,
                CreatedDate = x.CreatedDate,
                PIC = x.PIC,
                Departement = x.Departement,
                Location = x.Location,
                UserId = x.UserId,
                Priority = x.Priority == "L" ? "LOW" : x.Priority == "M" ? "MEDIUM" : "HIGH",
                Status = x.Status
                //Status = x.Status == "D" ? "DRAFT" : x.Status == "S" ? "SUBMIT" : x.Status == "T" ? "TAKEN" : "CLOSED"
            }).ToList();
            var rowSize = ticketModel.Count();

            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            //if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            //{
            //  dataTicket =  dataTicket.OrderBy(sortColumn + " " + sortColumnDir);
            //}

            var data = ticketModel.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            Session["ticketReport"] = ticketModel;
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult ExportToExcel()
        {

            var gv = new GridView();
            gv.DataSource = (List<model.TicketReportModel>)Session["ticketReport"];
            gv.DataBind();
            common.GlobalFunction.ConvertToExcelHelper(Response, gv, "TicketReport");
            return RedirectToAction(MVC.Ticket.Report());
        }
        public virtual JsonResult AjaxGetTicketTypeList(string sidx, string sord, int? page, int rows)
        {
            List<model.TicketTypeModel> ticTypeModel = new List<TicketTypeModel>();
            int pageSize = rows;
            ticTypeModel = TicketType.GetAllTicketType().OrderByDescending(x => x.CreatedDate).Select(x => new model.TicketTypeModel
            {
                TicketTypeID = x.TicketTypeID,
                TicketTypeName = x.TicketTypeName,
                CreatedBy = x.CreatedBy,
                CreatedDate = x.CreatedDate,
                UserGroup = x.UserGroup.UserGroupName

            }).ToList();
            var rowSize = ticTypeModel.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = ticTypeModel.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }

    }
}