﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data;
using PIRApps.common;
using PIRApps.model;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class UserController : BaseController
    {
        // GET: User
        public virtual ActionResult List()
        {
            PrivilegeModel privilege = GetPrivilege();
            ViewBag.privilege = privilege;
            return View();
        }
        public virtual ActionResult AddEdit(int UserId)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            var usergroup = UserGroup.GetAllActive().ToList();
            //foreach (var item in usergroup)
            //{
            //    items.Add(new SelectListItem { Text = item.UserGroupName, Value = item.UserGroupID.ToString()});
            //}
            var users = UserTb.GetUserByID(UserId);
            ViewBag.UserGroup = usergroup;
            if (UserId != 0)
            {
               
                if (!string.IsNullOrEmpty(users.Password))
                {
                    users.Password = common.Security.DataEncription.Decrypt(users.Password);
                }
                ViewData.Model = users;
            }
            else
            {
                ViewData.Model = new UserTb();
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult AddEdit(UserTb model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.UserID != 0)
            {
               
                var currentUser = UserTb.GetUserByID(model.UserID);
                if (currentUser != null)
                {
                    if (!string.IsNullOrEmpty(model.Password))
                    {
                        currentUser.Password = common.Security.DataEncription.Encrypt(model.Password);

                    }
                    if (!string.IsNullOrEmpty(model.Name))
                    {
                        currentUser.Name = model.Name;
                    }
                    currentUser.UpdatedDate = DateTime.Now;
                    currentUser.UpdatedBy = userInfo.Name;
                    if (model.UserGroupID != 0)
                    {
                        currentUser.UserGroupID = model.UserGroupID;
                    }
                    if (!string.IsNullOrEmpty(model.Email))
                    {
                        currentUser.Email = model.Email;
                    }
                    currentUser.UpdateSave<UserTb>();
                }
               
            }
            else
            {
                var newUser = new UserTb();
                if (!string.IsNullOrEmpty(model.UserName))
                {
                    newUser.UserName = model.UserName;

                }
                if (!string.IsNullOrEmpty(model.Password))
                {
                    newUser.Password = common.Security.DataEncription.Encrypt(model.Password);

                }
                if (!string.IsNullOrEmpty(model.Name))
                {
                    newUser.Name = model.Name;
                }
                newUser.CreatedDate = DateTime.Now;
                newUser.CreatedBy = userInfo.Name;
                if (model.UserGroupID != 0)
                {
                    newUser.UserGroupID = model.UserGroupID;
                }
                if (!string.IsNullOrEmpty(model.Email))
                {
                    newUser.Email = model.Email;
                }
                newUser.IsDeleted = false;
                newUser.InsertSave<UserTb>();
            }
            return RedirectToAction(MVC.User.List());
        }
        public virtual ActionResult DeleteUser(int UserId)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (UserId != 0)
            {
                var user = UserTb.GetUserByID(UserId);
                if (user != null)
                {
                    user.IsDeleted = true;
                    user.CreatedBy = userInfo.Name;
                    user.CreatedDate = DateTime.Now;
                    user.UpdateSave<UserTb>();
                }
            }
            return RedirectToAction(MVC.User.List());
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetUserGroupList(string sidx, string sord, int? page, int rows)
        {
            List<model.UserGroupModel> usGroup = new List<UserGroupModel>();
            int pageSize = rows;
            usGroup = UserGroup.GetAllActive().OrderByDescending(x => x.CreatedDate).Select(x => new model.UserGroupModel
            {
                UserGroupID = x.UserGroupID,
                UserGroupName = x.UserGroupName,
                CreatedBy = x.CreatedBy,
                CreatedDate = x.CreatedDate
            }).ToList();
            var rowSize = usGroup.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = usGroup.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetUserData(string sidx, string sord, int? page, int rows)
        {
            List<model.UserModel> UsMod = new List<model.UserModel>();
            int pageSize = rows;
            UsMod = UserTb.GetAllActiveUser().OrderByDescending(x => x.CreatedDate).Select(x => new model.UserModel
            {
                UserId = x.UserID,
                UserName = x.UserName,
                Name = x.Name,
                Email = x.Email,
                UserGroup = x.UserGroup.UserGroupName
            }).ToList();
            var rowSize = UsMod.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = UsMod.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult UserGroupList()
        {
            PrivilegeModel privilege = GetPrivilege();
            ViewBag.privilege = privilege;
            return View();
        }
        public virtual ActionResult GroupAddEdit(int GroupId)
        {
            if (GroupId != 0)
            {
                ViewData.Model = UserGroup.GetByID(GroupId);
            }
            else
            {
                ViewData.Model = new UserGroup();

            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult GroupAddEdit(UserGroup model)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (model.UserGroupID != 0)
            {
                UserGroup ug = UserGroup.GetByID(model.UserGroupID);
                if (ug != null)
                {
                    ug.UserGroupName = model.UserGroupName;
                    ug.UpdatedBy = userInfo.UserName;
                    ug.UpdatedDate = DateTime.Now;
                    ug.UpdateSave<UserGroup>();
                }

            }
            else
            {
                model.CreatedDate = DateTime.Now;
                model.CreatedBy = userInfo.UserName;
                model.IsDeleted = false;
                model.InsertSave<UserGroup>();
            }
            return RedirectToAction(MVC.User.UserGroupList());
        }
        public virtual ActionResult GroupUserDelete(int GroupId)
        {
            UserTb userInfo = (UserTb)Session["User"];
            if (GroupId != 0)
            {
                var group = UserGroup.GetByID(GroupId);
                if (group != null)
                {
                    group.IsDeleted = true;
                    group.UpdatedDate = DateTime.Now;
                    group.UpdatedBy = userInfo.Name;
                    group.UpdateSave<UserGroup>();
                }
            }
            return RedirectToAction(MVC.User.UserGroupList());
        }
    }
}