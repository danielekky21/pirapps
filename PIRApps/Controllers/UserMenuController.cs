﻿using PIRApps.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class UserMenuController : BaseController

    {
        // GET: UserMenu
        public virtual ActionResult SetMenu(int UserId,string username)
        {
            if (UserId != 0)
            {
                ViewBag.userid = UserId;
                ViewBag.UserName = username;
                var menuActive = data.MsUserMenu.GetAllActiveMenu().ToList();
                var currentUserMenu = data.MsUserMenu.getUserMenu(UserId).ToList();
                
                ViewBag.menuActive = menuActive;
                ViewBag.currentMenu = currentUserMenu;
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult SetMenu(IEnumerable<model.UserMenuModel> model,string UserName,int UserId)
        {
            data.UserTb userInfo = (data.UserTb)Session["User"];
            List<data.MsUserMenu> menuList = new List<data.MsUserMenu>();
            //populate user menu per module
            var currentUserMenu = data.MsUserMenu.getUserMenu(UserId).ToList();
            //clear current menu
            foreach (var items in currentUserMenu)
            {
                items.Delete<data.MsUserMenu>();
            }
            foreach (var item in model)
            {   
                data.MsUserMenu dataMenu = new data.MsUserMenu();
                if (item.CreateData)
                {
                    dataMenu.CreateData = "Y";
                }
                else
                {
                    dataMenu.CreateData = "N";
                }
                if (item.UpdateData)
                {
                    dataMenu.UpdateData = "Y";
                }
                else
                {
                    dataMenu.UpdateData = "N";
                }
                if (item.ReadData)
                {
                    dataMenu.ReadData = "Y";
                }
                else
                {
                    dataMenu.ReadData = "N";
                }
                if (item.DeleteData)
                {
                    dataMenu.DeleteData = "Y";
                }
                else
                {
                    dataMenu.DeleteData = "N";
                }
                if (item.isAdmin)
                {
                    dataMenu.IsAdmin = "Y";
                }
                else
                {
                    dataMenu.IsAdmin = "N";
                }
                if (item.IsShow)
                {
                    dataMenu.IsShow = "Y";
                }
                else
                {
                    dataMenu.IsShow = "N";
                }
                dataMenu.MenuID = item.MenuId;
                dataMenu.UserID = UserId;
                dataMenu.UserName = UserName;
                dataMenu.UpdatedDate = DateTime.Now;
                dataMenu.UpdatedBy = userInfo.Name;
                dataMenu.InsertSave<data.MsUserMenu>();
            }
            return RedirectToAction(MVC.UserMenu.SetMenu(UserId,UserName));
        }
    }
}