﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data;
using PIRApps.common;

namespace PIRApps.Controllers
{
    public partial class WorkFlowController : BaseController
    {
        [PrivilegeSession]
        // GET: WorkFlow
        public virtual ActionResult List()
        {

            int userid = int.Parse(Session["UserId"].ToString());
            ViewData.Model = data.WorkFlowTransaction.GetAllActiveWorkFlowByUserName(userid).OrderByDescending(x => x.CreatedDate).ToList();
            return View();
        }
        public virtual ActionResult WorkFlowDetail(int WorkFlowid)
        {
            var model= data.WorkFlowTransaction.GetWorkFlowByUniqueID(WorkFlowid);
            ViewBag.KeyField = WorkFlowRule.GetWorkFLowRuleByModuleID(model.ModuleID);
            ViewData.Model = model;
            ViewBag.History = data.WorkFlowHistory.GetHistoryByTableKeyID(model.KeytTableID).OrderByDescending(x => x.WorkFlowHistoryDate).ToList();
            return View();
        }
        public virtual JsonResult AjaxGetWorkFlowHistoryData(string sidx, string sord, int? page, int rows)
        {
            int pageSize = rows;
            var workflowhistory = WorkFlowTransaction.GetAllNotActiveWorkFlowByUserName(UserId).OrderByDescending(x => x.UpdatedDate).ToList();
            var rowSize = workflowhistory.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = workflowhistory.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        public virtual ActionResult WorkFlowHistoryList()
        {
            return View();
        }
        public virtual ActionResult SetApproveWorkflow(int WorkFlowid)
        {
            UserTb userInfo = (UserTb)Session["User"];
            WorkFlowTransaction data = WorkFlowTransaction.GetWorkFlowByUniqueID(WorkFlowid);
            WorkFlowTransaction.SetApproveWorkFlow(data, userInfo.Name);
            bool value = WorkFlowRule.CheckIfNextLevelExist(data.WorkFlowIdx.GetValueOrDefault(), data.ModuleID);
            if (value)
            {
                WorkFlowTransaction.addHistory(data.KeytTableID, "APPROVED", userInfo.Name);
                var nextValue = WorkFlowTransaction.nextIdxVal(data.ModuleID, data.WorkFlowIdx.GetValueOrDefault());
                WorkFlowTransaction.AddtoWorkFlow(data.ModuleID, data.WorkFlowName, userInfo.Name, data.KeytTableID, nextValue);
            }
            else
            {
                WorkFlowTransaction.addHistory(data.KeytTableID, "APPROVED and CLOSED", userInfo.Name);
                WorkFlowTransaction.UpdateTableAfterWorkFlowDone(data, userInfo.Name);
            }
            return RedirectToAction(MVC.WorkFlow.List());
        }
        [HttpPost]
        public virtual ActionResult SetRejectWorkFlow(string Reason ,int id)
        {
            UserTb userInfo = (UserTb)Session["User"];
            WorkFlowTransaction data = WorkFlowTransaction.GetWorkFlowByUniqueID(id);
            if (WorkFlowTransaction.SetRejectWorkFlow(data, userInfo.Name))
            {
                WorkFlowTransaction.addHistory(data.KeytTableID, "REJECTED, Reason : " + Reason, userInfo.Name);
                WorkFlowTransaction.UpdateTableAfterWorkFlowReject(data, userInfo.Name, Reason);
            }
            return RedirectToAction(MVC.WorkFlow.List());
        }

    }
}