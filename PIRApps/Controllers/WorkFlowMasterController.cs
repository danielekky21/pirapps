﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PIRApps.data;
using PIRApps.common;
using PIRApps.model;

namespace PIRApps.Controllers
{
    [PrivilegeSession]
    public partial class WorkFlowMasterController : BaseController
    {
        // GET: WorkFlowMaster
        public virtual ActionResult Index()
        {
            return View();
        }
        public virtual ActionResult WorkFlowMasterList()
        {
            PrivilegeModel privilege = GetPrivilege();
            ViewBag.privilege = privilege;
            
            return View();
        }
        public virtual ActionResult EditRule(string moduleID)
        {
            var rule = WorkFlowTransaction.GetWorkflowChildByModuleID(moduleID);

            if (rule != null)
            {
                var moduleName = WorkFlowTransaction.GetModuleNameByModuleID(moduleID);
                ViewData.Model = rule.ToList();
                ViewBag.UserList = data.UserTb.GetAllActiveUser();
                ViewBag.ModuleName = moduleName;
                ViewBag.ModuleID = rule.FirstOrDefault().ModuleID;
            }
            return View();
        }
        [HttpPost]
        public virtual ActionResult EditRule(List<int> UserApproval, string ModuleID)
        {
            int index = 1;
            var rule = WorkFlowTransaction.GetWorkflowRuleByModuleID(ModuleID);
            rule.ApproveLevel = UserApproval.Count;
            rule.UpdateSave<data.WorkFlowRule>();
            IList<data.WorkFlowChild> childItem = WorkFlowTransaction.GetWorkflowChildByModuleID(ModuleID).ToList();
            //delete initial Data
            foreach (var item in childItem)
            {
                item.Delete<data.WorkFlowChild>();
            }
            foreach (var item in UserApproval)
            {
                data.WorkFlowChild model = new data.WorkFlowChild();
                model.ModuleID = ModuleID;
                model.UserApproval = item;
                model.WorkFlowIdx = index;
                model.IsDeleted = false;
                model.InsertSave<data.WorkFlowChild>();
                index++;

            }
           
            //foreach (var item in UserApproval)
            //{

            //}
            return RedirectToAction(MVC.WorkFlowMaster.WorkFlowMasterList());
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetWorkFlowMasterData(string sidx, string sord, int? page, int rows)
        {
            List<data.WorkFlowRule> model = new List<data.WorkFlowRule>();
            int pageSize = rows;
            model = WorkFlowTransaction.GetWorkFlowModule().ToList();
            var rowSize = model.Count();
            if (rowSize < 1)
            {
                rowSize = 1;
            }
            int totalRecord = Convert.ToInt32(rowSize);
            int totalPages = (int)Math.Ceiling((float)totalRecord / (float)pageSize);
            var data = model.Skip((page.GetValueOrDefault() - 1) * pageSize).Take(pageSize).ToList();
            var jsonData = new
            {
                total = totalPages,
                page,
                records = totalRecord,
                rows = data
            };
            return Json(jsonData, JsonRequestBehavior.AllowGet);
        }
        [AcceptVerbs(HttpVerbs.Post)]
        public virtual JsonResult AjaxGetUserActive()
        {
            List<data.UserTb> userlist = data.UserTb.GetAllActiveUser().ToList();
            return Json(userlist.Select(x => new {
                UserID = x.UserID,
                UserName = x.UserName
            }), JsonRequestBehavior.AllowGet);
        }
    }
}