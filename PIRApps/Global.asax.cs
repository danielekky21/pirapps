﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using PIRApps.data;
using PIRApps.App_Start;

namespace PIRApps
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private class HttpDataContextDataStore : IDataRepositoryStore
        {
            public object this[string key]
            {
                get { return HttpContext.Current.Items[key]; }
                set { HttpContext.Current.Items[key] = value; }
            }
        }
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            DataRepositoryStore.CurrentDataStore = new HttpDataContextDataStore();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            MvcHandler.DisableMvcResponseHeader = true;
        }
    }
}
