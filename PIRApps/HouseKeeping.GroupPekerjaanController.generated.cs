// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace PIRApps.Areas.HouseKeeping.Controllers
{
    public partial class GroupPekerjaanController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public GroupPekerjaanController() { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected GroupPekerjaanController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult AjaxGetJSONData()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.AjaxGetJSONData);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult AddEdit()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AddEdit);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public GroupPekerjaanController Actions { get { return MVC.HouseKeeping.GroupPekerjaan; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "HouseKeeping";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "GroupPekerjaan";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "GroupPekerjaan";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string Index = "Index";
            public readonly string List = "List";
            public readonly string AjaxGetJSONData = "AjaxGetJSONData";
            public readonly string AddEdit = "AddEdit";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string Index = "Index";
            public const string List = "List";
            public const string AjaxGetJSONData = "AjaxGetJSONData";
            public const string AddEdit = "AddEdit";
        }


        static readonly ActionParamsClass_AjaxGetJSONData s_params_AjaxGetJSONData = new ActionParamsClass_AjaxGetJSONData();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_AjaxGetJSONData AjaxGetJSONDataParams { get { return s_params_AjaxGetJSONData; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_AjaxGetJSONData
        {
            public readonly string model = "model";
            public readonly string sidx = "sidx";
            public readonly string sord = "sord";
            public readonly string page = "page";
            public readonly string rows = "rows";
        }
        static readonly ActionParamsClass_AddEdit s_params_AddEdit = new ActionParamsClass_AddEdit();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_AddEdit AddEditParams { get { return s_params_AddEdit; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_AddEdit
        {
            public readonly string id = "id";
            public readonly string model = "model";
            public readonly string mutliselect_to = "mutliselect_to";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string AddEdit = "AddEdit";
                public readonly string List = "List";
            }
            public readonly string AddEdit = "~/Areas/HouseKeeping/Views/GroupPekerjaan/AddEdit.cshtml";
            public readonly string List = "~/Areas/HouseKeeping/Views/GroupPekerjaan/List.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_GroupPekerjaanController : PIRApps.Areas.HouseKeeping.Controllers.GroupPekerjaanController
    {
        public T4MVC_GroupPekerjaanController() : base(Dummy.Instance) { }

        [NonAction]
        partial void IndexOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult Index()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
            IndexOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void ListOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult List()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.List);
            ListOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void AjaxGetJSONDataOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, PIRApps.model.UserGroupModel model, string sidx, string sord, int? page, int rows);

        [NonAction]
        public override System.Web.Mvc.JsonResult AjaxGetJSONData(PIRApps.model.UserGroupModel model, string sidx, string sord, int? page, int rows)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.AjaxGetJSONData);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "model", model);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "sidx", sidx);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "sord", sord);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "page", page);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "rows", rows);
            AjaxGetJSONDataOverride(callInfo, model, sidx, sord, page, rows);
            return callInfo;
        }

        [NonAction]
        partial void AddEditOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string id);

        [NonAction]
        public override System.Web.Mvc.ActionResult AddEdit(string id)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AddEdit);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "id", id);
            AddEditOverride(callInfo, id);
            return callInfo;
        }

        [NonAction]
        partial void AddEditOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, PIRApps.data.GroupPekerjaanHeader model, string[] mutliselect_to);

        [NonAction]
        public override System.Web.Mvc.ActionResult AddEdit(PIRApps.data.GroupPekerjaanHeader model, string[] mutliselect_to)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AddEdit);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "model", model);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "mutliselect_to", mutliselect_to);
            AddEditOverride(callInfo, model, mutliselect_to);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
