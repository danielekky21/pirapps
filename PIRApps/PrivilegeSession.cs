﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PIRApps
{
    public class PrivilegeSession : System.Web.Mvc.ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Session["UserId"] == null)
            {
               filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "home" },{ "action", "login" },{ "area",""} });
            }
            if (filterContext.RouteData.Values["id"] != null)
            {
                if (HttpContext.Current.Session["UserId"] == null)
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "home" }, { "action", "login" }, { "area", "" } });
                }
                else
                {
                    int menuid = int.Parse(filterContext.RouteData.Values["id"].ToString());
                    int userid = int.Parse(HttpContext.Current.Session["UserId"].ToString());
                    var GetPrivilegeModel = data.Privilege.GetPrivilegeToModel(menuid, userid);
                    //if (GetPrivilegeModel.isShow != "Y")
                    //{
                    //    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "home" }, { "action", "login" } });
                    //}


                    //var GetPrivilege = data.MsUserMenu.GetPrivilegeByMenuId(menuid, userid);

                    if (GetPrivilegeModel != null)
                    {
                        HttpContext.Current.Session["privilege"] = GetPrivilegeModel;
                    }

                    string usergroup = data.UserTb.GetUserGroup(userid).ToString();
                    HttpContext.Current.Session["group"] = usergroup;
                }
            }
            
            base.OnActionExecuting(filterContext);
        }
    }
}
