// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
// 0114: suppress "Foo.BarController.Baz()' hides inherited member 'Qux.BarController.Baz()'. To make the current member override that implementation, add the override keyword. Otherwise add the new keyword." when an action (with an argument) overrides an action in a parent controller
#pragma warning disable 1591, 3008, 3009, 0108, 0114
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace PIRApps.Controllers
{
    public partial class WorkFlowController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public WorkFlowController() { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected WorkFlowController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult WorkFlowDetail()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.WorkFlowDetail);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.JsonResult AjaxGetWorkFlowHistoryData()
        {
            return new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.AjaxGetWorkFlowHistoryData);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult SetApproveWorkflow()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SetApproveWorkflow);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult SetRejectWorkFlow()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SetRejectWorkFlow);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public WorkFlowController Actions { get { return MVC.WorkFlow; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "WorkFlow";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "WorkFlow";
        [GeneratedCode("T4MVC", "2.0")]
        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string List = "List";
            public readonly string WorkFlowDetail = "WorkFlowDetail";
            public readonly string AjaxGetWorkFlowHistoryData = "AjaxGetWorkFlowHistoryData";
            public readonly string WorkFlowHistoryList = "WorkFlowHistoryList";
            public readonly string SetApproveWorkflow = "SetApproveWorkflow";
            public readonly string SetRejectWorkFlow = "SetRejectWorkFlow";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string List = "List";
            public const string WorkFlowDetail = "WorkFlowDetail";
            public const string AjaxGetWorkFlowHistoryData = "AjaxGetWorkFlowHistoryData";
            public const string WorkFlowHistoryList = "WorkFlowHistoryList";
            public const string SetApproveWorkflow = "SetApproveWorkflow";
            public const string SetRejectWorkFlow = "SetRejectWorkFlow";
        }


        static readonly ActionParamsClass_WorkFlowDetail s_params_WorkFlowDetail = new ActionParamsClass_WorkFlowDetail();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_WorkFlowDetail WorkFlowDetailParams { get { return s_params_WorkFlowDetail; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_WorkFlowDetail
        {
            public readonly string WorkFlowid = "WorkFlowid";
        }
        static readonly ActionParamsClass_AjaxGetWorkFlowHistoryData s_params_AjaxGetWorkFlowHistoryData = new ActionParamsClass_AjaxGetWorkFlowHistoryData();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_AjaxGetWorkFlowHistoryData AjaxGetWorkFlowHistoryDataParams { get { return s_params_AjaxGetWorkFlowHistoryData; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_AjaxGetWorkFlowHistoryData
        {
            public readonly string sidx = "sidx";
            public readonly string sord = "sord";
            public readonly string page = "page";
            public readonly string rows = "rows";
        }
        static readonly ActionParamsClass_SetApproveWorkflow s_params_SetApproveWorkflow = new ActionParamsClass_SetApproveWorkflow();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_SetApproveWorkflow SetApproveWorkflowParams { get { return s_params_SetApproveWorkflow; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_SetApproveWorkflow
        {
            public readonly string WorkFlowid = "WorkFlowid";
        }
        static readonly ActionParamsClass_SetRejectWorkFlow s_params_SetRejectWorkFlow = new ActionParamsClass_SetRejectWorkFlow();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_SetRejectWorkFlow SetRejectWorkFlowParams { get { return s_params_SetRejectWorkFlow; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_SetRejectWorkFlow
        {
            public readonly string Reason = "Reason";
            public readonly string id = "id";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string List = "List";
                public readonly string WorkFlowDetail = "WorkFlowDetail";
                public readonly string WorkFlowHistoryList = "WorkFlowHistoryList";
            }
            public readonly string List = "~/Views/WorkFlow/List.cshtml";
            public readonly string WorkFlowDetail = "~/Views/WorkFlow/WorkFlowDetail.cshtml";
            public readonly string WorkFlowHistoryList = "~/Views/WorkFlow/WorkFlowHistoryList.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_WorkFlowController : PIRApps.Controllers.WorkFlowController
    {
        public T4MVC_WorkFlowController() : base(Dummy.Instance) { }

        [NonAction]
        partial void ListOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult List()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.List);
            ListOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void WorkFlowDetailOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int WorkFlowid);

        [NonAction]
        public override System.Web.Mvc.ActionResult WorkFlowDetail(int WorkFlowid)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.WorkFlowDetail);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "WorkFlowid", WorkFlowid);
            WorkFlowDetailOverride(callInfo, WorkFlowid);
            return callInfo;
        }

        [NonAction]
        partial void AjaxGetWorkFlowHistoryDataOverride(T4MVC_System_Web_Mvc_JsonResult callInfo, string sidx, string sord, int? page, int rows);

        [NonAction]
        public override System.Web.Mvc.JsonResult AjaxGetWorkFlowHistoryData(string sidx, string sord, int? page, int rows)
        {
            var callInfo = new T4MVC_System_Web_Mvc_JsonResult(Area, Name, ActionNames.AjaxGetWorkFlowHistoryData);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "sidx", sidx);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "sord", sord);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "page", page);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "rows", rows);
            AjaxGetWorkFlowHistoryDataOverride(callInfo, sidx, sord, page, rows);
            return callInfo;
        }

        [NonAction]
        partial void WorkFlowHistoryListOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult WorkFlowHistoryList()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.WorkFlowHistoryList);
            WorkFlowHistoryListOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void SetApproveWorkflowOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, int WorkFlowid);

        [NonAction]
        public override System.Web.Mvc.ActionResult SetApproveWorkflow(int WorkFlowid)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SetApproveWorkflow);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "WorkFlowid", WorkFlowid);
            SetApproveWorkflowOverride(callInfo, WorkFlowid);
            return callInfo;
        }

        [NonAction]
        partial void SetRejectWorkFlowOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string Reason, int id);

        [NonAction]
        public override System.Web.Mvc.ActionResult SetRejectWorkFlow(string Reason, int id)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SetRejectWorkFlow);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "Reason", Reason);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "id", id);
            SetRejectWorkFlowOverride(callInfo, Reason, id);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108, 0114
