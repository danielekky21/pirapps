jQuery(document).ready(function() {

// Chartist

    var options = {
        low: 0,
        high: 100,
        showArea: true,
        showPoint: false,
        fullWidth: true,
        axisY: {
            labelInterpolationFnc: function(value) {
                return '$'+value+'k';
            },
            scaleMinSpace: 20
        },
        series: {
            'series-2': {
                showArea: true,
                showPoint: true,
                fullWidth: true,
            },
            'series-1': {
                fullWidth: true,
            }
        }
    };
    var randomVal = [];
    var randomVal2 = [];
    generateArrayChart(randomVal);
    generateArrayChart2(randomVal2);
    setInterval(function(){
    generateOneNumSmall(randomVal);
    generateOneNumSmall(randomVal2);
    var data = {
        labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'],
        series: [{
            name: 'series-1',
            data: randomVal
        }, {
            name: 'series-2',
            data: randomVal2
        }]
    };

    new Chartist.Line("#fullChart", data, options);
    },1000)
    var data = {
        series: [1, 5]
    };

    var sum = function(a, b) { return a + b };

    new Chartist.Pie('#chartistPie', data, {
        labelInterpolationFnc: function(value) {
            return '';
        }
    });
    var randomArea = [];
    generateAreaChart(randomArea);
      setInterval(function(){
    generateAreaNum(randomArea);
    new Chartist.Line('#areaChart', {
        labels: [1 , 2 , 3 , 4 , 5 , 6 , 7],
        series: [
            randomArea
        ]
    }, {
    low: 0,
    showArea: true,
    fullWidth: true,
    fullWidth: true,
    showPoint: false,
    colors:["#f44336"],
    axisY: {
        showGrid: false,
        showLabel: false,
        offset: 0
    },
        axisX:{
        showGrid: false,
        showLabel: false,
        offset: 0
      },

        lineSmooth: true,
    });
},1000);
// Extrabar
    $("#layout-static .static-content-wrapper").append("<div class='extrabar-underlay'></div>");

// Calendar

    $('#calendar').datepicker({todayHighlight: true});

// Easypie chart
    try{
        $('.easypiechart#chart1').easyPieChart({
            barColor: "#00bcd4",
            trackColor: 'rgba(255,255,255,0.1)',
            scaleColor: 'transparent',
            scaleLength: 8,
            lineCap: 'round',
            lineWidth: 4,
            size: 144,
            onStep: function(from, to, percent) {
                $(this.el).find('.percent').text(Math.round(percent));
            }
        });
    }
    catch(e){}
    $('.progress-pie-chart').each(function(index, obj) {
        new Chartist.Pie(obj, {
            series: [$(obj).attr('data-percent'), 22]
        }, {
            labelInterpolationFnc: function(value) {
                return '';
            },
            width: '42px',
            height: '42px',
        });
    })

// Sparklines
    var sparker = function() {


        var barSpacing = ($('#dailysales2').width() - 13*6)/13;
        $("#biglines").sparkline([11,5,8,13,10,12,5,9,11], {
            type: 'line',
            width: '100%',
            height: '106px',
            lineWidth: 0.01,
            lineColor: '#fff',
            fillColor: '#e0e0e0',
            highlightSpotColor: '#b0bec5',
            highlightLineColor: '#b0bec5',
            chartRangeMin: 0,chartRangeMax: 20,
            spotRadius: 0
        });
        $("#biglines").sparkline([9,5,10,8,12,5,12,7,10], {
            type: 'line',
            width: '100%',
            height: '106px',
            lineWidth: 0.01,
            lineColor: '#fff',
            fillColor: '#3f51b5',
            highlightSpotColor: '#546e7a',
            highlightLineColor: '#546e7a',
            chartRangeMin: 0,
            chartRangeMax: 20,
            composite: true,
            spotRadius: 0
        });

        var randomNum = [];
        var randomNum1 = [];
        var randomNum2 = [];
        var randomNum3 = [];
        var randomNum4 = [];
        generateArrayNum(randomNum);
        generateArrayNum(randomNum1);
        generateArrayNum(randomNum2);
        generateArrayNum(randomNum3);
        generateArrayNum(randomNum4);
        if(typeof(sparklineInterval) != 'undefined') clearInterval(sparklineInterval);
        sparklineInterval = setInterval(function(){
        generateOneNum(randomNum);
        generateOneNum(randomNum1);
        generateOneNum(randomNum2);
        generateOneNum(randomNum3);
        generateOneNum(randomNum4);
        $("#dailysales, #dailysales2").sparkline(randomNum4, {
          type: 'bar',
          height: '144px',
          width: '100%',
          barWidth: 4,
          barSpacing: Math.floor(barSpacing),
          barColor: 'rgba(255,255,255,0.3)'});
           $('#dashboard-sparkline-indigo').sparkline(randomNum, { type: 'bar', barColor: 'rgba(255,255,255,0.5)', height: '48px',width: '100%', barWidth: 2, barSpacing: 4, spotRadius: 4, chartRangeMin: 1});
           $('#dashboard-sparkline-gray').sparkline(randomNum1, { type: 'bar', barColor: 'rgba(255,255,255,0.5)', height: '48px',width: '100%', barWidth: 2, barSpacing: 4, spotRadius: 4, chartRangeMin: 1});
           $('#dashboard-sparkline-primary').sparkline(randomNum2, { type: 'bar', barColor: 'rgba(255,255,255,0.5)', height: '48px',width: '100%', barWidth: 2, barSpacing: 4, spotRadius: 4, chartRangeMin: 1});
           $('#dashboard-sparkline-success').sparkline(randomNum3, { type: 'bar', barColor: 'rgba(255,255,255,0.5)', height: '48px',width: '100%', barWidth: 2, barSpacing: 4, spotRadius: 4, chartRangeMin: 1});
        },1000)






    }
    var sparkResize;

    $(window).resize(function(e) {
        clearTimeout(sparkResize);
        sparkResize = setTimeout(sparker, 500);
    });
    sparker();


});
function generateArrayNum(randomNum){
  for (var i = 0; i <= 9; i++) {
    var number = Math.floor(Math.random() * 1000);
    randomNum[i] = number;
  }
}
function generateArrayChart(randomVal){
  for (var i = 0; i <= 11; i++) {
    var number = Math.floor(Math.random() * 10);
    randomVal[i] = number;
  }
}
function generateArrayChart2(randomVal2){
  for (var i = 0; i <= 11; i++) {
    var number = Math.floor(Math.random() * 10);
    randomVal2[i] = number;
  }
}
function generateOneNum(randomNum){
  randomNum.splice(0,1);
  var number = Math.floor(Math.random() * 1000);
  randomNum.push(number);
}
function generateOneNumSmall(randomNum){
  randomNum.splice(0,1);
  var number = Math.floor(Math.random() * 100);
  randomNum.push(number);
}
function generateAreaChart(randomArea){
  for (var i = 0; i <= 6; i++) {
    var number = Math.floor(Math.random() * 10);
    randomArea[i] = number;
  }
}
function generateAreaNum(randomNum){
  randomNum.splice(0,1);
  var number = Math.floor(Math.random() * 10);
  randomNum.push(number);
}
