﻿function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
function delete_cookie(name) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}
function openMenu() {
    if (getCookie("classid") != "") {
        $('.dropdown-toggle.' + getCookie("classid"))
        parent = $('.dropdown-toggle.' + getCookie("classid")).parent();
        parent.addClass("open");
    }
}
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
$(document).ready(function () {
    openMenu();
    //$("#TicketAddForm").parsley();
    //$("#tblWF").dataTable();
    //$('.panel-ctrls').append($('.dataTables_filter').addClass("pull-right")).find("label").addClass("panel-ctrls-center");
    //$('.panel-ctrls').append("<i class='separator'></i>");
    //$('.panel-ctrls').append($('.dataTables_length').addClass("pull-left")).find("label").addClass("panel-ctrls-center");
    //$('.panel-footer').append($(".dataTable+.row"));
    //$('.dataTables_paginate>ul.pagination').addClass("pull-right m-n");
   
    
    $("#ticketType").change(function () {
        var $problem = $("#problem");
        removeOptionDropdown($problem);
        var url = $problem.data("url") + '?ticketId=' + $(this).val();
        //$.getJSON(url, function (items) {
        //    $.each(items, function (a, b) {
        //        $problem.append('<option value="' + b.ProblemTypeID + '">' + b.ProblemTypeName + '</option>');
        //    });
        //});
        UpdateProblemDropDown(url,$problem)
    })
    $(".dropdown-toggle").click(function () {
        var header = $(this);
        var getClass = header.attr('class');
        var menuCode = getClass.split(" ")[1];
        setCookie("classid", menuCode, 1);


    })
    
   
    function UpdateProblemDropDown(url, $problem) {
        var isExist = false;
        var header = $("#Header").val();
        $.ajax({
            url: url,
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            dataType: "json",
            success: function (items) {
                $problem.append('<option value="">--Select One--</option>');
                $.each(items, function (a, b) {
                    if (b.ProblemTypeID === header) {
                        isExist = true;
                    }
                    $problem.append('<option value="' + b.ProblemTypeID + '">' + b.ProblemTypeName + '</option>');
                })
                if (isExist) {
                    $problem.val($("#Header").val());
                }
              
            }
        })
    }
    function removeOptionDropdown(select) {
        select.find('option').remove().end()
    }
    //replace icons with FontAwesome icons like above
    function updatePagerIcons(table) {
        var replacement =
            {
                'ui-icon-seek-first': 'ace-icon fa fa-angle-double-left bigger-140',
                'ui-icon-seek-prev': 'ace-icon fa fa-angle-left bigger-140',
                'ui-icon-seek-next': 'ace-icon fa fa-angle-right bigger-140',
                'ui-icon-seek-end': 'ace-icon fa fa-angle-double-right bigger-140'
            };
        $('.ui-pg-table:not(.navtable) > tbody > tr > .ui-pg-button > .ui-icon').each(function () {
            var icon = $(this);
            var $class = $.trim(icon.attr('class').replace('ui-icon', ''));

            if ($class in replacement) icon.attr('class', 'ui-icon ' + replacement[$class]);
        })
    }
    //$('[data-confirm]').click(function (e) {
    //    bootbox.confirm({
    //        message: $(this).attr("data-confirm"),
    //        buttons: {
    //            confirm: {
    //                label: 'Yes',
    //                className: 'btn-success'
    //            },
    //            cancel: {
    //                label: 'No',
    //                className: 'btn-danger'
    //            }
    //        },
    //        callback: function (result) {
    //            if (result === false) {
    //                e.preventDefault();
    //            }
    //            else
    //            {
    //                $('[data-confirm]').submit();
    //            }
    //        }
    //    });
    //})
})
