﻿$(document).ready(function () {
    $(".formPars").parsley();
    $(".decimal").change(function () {
        var $this = $(this);
        $this.val(parseFloat($this.val()).toFixed(2));
    });
})