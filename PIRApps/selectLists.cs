﻿using PIRApps.data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PIRApps
{
    public class SelectLists
    {
        public IEnumerable<SelectListItem> GetActiveSelectList()
        {
            var selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem
            {
                Value = "true",
                Text = "Active"
            });
            selectList.Add(new SelectListItem
            {
                Value = "false",
                Text = "InActive"
            });
            return selectList;
        }
        public IEnumerable<SelectListItem> GetPaidBySelectList()
        {
            var selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem
            {
                Value = "Vendor",
                Text = "Vendor"
            });
            selectList.Add(new SelectListItem
            {
                Value = "Tenant",
                Text = "Tenant"
            });
            return selectList;
        }
        public IEnumerable<SelectListItem> GetShiftSelectList()
        {
            var selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem
            {
                Value = "Pagi",
                Text = "Pagi"
            });
            selectList.Add(new SelectListItem
            {
                Value = "Siang",
                Text = "Siang"
            });
            selectList.Add(new SelectListItem
            {
                Value = "Malam",
                Text = "Malam"
            });
            return selectList;
        }
        public IEnumerable<SelectListItem> getConditionSelectList()
        {
            var selectlist = new List<SelectListItem>();
            var condition = MasterConditionSecurity.GetAllCondition().Where(x => x.IsActive == true).ToList();
            foreach (var item in condition)
            {
                selectlist.Add(new SelectListItem
                {
                    Value = item.ConditionCode,
                    Text = item.ConditionCode
                });
            }
            return selectlist;
        }
        public IEnumerable<SelectListItem> GetItemCheckingSelectList()
        {
            var data = from i in ItemChecking.GetAllItemChecking()
                       orderby i.CreatedDate descending
                       select new SelectListItem
                       {
                           Value = i.ItemCheckingCode,
                           Text = i.ItemDescription
                       };
            return data;
        }
        public IEnumerable<SelectListItem> GetGroupList()
        {
            var data = from i in MsUserGroup.GetAllUSerGroup()
                       orderby i.CreatedDate descending
                       select new SelectListItem
                       {
                           Value = i.UserGroupCode,
                           Text = i.UserGroupName
                       };
            return data;
        }
        public IEnumerable<SelectListItem> GetUserList()
        {
            var data = UserTb.GetAllActiveUser();
            List<SelectListItem> listsel = new List<SelectListItem>();
            foreach (var item in data)
            {
                SelectListItem sel = new SelectListItem();
                sel.Value = item.UserID.ToString();
                sel.Text = item.Name;
                listsel.Add(sel);
            }
            return listsel;
        }
        public IEnumerable<SelectListItem> GetDepatSelectList()
        {
            var data = DepartementMaster.GetAllActiveDepartement();
            List<SelectListItem> listdept = new List<SelectListItem>();
            foreach (var item in data)
            {
                SelectListItem sel = new SelectListItem();
                sel.Value = item.DepartementID.ToString();
                sel.Text = item.DepartementName;
                listdept.Add(sel);
            }
            return listdept.ToList();
        }
        public IEnumerable<SelectListItem> CategoryPekerjaan()
        {
            var selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem
            {
                Value = "item",
                Text = "Item"
            });
            selectList.Add(new SelectListItem
            {
                Value = "service",
                Text = "Service"
            });
            return selectList;
        }
        public IEnumerable<SelectListItem> Subportfolio()
        {
            var selectList = new List<SelectListItem>();
            selectList.Add(new SelectListItem
            {
                Value = "SPC",
                Text = "SPC"
            });
            selectList.Add(new SelectListItem
            {
                Value = "OFT",
                Text = "OFT"
            });
            return selectList;
        }
        public IEnumerable<SelectListItem> ShopNameSeleclist()
        {
            var data = CurrentDataContext.CurrentContext.TenantMasters.Where(x => x.ShopName != string.Empty);
            var sel = from i in data
                      orderby i.ShopName
                      select new SelectListItem
                      {
                          Text = i.ShopName,
                          Value = i.ShopName

                      };
            return sel;
        }
        public IEnumerable<SelectListItem> GroupPekerjaanCodeToSelectList()
        {
            var data = CurrentDataContext.CurrentContext.GroupPekerjaanHeaders;
            var sel = from i in data
                      orderby i.GroupPekerjaanName
                      select new SelectListItem
                      {
                          Text = i.GroupPekerjaanName,
                          Value = i.GroupPekerjaanCode
                      };
            return sel;
        }
        public IEnumerable<SelectListItem> ItemPekerjaanToSelectList()
        {
            var data = CurrentDataContext.CurrentContext.MsPekerjaans;
            var sel = from i in data
                      orderby i.PekerjaanName
                      select new SelectListItem {
                          Text = i.PekerjaanName,
                          Value = i.PekerjaanCode
                      };
            return sel;
        }
    }
}