USE [master]
GO
/****** Object:  Database [PirApps]    Script Date: 11/5/2017 6:03:25 PM ******/
CREATE DATABASE [PirApps]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PirApps', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\PirApps.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'PirApps_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\PirApps_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [PirApps] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PirApps].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PirApps] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PirApps] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PirApps] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PirApps] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PirApps] SET ARITHABORT OFF 
GO
ALTER DATABASE [PirApps] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PirApps] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [PirApps] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PirApps] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PirApps] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PirApps] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PirApps] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PirApps] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PirApps] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PirApps] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PirApps] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PirApps] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PirApps] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PirApps] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PirApps] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PirApps] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PirApps] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PirApps] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PirApps] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PirApps] SET  MULTI_USER 
GO
ALTER DATABASE [PirApps] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PirApps] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PirApps] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PirApps] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [PirApps]
GO
/****** Object:  Table [dbo].[DepartementMaster]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DepartementMaster](
	[DepartementID] [int] IDENTITY(1,1) NOT NULL,
	[DepartementName] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[DepartementShortName] [nvarchar](50) NULL,
 CONSTRAINT [PK_DepartementMaster] PRIMARY KEY CLUSTERED 
(
	[DepartementID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[EmployeeNIK] [nvarchar](50) NOT NULL,
	[EmployeeName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmployeeNIK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ImgTb]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImgTb](
	[ImgID] [nvarchar](100) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[IsDeleted] [bit] NULL,
	[ImgName] [nvarchar](500) NULL,
	[url] [nvarchar](300) NULL,
 CONSTRAINT [PK_ImgTb] PRIMARY KEY CLUSTERED 
(
	[ImgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ImgTicket]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ImgTicket](
	[ImgTicketid] [int] IDENTITY(1,1) NOT NULL,
	[ImgId] [nvarchar](100) NOT NULL,
	[TicketNo] [nvarchar](300) NULL,
 CONSTRAINT [PK_ImgTicket] PRIMARY KEY CLUSTERED 
(
	[ImgTicketid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ITAssets]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ITAssets](
	[BarcodeNumber] [nvarchar](100) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[User] [nvarchar](50) NULL,
	[LevelPosisi] [nvarchar](50) NULL,
	[Departement] [int] NULL,
	[Location] [int] NULL,
	[UserName] [nvarchar](100) NULL,
	[Model] [nvarchar](50) NULL,
	[Processor] [nvarchar](50) NULL,
	[SN] [nvarchar](50) NULL,
	[Tipe] [nvarchar](50) NULL,
	[TahunPembelian] [nvarchar](50) NULL,
	[RAM] [nvarchar](50) NULL,
	[OS] [nvarchar](50) NULL,
	[OSOEM] [nvarchar](50) NULL,
	[UserStatus] [nvarchar](50) NULL,
	[DomainStatus] [nvarchar](50) NULL,
	[VGACard] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[StatusDesc] [nvarchar](50) NULL,
	[AvayaExt] [nvarchar](50) NULL,
	[AvayaSN] [nvarchar](50) NULL,
	[NoteAvaya] [nvarchar](50) NULL,
	[WorkFlowStatus] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [nvarchar](50) NULL,
	[AssetNo] [nvarchar](100) NULL,
 CONSTRAINT [PK_ITAssets] PRIMARY KEY CLUSTERED 
(
	[BarcodeNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LocationMaster]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LocationMaster](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[LocationName] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_LocationMaster] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MsMenuTb]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MsMenuTb](
	[MenuId] [int] NOT NULL,
	[MenuName] [varchar](100) NULL,
	[MenuParent] [int] NULL,
	[MenuLink] [varchar](200) NULL,
	[MenuAction] [varchar](200) NULL,
	[MenuIcon] [varchar](100) NULL,
	[IsActive] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[ModulID] [nvarchar](50) NULL,
 CONSTRAINT [PK_MsMenuTb] PRIMARY KEY CLUSTERED 
(
	[MenuId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MsUserMenu]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MsUserMenu](
	[MenuID] [int] NULL,
	[UserID] [int] NULL,
	[UserMenuID] [int] IDENTITY(1,1) NOT NULL,
	[CreateData] [varchar](50) NULL,
	[UpdateData] [varchar](50) NULL,
	[ReadData] [varchar](50) NULL,
	[DeleteData] [varchar](50) NULL,
	[IsDefault] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[IsAdmin] [varchar](50) NULL,
 CONSTRAINT [PK_MsUserMenu] PRIMARY KEY CLUSTERED 
(
	[UserMenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProblemType]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProblemType](
	[ProblemTypeID] [int] IDENTITY(1,1) NOT NULL,
	[ProblemTypeName] [nvarchar](500) NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[TicketTypeID] [int] NULL,
 CONSTRAINT [PK_ProblemType] PRIMARY KEY CLUSTERED 
(
	[ProblemTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TaskHistory]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaskHistory](
	[LogTaskId] [int] IDENTITY(1,1) NOT NULL,
	[LogTask] [nvarchar](500) NULL,
	[LogCreatedDate] [datetime] NULL,
	[LogBy] [nvarchar](400) NULL,
	[LogTicketNo] [nvarchar](300) NULL,
 CONSTRAINT [PK_TaskHistory] PRIMARY KEY CLUSTERED 
(
	[LogTaskId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TicketMaster]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketMaster](
	[TicketNo] [nvarchar](300) NOT NULL,
	[TicketType] [int] NULL,
	[TicketHeader] [nvarchar](100) NULL,
	[TicketSubject] [nvarchar](300) NULL,
	[UserId] [nvarchar](500) NULL,
	[PIC] [int] NULL,
	[Status] [nvarchar](1) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[IsDeleted] [bit] NULL,
	[DepartementID] [int] NULL,
	[Location] [int] NULL,
	[Priority] [nvarchar](10) NULL,
	[imgTicket] [int] NULL,
 CONSTRAINT [PK_TicketMaster] PRIMARY KEY CLUSTERED 
(
	[TicketNo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TicketType]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TicketType](
	[TicketTypeID] [int] IDENTITY(1,1) NOT NULL,
	[TicketTypeName] [nvarchar](500) NULL,
	[CreatedBy] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](100) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
	[UserGroupId] [int] NULL,
 CONSTRAINT [PK_TicketType] PRIMARY KEY CLUSTERED 
(
	[TicketTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserGroup]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserGroup](
	[UserGroupID] [int] IDENTITY(1,1) NOT NULL,
	[UserGroupName] [nvarchar](100) NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_UserGroup] PRIMARY KEY CLUSTERED 
(
	[UserGroupID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserTb]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTb](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[IsDeleted] [bit] NULL,
	[UserGroupID] [int] NOT NULL,
	[Email] [nvarchar](100) NULL,
	[Name] [nvarchar](500) NULL,
 CONSTRAINT [PK_UserTb] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkFlowChild]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkFlowChild](
	[WorkFlowChildID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleID] [nvarchar](500) NULL,
	[UserApproval] [int] NULL,
	[IsDeleted] [bit] NULL,
	[WorkFlowIdx] [int] NULL,
 CONSTRAINT [PK_WorkFlowChild] PRIMARY KEY CLUSTERED 
(
	[WorkFlowChildID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkFlowHistory]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkFlowHistory](
	[WorkFlowHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[WorkFlowHistoryDate] [datetime] NULL,
	[WorkFlowHistoryBy] [nvarchar](300) NULL,
	[WorkFlowHistoryMessage] [nvarchar](500) NULL,
	[WorkFlowHistoryKeyID] [nvarchar](50) NULL,
 CONSTRAINT [PK_WorkFlowHistory] PRIMARY KEY CLUSTERED 
(
	[WorkFlowHistoryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkFlowRule]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkFlowRule](
	[RuleID] [int] IDENTITY(1,1) NOT NULL,
	[ModuleID] [nvarchar](500) NULL,
	[ApproveLevel] [int] NULL,
	[IsActive] [bit] NULL,
	[UpdateField] [nvarchar](100) NULL,
	[UpdateTable] [nvarchar](100) NULL,
	[WorkFlowController] [nvarchar](300) NULL,
	[WorkFlowAction] [nvarchar](300) NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[WorkFlowTransaction]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkFlowTransaction](
	[WorkFlowId] [int] IDENTITY(1,1) NOT NULL,
	[WorkFlowName] [varchar](50) NULL,
	[ModuleID] [varchar](50) NULL,
	[UserApproveID] [int] NULL,
	[UniqueIdentifier] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[IsDeleted] [bit] NULL,
	[KeytTableID] [nvarchar](500) NULL,
	[WorkFlowIdx] [int] NULL,
	[WorkFlowStatus] [nvarchar](50) NULL,
	[DetailAction] [nvarchar](300) NULL,
	[DetailController] [nvarchar](300) NULL,
 CONSTRAINT [PK_WorkFlow] PRIMARY KEY CLUSTERED 
(
	[WorkFlowId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[vw_EmployeeMaster]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_EmployeeMaster]
AS
SELECT        EmpNo AS 'NIK', EmployeeName AS 'Name'
FROM            MasterDB.dbo.DimEmployee
WHERE        (LevelCode <> 'BOD')

GO
/****** Object:  View [dbo].[vw_UserMenus]    Script Date: 11/5/2017 6:03:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_UserMenus]
AS
SELECT        dbo.MsUserMenu.MenuID AS MenuId, dbo.MsUserMenu.UserID, dbo.MsUserMenu.UserMenuID, dbo.MsUserMenu.CreateData, dbo.MsUserMenu.UpdateData, dbo.MsUserMenu.ReadData, 
                         dbo.MsUserMenu.IsDefault, dbo.MsUserMenu.DeleteData, dbo.MsUserMenu.CreatedDate AS Expr2, dbo.MsUserMenu.CreatedBy AS Expr3, dbo.MsUserMenu.UpdatedDate AS Expr4, 
                         dbo.MsUserMenu.UpdatedBy AS Expr5, dbo.MsMenuTb.MenuParent, dbo.MsMenuTb.MenuName, dbo.MsMenuTb.MenuLink, dbo.MsMenuTb.MenuAction, dbo.MsMenuTb.MenuIcon, dbo.MsMenuTb.IsActive, 
                         dbo.MsMenuTb.CreatedDate, dbo.MsMenuTb.CreatedBy, dbo.MsMenuTb.UpdatedDate, dbo.MsMenuTb.UpdatedBy, dbo.MsMenuTb.ModulID, dbo.MsUserMenu.UserName, dbo.MsUserMenu.IsAdmin
FROM            dbo.MsUserMenu INNER JOIN
                         dbo.MsMenuTb ON dbo.MsUserMenu.MenuID = dbo.MsMenuTb.MenuId

GO
SET IDENTITY_INSERT [dbo].[DepartementMaster] ON 

INSERT [dbo].[DepartementMaster] ([DepartementID], [DepartementName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [DepartementShortName]) VALUES (0, N'Information Technology', N'Daniel Ekky', CAST(0x0000A81001089A1A AS DateTime), N'Daniel Ekky', CAST(0x0000A81001089ADD AS DateTime), 0, N'IT')
INSERT [dbo].[DepartementMaster] ([DepartementID], [DepartementName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [DepartementShortName]) VALUES (1, N'General Affair', N'Daniel Ekky', CAST(0x0000A81001098533 AS DateTime), N'Daniel Ekky', CAST(0x0000A81001098533 AS DateTime), 0, N'GA')
SET IDENTITY_INSERT [dbo].[DepartementMaster] OFF
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'0906be0a-6072-4469-b740-74a405a92e6d', NULL, N'3', CAST(0x0000A81E00EE5AA5 AS DateTime), NULL, NULL, N'323627e9-d52e-4799-8beb-1be3e50fbbc2.jpg', N'/assets/Image/323627e9-d52e-4799-8beb-1be3e50fbbc2.jpg')
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'13128cf3-c5f8-4984-aa36-1ddf9805882c', NULL, N'3', CAST(0x0000A81E00AB275E AS DateTime), NULL, NULL, N'7d485665-d38e-4653-a120-8b8e4a5709cd.jpg', N'/assets/Image/7d485665-d38e-4653-a120-8b8e4a5709cd.jpg')
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'20f32692-edb8-4908-ad9d-3bf1418d9ade', NULL, N'3', CAST(0x0000A8180118801F AS DateTime), NULL, NULL, N'9c98a858-92e3-41e3-8636-c95fc7511438.jpg', N'/assets/Image/acf153cf-f389-4a6c-b88c-432eb22be8ea.jpg')
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'348f71bf-04c8-4249-aac2-3f8bc94f8402', NULL, N'3', CAST(0x0000A8180125BD26 AS DateTime), NULL, NULL, N'f67be301-5ad1-406d-92aa-2919db2da04b.jpg', N'C:\Users\daniel.ekky\Documents\Visual Studio 2013\Projects\PIRApps\PIRApps\assets\Image\f67be301-5ad1-406d-92aa-2919db2da04b.jpgf67be301-5ad1-406d-92aa-2919db2da04b.jpg')
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'48aa3840-7c82-48c4-b131-3e73666ba36c', NULL, N'3', CAST(0x0000A8180120BFA4 AS DateTime), NULL, NULL, N'8b3aafe0-2de7-46d6-bf3f-053d7fcffb4c.jpg', N'/assets/Image/acf153cf-f389-4a6c-b88c-432eb22be8ea.jpg')
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'ac4da1f5-18fd-444d-bb0b-00ddae1de4e5', NULL, N'9', CAST(0x0000A81E00BD3AE2 AS DateTime), NULL, NULL, N'd551d3aa-2226-4812-bd81-4d781cac58a1.jpg', N'/assets/Image/d551d3aa-2226-4812-bd81-4d781cac58a1.jpg')
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'ae665390-8dd4-4211-9f84-3c33fd52a041', NULL, N'3', CAST(0x0000A81900EBEB35 AS DateTime), NULL, NULL, N'636827be-a4ea-4884-9687-2199a748cac3.jpg', N'/assets/Image/636827be-a4ea-4884-9687-2199a748cac3.jpg')
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'c175e6fe-ab92-492a-9004-5d660f3c28c3', NULL, N'3', CAST(0x0000A81F013036AF AS DateTime), NULL, NULL, N'389e0524-1f83-4ad0-9198-fcb0d0900cac.jpg', N'/assets/Image/389e0524-1f83-4ad0-9198-fcb0d0900cac.jpg')
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'cc781dcc-c314-4c12-b899-43ad9ec2c80e', NULL, N'3', CAST(0x0000A8180126CCA2 AS DateTime), NULL, NULL, N'65cff49e-80ef-4632-b849-0bd9492f3fce.jpg', N'/assets/Image/65cff49e-80ef-4632-b849-0bd9492f3fce.jpg')
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'ce9ba0d2-619a-4158-b5d6-b5d3f06c123c', NULL, N'3', CAST(0x0000A81E00A86E08 AS DateTime), NULL, NULL, N'354f6d65-8805-4d4c-ab72-00ca3ebbf79f.jpg', N'/assets/Image/354f6d65-8805-4d4c-ab72-00ca3ebbf79f.jpg')
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'd0511968-79c9-492c-acac-4d0dace3bba7', NULL, N'9', CAST(0x0000A81F0125785D AS DateTime), NULL, NULL, N'a0cc7b69-49f7-4680-914b-4d94868de763.jpg', N'/assets/Image/a0cc7b69-49f7-4680-914b-4d94868de763.jpg')
INSERT [dbo].[ImgTb] ([ImgID], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [ImgName], [url]) VALUES (N'fbed5b41-3524-44cc-9b2a-93b93fc70c71', NULL, N'3', CAST(0x0000A81801192BFE AS DateTime), NULL, NULL, N'acf153cf-f389-4a6c-b88c-432eb22be8ea.jpg', N'/assets/Image/acf153cf-f389-4a6c-b88c-432eb22be8ea.jpg')
SET IDENTITY_INSERT [dbo].[ImgTicket] ON 

INSERT [dbo].[ImgTicket] ([ImgTicketid], [ImgId], [TicketNo]) VALUES (2, N'48aa3840-7c82-48c4-b131-3e73666ba36c', N'TIC-19/10/2017-0')
INSERT [dbo].[ImgTicket] ([ImgTicketid], [ImgId], [TicketNo]) VALUES (3, N'348f71bf-04c8-4249-aac2-3f8bc94f8402', N'TIC-19/10/2017-0')
INSERT [dbo].[ImgTicket] ([ImgTicketid], [ImgId], [TicketNo]) VALUES (4, N'cc781dcc-c314-4c12-b899-43ad9ec2c80e', N'TIC-19/10/2017-0')
INSERT [dbo].[ImgTicket] ([ImgTicketid], [ImgId], [TicketNo]) VALUES (5, N'ae665390-8dd4-4211-9f84-3c33fd52a041', N'TIC-27/10/2017-1')
INSERT [dbo].[ImgTicket] ([ImgTicketid], [ImgId], [TicketNo]) VALUES (6, N'ce9ba0d2-619a-4158-b5d6-b5d3f06c123c', N'TIC-31/10/2017-1')
INSERT [dbo].[ImgTicket] ([ImgTicketid], [ImgId], [TicketNo]) VALUES (7, N'13128cf3-c5f8-4984-aa36-1ddf9805882c', N'TIC-31/10/2017-1')
INSERT [dbo].[ImgTicket] ([ImgTicketid], [ImgId], [TicketNo]) VALUES (8, N'ac4da1f5-18fd-444d-bb0b-00ddae1de4e5', N'TIC-01/11/2017-1')
INSERT [dbo].[ImgTicket] ([ImgTicketid], [ImgId], [TicketNo]) VALUES (9, N'0906be0a-6072-4469-b740-74a405a92e6d', N'TIC-01/11/2017-3')
INSERT [dbo].[ImgTicket] ([ImgTicketid], [ImgId], [TicketNo]) VALUES (10, N'd0511968-79c9-492c-acac-4d0dace3bba7', N'TIC-01/11/2017-4')
INSERT [dbo].[ImgTicket] ([ImgTicketid], [ImgId], [TicketNo]) VALUES (11, N'c175e6fe-ab92-492a-9004-5d660f3c28c3', N'TIC-01/11/2017-2')
SET IDENTITY_INSERT [dbo].[ImgTicket] OFF
SET IDENTITY_INSERT [dbo].[LocationMaster] ON 

INSERT [dbo].[LocationMaster] ([LocationID], [LocationName], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted]) VALUES (1, N'P2', CAST(0x0000A81001111965 AS DateTime), N'Daniel Ekky', NULL, NULL, 0)
INSERT [dbo].[LocationMaster] ([LocationID], [LocationName], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted]) VALUES (2, N'P1', CAST(0x0000A81001112095 AS DateTime), N'Daniel Ekky', NULL, NULL, 0)
INSERT [dbo].[LocationMaster] ([LocationID], [LocationName], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted]) VALUES (3, N'P3', CAST(0x0000A81001112991 AS DateTime), N'Daniel Ekky', NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[LocationMaster] OFF
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (1, N'Dashboard', 1, N'Dashboard', N'Dashboard', N'<i class="material-icons">apps</i></span><span>Tables</span>', 1, NULL, NULL, NULL, NULL, N'Mod-dash')
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (100000, N'Ticket', 100000, NULL, N'Dashboard', N'<i class="material-icons">map</i>', 1, NULL, NULL, NULL, NULL, N'Mod-Tic')
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (100001, N'Ticket Dashboard', 100000, N'Ticket', N'TicketDashboard', NULL, 1, NULL, NULL, NULL, NULL, N'Mod-Tic')
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (100002, N'Ticket Type', 100000, N'Ticket', N'TicketTypeList', NULL, 1, NULL, NULL, NULL, NULL, N'Mod-Tic')
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (200000, N'Departement', 200000, NULL, N'Departement', N'<i class="material-icons">home</i>', 1, NULL, NULL, NULL, NULL, N'Mod-Dept')
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (200001, N'Departement List', 200000, N'Departement', N'List', NULL, 1, NULL, NULL, NULL, NULL, N'Mod-Dept')
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (300000, N'Location', 300000, NULL, N'Location', N'<i class="material-icons">explore</i>', 1, NULL, NULL, NULL, NULL, N'Mod-Loc')
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (300001, N'Location List', 300000, N'Location', N'List', NULL, 1, NULL, NULL, NULL, NULL, N'Mod-Loc')
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (400000, N'User', 400000, NULL, N'User', N'<i class="material-icons">face</i>', 1, NULL, NULL, NULL, NULL, N'Mod-Usr')
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (400001, N'User List', 400000, N'User', N'List', NULL, 1, NULL, NULL, NULL, NULL, N'Mod-Usr')
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (400002, N'User Group List', 400000, N'User', N'UserGroupList', NULL, 1, NULL, NULL, NULL, NULL, N'Mod-Usr')
INSERT [dbo].[MsMenuTb] ([MenuId], [MenuName], [MenuParent], [MenuLink], [MenuAction], [MenuIcon], [IsActive], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [ModulID]) VALUES (900000, N'WorkFlow', 900000, N'WorkFlow', N'List', N'<i class="material-icons">view_stream</i>', 1, NULL, NULL, NULL, NULL, N'Mod-WF')
SET IDENTITY_INSERT [dbo].[MsUserMenu] ON 

INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (1, 1, 1, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'admin', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (100000, 1, 2, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'admin', N'Y')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (100001, 1, 3, N'Y', N'Y', N'Y', N'Y', N'Y', NULL, NULL, NULL, NULL, N'admin', N'Y')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (200000, 1, 4, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'admin', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (200001, 1, 5, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'admin', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (300000, 1, 6, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'admin', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (300001, 1, 7, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'admin', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (400000, 1, 8, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'admin', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (400001, 1, 9, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'admin', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (400002, 1, 10, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'admin', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (100002, 1, 11, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'admin', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (100000, 3, 12, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'ekky', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (100001, 3, 13, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'ekky', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (1, 3, 14, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'ekky', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (900000, 1, 15, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'admin', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (900000, 5, 16, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'arie', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (100000, 9, 17, N'Y', N'Y', N'Y', N'Y', N'Y', NULL, NULL, NULL, NULL, N'aris', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (100001, 9, 18, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'aris', N'N')
INSERT [dbo].[MsUserMenu] ([MenuID], [UserID], [UserMenuID], [CreateData], [UpdateData], [ReadData], [DeleteData], [IsDefault], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [UserName], [IsAdmin]) VALUES (1, 9, 19, N'Y', N'Y', N'Y', N'Y', N'N', NULL, NULL, NULL, NULL, N'aris', N'N')
SET IDENTITY_INSERT [dbo].[MsUserMenu] OFF
SET IDENTITY_INSERT [dbo].[ProblemType] ON 

INSERT [dbo].[ProblemType] ([ProblemTypeID], [ProblemTypeName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [TicketTypeID]) VALUES (1, N'Blue Screen', N'admin', CAST(0x0000A81100A9886B AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[ProblemType] ([ProblemTypeID], [ProblemTypeName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [TicketTypeID]) VALUES (2, N'HDD Failure', N'admin', CAST(0x0000A81100A9D523 AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[ProblemType] ([ProblemTypeID], [ProblemTypeName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [TicketTypeID]) VALUES (3, N'Mouse', N'admin', CAST(0x0000A81100AA50D7 AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[ProblemType] ([ProblemTypeID], [ProblemTypeName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [TicketTypeID]) VALUES (4, N'PAMS', N'admin', CAST(0x0000A81100CAF8AB AS DateTime), NULL, NULL, 0, 2)
INSERT [dbo].[ProblemType] ([ProblemTypeID], [ProblemTypeName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [TicketTypeID]) VALUES (5, N'Admin Apps', N'admin', CAST(0x0000A81100CB14B3 AS DateTime), NULL, NULL, 0, 2)
INSERT [dbo].[ProblemType] ([ProblemTypeID], [ProblemTypeName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [TicketTypeID]) VALUES (6, N'Other', N'admin', CAST(0x0000A81100CB1C48 AS DateTime), NULL, NULL, 0, 2)
INSERT [dbo].[ProblemType] ([ProblemTypeID], [ProblemTypeName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [TicketTypeID]) VALUES (7, N'Other', N'admin', CAST(0x0000A81100CB39E4 AS DateTime), NULL, NULL, 0, 1)
SET IDENTITY_INSERT [dbo].[ProblemType] OFF
SET IDENTITY_INSERT [dbo].[TaskHistory] ON 

INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (1, N'TICKET TAKEN BY ekky', CAST(0x0000A81D012B8500 AS DateTime), NULL, N'TIC-31/10/2017-1')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (2, N'TICKET SOLVED BY ekky', CAST(0x0000A81E00A89334 AS DateTime), NULL, N'TIC-31/10/2017-1')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (3, N'TICKET SOLVED BY ekky', CAST(0x0000A81E00A9CB8E AS DateTime), NULL, N'TIC-31/10/2017-1')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (4, N'TICKET SOLVED BY ekky', CAST(0x0000A81E00AB2BDB AS DateTime), NULL, N'TIC-31/10/2017-1')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (5, N'TICKET TAKEN BY Aristya', CAST(0x0000A81E00BD170F AS DateTime), NULL, N'TIC-01/11/2017-1')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (6, N'TICKET SOLVED BY Aristya', CAST(0x0000A81E00BD4028 AS DateTime), NULL, N'TIC-01/11/2017-1')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (7, N'TICKET TAKEN BY ekky', CAST(0x0000A81E00E396F9 AS DateTime), NULL, N'TIC-01/11/2017-3')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (8, N'TICKET SOLVED BY ekky', CAST(0x0000A81E00EE6A12 AS DateTime), NULL, N'TIC-01/11/2017-3')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (9, N'TICKET TAKEN BY ekky', CAST(0x0000A81F0123B57D AS DateTime), NULL, N'TIC-01/11/2017-4')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (10, N'TICKET SOLVED BY Aristya', CAST(0x0000A81F012960B9 AS DateTime), NULL, N'TIC-01/11/2017-4')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (11, N'TICKET UPDATED BY Daniel Ekky', CAST(0x0000A81F01300740 AS DateTime), NULL, N'TIC-01/11/2017-2')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (12, N'TICKET TAKEN BY ekky', CAST(0x0000A81F01302286 AS DateTime), NULL, N'TIC-01/11/2017-2')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (13, N'TICKET SOLVED BY ekky', CAST(0x0000A81F01304F59 AS DateTime), NULL, N'TIC-01/11/2017-2')
INSERT [dbo].[TaskHistory] ([LogTaskId], [LogTask], [LogCreatedDate], [LogBy], [LogTicketNo]) VALUES (14, N'test add comment', CAST(0x0000A82000BED6B8 AS DateTime), NULL, N'TIC-01/11/2017-3')
SET IDENTITY_INSERT [dbo].[TaskHistory] OFF
INSERT [dbo].[TicketMaster] ([TicketNo], [TicketType], [TicketHeader], [TicketSubject], [UserId], [PIC], [Status], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DepartementID], [Location], [Priority], [imgTicket]) VALUES (N'TIC-01/11/2017-1', 2, N'4', N'PAMS TIDAK BISA DI BUKA', N'ARIS', 9, N'C', CAST(0x0000A81E00BB1A24 AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00BD3B3A AS DateTime), N'Aristya', 0, 0, 1, N'M', 8)
INSERT [dbo].[TicketMaster] ([TicketNo], [TicketType], [TicketHeader], [TicketSubject], [UserId], [PIC], [Status], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DepartementID], [Location], [Priority], [imgTicket]) VALUES (N'TIC-01/11/2017-2', 1, N'2', N'tes', N'tes', 3, N'C', CAST(0x0000A81E00DEEB8C AS DateTime), N'Daniel Ekky', CAST(0x0000A81F013036E8 AS DateTime), N'ekky', 0, 0, 1, N'L', 11)
INSERT [dbo].[TicketMaster] ([TicketNo], [TicketType], [TicketHeader], [TicketSubject], [UserId], [PIC], [Status], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DepartementID], [Location], [Priority], [imgTicket]) VALUES (N'TIC-01/11/2017-3', 1, N'1', N'tes', N'tes', 3, N'C', CAST(0x0000A81E00E280F2 AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00EE5B1B AS DateTime), N'ekky', 0, 0, 1, N'L', 9)
INSERT [dbo].[TicketMaster] ([TicketNo], [TicketType], [TicketHeader], [TicketSubject], [UserId], [PIC], [Status], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DepartementID], [Location], [Priority], [imgTicket]) VALUES (N'TIC-01/11/2017-4', 1, N'1', N'tes', N'tes', 9, N'C', CAST(0x0000A81E012E8A2C AS DateTime), N'Daniel Ekky', CAST(0x0000A81F0125789A AS DateTime), N'Aristya', 0, 0, 1, N'L', 10)
INSERT [dbo].[TicketMaster] ([TicketNo], [TicketType], [TicketHeader], [TicketSubject], [UserId], [PIC], [Status], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DepartementID], [Location], [Priority], [imgTicket]) VALUES (N'TIC-19/10/2017-0', 1, N'1', N'tiba tiba laptop blue screen', N'adi', 3, N'C', CAST(0x0000A81100FFAE5F AS DateTime), N'Daniel Ekky', CAST(0x0000A8180126CEC0 AS DateTime), N'Daniel Ekky S', 0, 1, 1, N'L', 4)
INSERT [dbo].[TicketMaster] ([TicketNo], [TicketType], [TicketHeader], [TicketSubject], [UserId], [PIC], [Status], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DepartementID], [Location], [Priority], [imgTicket]) VALUES (N'TIC-19/10/2017-1', 2, N'4', N'PAMS tidak bisa di akses', N'lala', 3, N'S', CAST(0x0000A81101002676 AS DateTime), N'Daniel Ekky', CAST(0x0000A81200AC8B31 AS DateTime), N'Daniel Ekky S', 0, 0, 1, N'L', NULL)
INSERT [dbo].[TicketMaster] ([TicketNo], [TicketType], [TicketHeader], [TicketSubject], [UserId], [PIC], [Status], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DepartementID], [Location], [Priority], [imgTicket]) VALUES (N'TIC-27/10/2017-1', 1, N'1', N'tes', N'tes', 3, N'C', CAST(0x0000A81900EB21D6 AS DateTime), N'Daniel Ekky', CAST(0x0000A81900EBED06 AS DateTime), N'Daniel Ekky S', 0, 0, 1, N'L', 5)
INSERT [dbo].[TicketMaster] ([TicketNo], [TicketType], [TicketHeader], [TicketSubject], [UserId], [PIC], [Status], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DepartementID], [Location], [Priority], [imgTicket]) VALUES (N'TIC-30/10/2017-1', 1, N'2', N'HDD MELEDAK', N'aris', NULL, N'D', CAST(0x0000A81C011307B3 AS DateTime), N'Daniel Ekky', CAST(0x0000A81D01283B7B AS DateTime), N'Daniel Ekky', 1, 0, 1, N'L', NULL)
INSERT [dbo].[TicketMaster] ([TicketNo], [TicketType], [TicketHeader], [TicketSubject], [UserId], [PIC], [Status], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [DepartementID], [Location], [Priority], [imgTicket]) VALUES (N'TIC-31/10/2017-1', 1, N'1', N'laptop meledak', N'aris', 3, N'C', CAST(0x0000A81D012926D0 AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00AB27EC AS DateTime), N'ekky', 0, 0, 1, N'H', 7)
SET IDENTITY_INSERT [dbo].[TicketType] ON 

INSERT [dbo].[TicketType] ([TicketTypeID], [TicketTypeName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [UserGroupId]) VALUES (1, N'Hardware', N'admin', CAST(0x0000A810013D6672 AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[TicketType] ([TicketTypeID], [TicketTypeName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [UserGroupId]) VALUES (2, N'Application', N'admin', CAST(0x0000A81100CAD58C AS DateTime), NULL, NULL, 0, 2)
INSERT [dbo].[TicketType] ([TicketTypeID], [TicketTypeName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted], [UserGroupId]) VALUES (3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[TicketType] OFF
SET IDENTITY_INSERT [dbo].[UserGroup] ON 

INSERT [dbo].[UserGroup] ([UserGroupID], [UserGroupName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted]) VALUES (1, N'Hardware', N'admin', NULL, NULL, NULL, 0)
INSERT [dbo].[UserGroup] ([UserGroupID], [UserGroupName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted]) VALUES (2, N'Applicaton', N'admin', NULL, NULL, NULL, 0)
INSERT [dbo].[UserGroup] ([UserGroupID], [UserGroupName], [CreatedBy], [CreatedDate], [UpdatedBy], [UpdatedDate], [IsDeleted]) VALUES (3, N'General Affair', N'admin', CAST(0x0000A81001364392 AS DateTime), NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[UserGroup] OFF
SET IDENTITY_INSERT [dbo].[UserTb] ON 

INSERT [dbo].[UserTb] ([UserID], [UserName], [Password], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [UserGroupID], [Email], [Name]) VALUES (1, N'admin', N'vPelNrzLabeGOYF6LMYvZA==', NULL, NULL, NULL, NULL, 0, 1, N'daniel.ekky@plazaindonesia.com', N'Daniel Ekky')
INSERT [dbo].[UserTb] ([UserID], [UserName], [Password], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [UserGroupID], [Email], [Name]) VALUES (3, N'ekky', N'vPelNrzLabeGOYF6LMYvZA==', NULL, NULL, NULL, NULL, NULL, 1, N'daniel.ekky@gmail.com', N'ekky')
INSERT [dbo].[UserTb] ([UserID], [UserName], [Password], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [UserGroupID], [Email], [Name]) VALUES (5, N'arie', N'vPelNrzLabeGOYF6LMYvZA==', NULL, NULL, NULL, NULL, NULL, 2, N'ekky.daniel@gmail.com', N'arie sandjay')
INSERT [dbo].[UserTb] ([UserID], [UserName], [Password], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [UserGroupID], [Email], [Name]) VALUES (9, N'aris', N'vPelNrzLabeGOYF6LMYvZA==', NULL, NULL, NULL, NULL, NULL, 1, N'aristya.kusmanugraha@plazaindonesia.com', N'Aristya')
SET IDENTITY_INSERT [dbo].[UserTb] OFF
SET IDENTITY_INSERT [dbo].[WorkFlowChild] ON 

INSERT [dbo].[WorkFlowChild] ([WorkFlowChildID], [ModuleID], [UserApproval], [IsDeleted], [WorkFlowIdx]) VALUES (1, N'Mod-Tic', 1, 0, 1)
INSERT [dbo].[WorkFlowChild] ([WorkFlowChildID], [ModuleID], [UserApproval], [IsDeleted], [WorkFlowIdx]) VALUES (2, N'Mod-Tic', 5, 0, 2)
SET IDENTITY_INSERT [dbo].[WorkFlowChild] OFF
SET IDENTITY_INSERT [dbo].[WorkFlowHistory] ON 

INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (1, CAST(0x0000A81D01357717 AS DateTime), N'Daniel Ekky', N'APPROVED', N'TIC-19/10/2017-0')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (2, CAST(0x0000A81D0135A8F0 AS DateTime), N'arie sandjay', N'APPROVED and CLOSED', N'TIC-19/10/2017-0')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (3, CAST(0x0000A81E00A8C4E9 AS DateTime), N'Daniel Ekky', N'APPROVED', N'TIC-31/10/2017-1')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (4, CAST(0x0000A81E00A8DEBF AS DateTime), N'arie sandjay', N'APPROVED and CLOSED', N'TIC-31/10/2017-1')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (5, CAST(0x0000A81E00A9E707 AS DateTime), N'Daniel Ekky', N'APPROVED', N'TIC-31/10/2017-1')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (6, CAST(0x0000A81E00AA027A AS DateTime), N'arie sandjay', N'APPROVED and CLOSED', N'TIC-31/10/2017-1')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (7, CAST(0x0000A81E00AB53CF AS DateTime), N'Daniel Ekky', N'APPROVED', N'TIC-31/10/2017-1')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (8, CAST(0x0000A81E00AB6FB5 AS DateTime), N'arie sandjay', N'APPROVED and CLOSED', N'TIC-31/10/2017-1')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (9, CAST(0x0000A81E00B7F763 AS DateTime), N'Daniel Ekky', N'APPROVED', N'TIC-19/10/2017-0')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (10, CAST(0x0000A81E00BFFD1E AS DateTime), N'arie sandjay', N'APPROVED and CLOSED', N'TIC-19/10/2017-0')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (11, CAST(0x0000A81E00C017A0 AS DateTime), N'arie sandjay', N'REJECTED, Reason : kurang jelas', N'TIC-19/10/2017-0')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (12, CAST(0x0000A81E00C3FFEE AS DateTime), N'arie sandjay', N'APPROVED and CLOSED', N'TIC-19/10/2017-0')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (13, CAST(0x0000A81E00C5500A AS DateTime), N'arie sandjay', N'APPROVED and CLOSED', N'TIC-19/10/2017-0')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (14, CAST(0x0000A81E00C6E41A AS DateTime), N'Daniel Ekky', N'APPROVED', N'TIC-01/11/2017-1')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (15, CAST(0x0000A81E00C711FE AS DateTime), N'arie sandjay', N'APPROVED and CLOSED', N'TIC-01/11/2017-1')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (16, CAST(0x0000A81E00EE93FD AS DateTime), N'Daniel Ekky', N'APPROVED', N'TIC-01/11/2017-3')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (17, CAST(0x0000A81E00EFA4A6 AS DateTime), N'arie sandjay', N'APPROVED and CLOSED', N'TIC-01/11/2017-3')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (18, CAST(0x0000A81F0129FCF7 AS DateTime), N'Daniel Ekky', N'APPROVED', N'TIC-01/11/2017-4')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (19, CAST(0x0000A81F012A2619 AS DateTime), N'arie sandjay', N'APPROVED and CLOSED', N'TIC-01/11/2017-4')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (20, CAST(0x0000A81F01307BE1 AS DateTime), N'Daniel Ekky', N'APPROVED', N'TIC-01/11/2017-2')
INSERT [dbo].[WorkFlowHistory] ([WorkFlowHistoryID], [WorkFlowHistoryDate], [WorkFlowHistoryBy], [WorkFlowHistoryMessage], [WorkFlowHistoryKeyID]) VALUES (21, CAST(0x0000A81F0130DBB6 AS DateTime), N'arie sandjay', N'APPROVED and CLOSED', N'TIC-01/11/2017-2')
SET IDENTITY_INSERT [dbo].[WorkFlowHistory] OFF
SET IDENTITY_INSERT [dbo].[WorkFlowRule] ON 

INSERT [dbo].[WorkFlowRule] ([RuleID], [ModuleID], [ApproveLevel], [IsActive], [UpdateField], [UpdateTable], [WorkFlowController], [WorkFlowAction]) VALUES (1, N'Mod-Tic', 2, 1, N'Status', N'TicketMaster', N'Ticket', N'ViewTicketDetailPartial')
INSERT [dbo].[WorkFlowRule] ([RuleID], [ModuleID], [ApproveLevel], [IsActive], [UpdateField], [UpdateTable], [WorkFlowController], [WorkFlowAction]) VALUES (2, N'Mod_Ast', 1, 1, N'Status', N'ITAsset', N'ITAssets', N'ViewAssetsDetail')
SET IDENTITY_INSERT [dbo].[WorkFlowRule] OFF
SET IDENTITY_INSERT [dbo].[WorkFlowTransaction] ON 

INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (1, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A8120109B964 AS DateTime), N'Daniel Ekky S', CAST(0x0000A812013047BD AS DateTime), N'Daniel Ekky', 0, N'TIC-19/10/2017-1', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (3, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81201306C56 AS DateTime), N'Daniel Ekky', CAST(0x0000A81201313683 AS DateTime), N'daniel ekky', 0, N'TIC-19/10/2017-1', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (4, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A8180118F77A AS DateTime), N'Daniel Ekky S', CAST(0x0000A81E00B7F721 AS DateTime), N'Daniel Ekky', 0, N'TIC-19/10/2017-0', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (5, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A8180129DE21 AS DateTime), N'Daniel Ekky S', CAST(0x0000A81D01357705 AS DateTime), N'Daniel Ekky', 0, N'TIC-19/10/2017-0', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (6, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A818012AFEAD AS DateTime), N'Daniel Ekky S', CAST(0x0000A818013833BF AS DateTime), N'Daniel Ekky', 0, N'TIC-19/10/2017-0', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (7, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A818012C144B AS DateTime), N'Daniel Ekky S', CAST(0x0000A81801307D42 AS DateTime), N'Daniel Ekky', 0, N'TIC-19/10/2017-0', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (8, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A818012C5FF4 AS DateTime), N'Daniel Ekky S', CAST(0x0000A818012F6EA0 AS DateTime), N'Daniel Ekky', 0, N'TIC-19/10/2017-0', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (9, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A818012CE52B AS DateTime), N'Daniel Ekky S', CAST(0x0000A818012E5CA9 AS DateTime), N'Daniel Ekky', 0, N'TIC-19/10/2017-0', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (10, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A818012E5D39 AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00BFFE5A AS DateTime), N'arie sandjay', 0, N'TIC-19/10/2017-0', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (11, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A818012F74F2 AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00C558B8 AS DateTime), N'arie sandjay', 0, N'TIC-19/10/2017-0', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (12, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81801307D53 AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00C40757 AS DateTime), N'arie sandjay', 0, N'TIC-19/10/2017-0', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (13, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A818013833CC AS DateTime), N'Daniel Ekky', CAST(0x0000A81C010CCF55 AS DateTime), N'daniel ekky', 0, N'TIC-19/10/2017-0', 2, N'REJECTED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (14, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A81900EBF82A AS DateTime), N'Daniel Ekky S', CAST(0x0000A81900EC468F AS DateTime), N'Daniel Ekky', 0, N'TIC-27/10/2017-1', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (15, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81900EC46AA AS DateTime), N'Daniel Ekky', CAST(0x0000A81900EC6E08 AS DateTime), N'daniel ekky', 0, N'TIC-27/10/2017-1', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (16, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81D01357751 AS DateTime), N'Daniel Ekky', CAST(0x0000A81D0135A8F2 AS DateTime), N'arie sandjay', 0, N'TIC-19/10/2017-0', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (17, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A81E00A89364 AS DateTime), N'ekky', CAST(0x0000A81E00A8C4C0 AS DateTime), N'Daniel Ekky', 0, N'TIC-31/10/2017-1', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (18, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81E00A8C4F1 AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00A8DEC1 AS DateTime), N'arie sandjay', 0, N'TIC-31/10/2017-1', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (19, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A81E00A9CB98 AS DateTime), N'ekky', CAST(0x0000A81E00A9E703 AS DateTime), N'Daniel Ekky', 0, N'TIC-31/10/2017-1', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (20, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81E00A9E70D AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00AA074D AS DateTime), N'arie sandjay', 0, N'TIC-31/10/2017-1', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (21, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A81E00AB2BFA AS DateTime), N'ekky', CAST(0x0000A81E00AB53C4 AS DateTime), N'Daniel Ekky', 0, N'TIC-31/10/2017-1', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (22, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81E00AB53DB AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00AB7426 AS DateTime), N'arie sandjay', 0, N'TIC-31/10/2017-1', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (23, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81E00B7F772 AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00C0179D AS DateTime), N'arie sandjay', 0, N'TIC-19/10/2017-0', 2, N'REJECTED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (24, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A81E00BD4052 AS DateTime), N'Aristya', CAST(0x0000A81E00C6E39C AS DateTime), N'Daniel Ekky', 0, N'TIC-01/11/2017-1', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (25, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81E00C6E46A AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00C713FA AS DateTime), N'arie sandjay', 0, N'TIC-01/11/2017-1', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (26, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A81E00EE6A3F AS DateTime), N'ekky', CAST(0x0000A81E00EE93EB AS DateTime), N'Daniel Ekky', 0, N'TIC-01/11/2017-3', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (27, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81E00EE9415 AS DateTime), N'Daniel Ekky', CAST(0x0000A81E00EFA5C9 AS DateTime), N'arie sandjay', 0, N'TIC-01/11/2017-3', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (28, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A81F012960EF AS DateTime), N'Aristya', CAST(0x0000A81F0129FCE8 AS DateTime), N'Daniel Ekky', 0, N'TIC-01/11/2017-4', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (29, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81F0129FCFF AS DateTime), N'Daniel Ekky', CAST(0x0000A81F012A2934 AS DateTime), N'arie sandjay', 0, N'TIC-01/11/2017-4', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (30, N'Ticketing WorkFlow', N'Mod-Tic', 1, N'WF-Mod-Tic', CAST(0x0000A81F01304F6C AS DateTime), N'ekky', CAST(0x0000A81F01307BDC AS DateTime), N'Daniel Ekky', 0, N'TIC-01/11/2017-2', 1, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
INSERT [dbo].[WorkFlowTransaction] ([WorkFlowId], [WorkFlowName], [ModuleID], [UserApproveID], [UniqueIdentifier], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy], [IsDeleted], [KeytTableID], [WorkFlowIdx], [WorkFlowStatus], [DetailAction], [DetailController]) VALUES (31, N'Ticketing WorkFlow', N'Mod-Tic', 5, N'WF-Mod-Tic', CAST(0x0000A81F01307BEA AS DateTime), N'Daniel Ekky', CAST(0x0000A81F0130DD50 AS DateTime), N'arie sandjay', 0, N'TIC-01/11/2017-2', 2, N'APPROVED', N'ViewTicketDetailPartial', N'Ticket')
SET IDENTITY_INSERT [dbo].[WorkFlowTransaction] OFF
ALTER TABLE [dbo].[ImgTicket]  WITH CHECK ADD  CONSTRAINT [FK_ImgTicket_ImgTb] FOREIGN KEY([ImgId])
REFERENCES [dbo].[ImgTb] ([ImgID])
GO
ALTER TABLE [dbo].[ImgTicket] CHECK CONSTRAINT [FK_ImgTicket_ImgTb]
GO
ALTER TABLE [dbo].[ITAssets]  WITH CHECK ADD  CONSTRAINT [FK_ITAssets_DepartementMaster] FOREIGN KEY([Departement])
REFERENCES [dbo].[DepartementMaster] ([DepartementID])
GO
ALTER TABLE [dbo].[ITAssets] CHECK CONSTRAINT [FK_ITAssets_DepartementMaster]
GO
ALTER TABLE [dbo].[ITAssets]  WITH CHECK ADD  CONSTRAINT [FK_ITAssets_ITAssets] FOREIGN KEY([Location])
REFERENCES [dbo].[LocationMaster] ([LocationID])
GO
ALTER TABLE [dbo].[ITAssets] CHECK CONSTRAINT [FK_ITAssets_ITAssets]
GO
ALTER TABLE [dbo].[ProblemType]  WITH CHECK ADD  CONSTRAINT [FK_ProblemType_TicketType] FOREIGN KEY([TicketTypeID])
REFERENCES [dbo].[TicketType] ([TicketTypeID])
GO
ALTER TABLE [dbo].[ProblemType] CHECK CONSTRAINT [FK_ProblemType_TicketType]
GO
ALTER TABLE [dbo].[TicketMaster]  WITH CHECK ADD  CONSTRAINT [FK_TicketMaster_DepartementMaster] FOREIGN KEY([DepartementID])
REFERENCES [dbo].[DepartementMaster] ([DepartementID])
GO
ALTER TABLE [dbo].[TicketMaster] CHECK CONSTRAINT [FK_TicketMaster_DepartementMaster]
GO
ALTER TABLE [dbo].[TicketMaster]  WITH CHECK ADD  CONSTRAINT [FK_TicketMaster_ImgTicket] FOREIGN KEY([imgTicket])
REFERENCES [dbo].[ImgTicket] ([ImgTicketid])
GO
ALTER TABLE [dbo].[TicketMaster] CHECK CONSTRAINT [FK_TicketMaster_ImgTicket]
GO
ALTER TABLE [dbo].[TicketMaster]  WITH CHECK ADD  CONSTRAINT [FK_TicketMaster_LocationMaster] FOREIGN KEY([Location])
REFERENCES [dbo].[LocationMaster] ([LocationID])
GO
ALTER TABLE [dbo].[TicketMaster] CHECK CONSTRAINT [FK_TicketMaster_LocationMaster]
GO
ALTER TABLE [dbo].[TicketMaster]  WITH CHECK ADD  CONSTRAINT [FK_TicketMaster_TicketType] FOREIGN KEY([TicketType])
REFERENCES [dbo].[TicketType] ([TicketTypeID])
GO
ALTER TABLE [dbo].[TicketMaster] CHECK CONSTRAINT [FK_TicketMaster_TicketType]
GO
ALTER TABLE [dbo].[TicketMaster]  WITH CHECK ADD  CONSTRAINT [FK_TicketMaster_UserTb] FOREIGN KEY([PIC])
REFERENCES [dbo].[UserTb] ([UserID])
GO
ALTER TABLE [dbo].[TicketMaster] CHECK CONSTRAINT [FK_TicketMaster_UserTb]
GO
ALTER TABLE [dbo].[TicketType]  WITH CHECK ADD  CONSTRAINT [FK_TicketType_UserGroup] FOREIGN KEY([UserGroupId])
REFERENCES [dbo].[UserGroup] ([UserGroupID])
GO
ALTER TABLE [dbo].[TicketType] CHECK CONSTRAINT [FK_TicketType_UserGroup]
GO
ALTER TABLE [dbo].[UserTb]  WITH CHECK ADD  CONSTRAINT [FK_UserTb_UserGroup] FOREIGN KEY([UserGroupID])
REFERENCES [dbo].[UserGroup] ([UserGroupID])
GO
ALTER TABLE [dbo].[UserTb] CHECK CONSTRAINT [FK_UserTb_UserGroup]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "DimEmployee (MasterDB.dbo)"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 220
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_EmployeeMaster'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_EmployeeMaster'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "MsUserMenu"
            Begin Extent = 
               Top = 6
               Left = 56
               Bottom = 217
               Right = 406
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MsMenuTb"
            Begin Extent = 
               Top = 42
               Left = 466
               Bottom = 172
               Right = 636
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 27
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_UserMenus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_UserMenus'
GO
USE [master]
GO
ALTER DATABASE [PirApps] SET  READ_WRITE 
GO
